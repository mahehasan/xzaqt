<?php

class Emergency_model extends CI_Model {

    private $tableName = 'emergency_contact';
    
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $uid = Common::user('id');
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $relationship = $this->input->post('relationship');
        $homePhone = $this->input->post('homePhone');
        $mobilePhone = $this->input->post('mobilePhone');
        
        $data = array(
            'user_id' => $uid,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'relationship' => $relationship,
            'home_phone' => $homePhone,
            'mobile_phone' => $mobilePhone,
        );
        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
