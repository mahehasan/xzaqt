<div class="content" id="content-your-quiz" style="display: none;">
    <div class="row margin-10 margin-bottom-50">
        <div class="col-md-1"> </div>
        <div class="col-md-8">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class=" fa fa-fw fa-heart"></i></span>
                <span class="thm-text"> Quiz  </span>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
    <form role="form" action="<?php echo BASE_URL; ?>candidate/profilesetup/quiz" method="post" id="your-quiz-frm">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-10">
                    <div class="quiz">
                        <div class="text-center"> <span class=" skill-circle" >1/5</span> </div>
                        <div class="quiz-title">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididuntut 
                            labore et dolore magna aliqua ? 
                        </div>
                        <div class="quiz-answer-list">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <ul>
                                        <li>
                                            <label for="qualification"> 
                                                <input name="answer"  type="checkbox" class="form-control"> 
                                                Question answer option 1
                                            </label>                                        
                                        </li>
                                        <li>
                                            <label for="qualification"> 
                                                <input name="answer"  type="checkbox" class="form-control"> 
                                               Question answer option 2
                                            </label>                                        
                                        </li> 
                                        <li>
                                            <label for="qualification"> 
                                                <input name="answer"  type="checkbox" class="form-control"> 
                                                Question answer option 3
                                            </label>                                        
                                        </li>  
                                        <li>
                                            <label for="qualification"> 
                                                <input name="answer"  type="checkbox" class="form-control"> 
                                                Question answer option 4
                                            </label>                                        
                                        </li>                                          
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-top-50 text-center">
                            <button class="btn btn-theme-2" name="save"> Next Question </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
   
        <div style="margin-top:30px;" class="row margin-bottom-50 "> 
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-theme" name="save" id="save-your-quiz"> Save & Next </button>
            </div>
        </div>
    </form>
</div> <!-- Employment History End -->  
<script src="<?php echo BASE_URL; ?>assets/js/profile/quiz.js"></script>