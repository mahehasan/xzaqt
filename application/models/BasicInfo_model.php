<?php

class BasicInfo_model extends CI_Model {

    private $tableName = 'basic_info';
    private $userId;
    private $msg;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->userId = Common::userId();
        
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add($msg = NULL) {
        $qualification = $this->input->post('qualification');
        $jobTitle = $this->input->post('title');
        $firstName = $this->input->post('firstName');
        $forName = $this->input->post('forName');
        $lastName = $this->input->post('lastName');
        $dob = $this->input->post('dateOfBirth');
        $mobile = $this->input->post('mobileTelephone');
        $streetAddress = $this->input->post('streetAddress');
        $addressTwo = $this->input->post('address2');
        $city = $this->input->post('town');
        $country = $this->input->post('country_selector_code');
        $postalCode = $this->input->post('postalCode');
        $faxNumber = $this->input->post('faxNumber');
        
        // date mysql formate 
        //  $dob
        
       
        
        
        $data = array(
            'user_id' => $this->userId,
            'job_title' => $jobTitle,
            'first_name' => $firstName,
            'fore_name' => $forName,
            'last_name' => $lastName,
            'email' => $this->session->userdata('email'),
            'dob' => $dob,
            'mobile' => $mobile,
            'validation_code' => $msg,
            'street_address' => $streetAddress,
            'address_2' => $addressTwo,
            'city' => $city,
            'country' => $country,
            'postal_code' => $postalCode,
            'fax_number' => $faxNumber,
        );
        if ($this->save($data)) {
            return true;
        }
    }

    public function save($data) {
        if (Common::findByEmailBul($this->tableName) !== TRUE) {
            Common::save($this->tableName, $data);
            return true;
        } else {
            unset($data['email']);
            $this->db->where('user_id', $this->userId);
            $this->db->update($this->tableName, $data);
            return FALSE;
        }
    }

}
