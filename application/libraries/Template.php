<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Template
 *
 * @author farhad
 */
class Template {
    public static $ci;
    public static $basicInfo;
    

    public function __construct() {
        self::$ci = & get_instance();
        self::$ci->load->database();
    }
        
    //put your code here
    public static function set_view($name = NULL , $data = NULL ) {
        self::$ci->load->view($name, $data);
    }
}
