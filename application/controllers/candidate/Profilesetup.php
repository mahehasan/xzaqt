<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileSetup extends CI_Controller {

    private $to;
    public $progressChart = array();

    public function __construct() {

        parent::__construct();
        Common::canview();
        Common::goToEmp();
        $this->load->library('Common');
        $this->load->model('profile_model');
        $this->load->model('Grade_model');
        $this->load->model('Language_model');
        $this->load->model('Industry_type_model');
        $this->load->model('Progress_model');
        $this->load->model('Progress_model');
        //$this->to = Common::basic('mobile');
        $this->msg = Common::msg();
        $this->load->model('Progress_settings_model');
        $this->load->model('Progress_settings_model');
    }

    public function index() {
        if ($this->session->userdata('isUserConnected')) {
            $this->indexdata();
        } else {
            redirect('hauth');
            //Common::canview();
        }
    }

    public function indexdata() {
        $data['title'] = 'Profile Setup';
        $data['view_name'] = 'profile/profile_setup';
        $data['grade'] = $this->Grade_model->get();
        $data['languages'] = $this->Language_model->get();
        $data['industry_type'] = $this->Industry_type_model->get();
        $this->template($data);
    }

    public function basicinfo() {

        //if ($this->input->post('save-basicinfo')) {
            $msg = $this->msg;
            $data['basicinfo'] = $this->profile_model->basic_info($_SESSION['email']);
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'trim|xss_clean|required|required');
            $this->form_validation->set_rules('firstName', 'First Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('forName', 'Forename', 'trim|xss_clean|required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|is_unique[basic_info.email]|valid_email');
            $this->form_validation->set_rules('dateOfBirth', 'Date Of Birth', 'trim|xss_clean');
            $this->form_validation->set_rules('mobileTelephone', 'Mobile Telephone', 'trim|xss_clean|required');
            $this->form_validation->set_rules('streetAddress', 'Street Address', 'trim|xss_clean|required');
            $this->form_validation->set_rules('address2', 'Address Two', 'trim|xss_clean|required');
            $this->form_validation->set_rules('town', 'Town', 'trim|xss_clean|required');
            $this->form_validation->set_rules('country_selector_code', 'Country', 'trim|xss_clean|required');
            $this->form_validation->set_rules('postalCode', 'Postal Code', 'trim|xss_clean|required');
            $this->form_validation->set_rules('faxNumber', 'Fax Number', 'trim|numeric|xss_clean|required');
            //$this->form_validation->set_message('is_unique', 'This email is already exist');
            $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
            if ($this->form_validation->run()) {
                $this->load->model('BasicInfo_model');
                $this->load->model('Progress_model');
                if ($this->BasicInfo_model->add($msg)) {
//                    $this->load->library('twilio');
//                    $from = '+442033225743';
//                    $to = $this->input->post('mobileTelephone');
//                    $response = $this->twilio->sms($from, $to, $msg);
//                    if ($response->IsError) {
//                        return '"msg":' . '"' . $response->ErrorMessage . '",';
//                    } else {
//                        //echo $this->Progress_model->add('basicInfo',20);
//                        return 'Sent message to ' . $to;
//                    }
                }
                $progress = $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('basicInfo'));
                echo $progress;
            } else {
                $error = array();
                $error['title'] = form_error('title');
                $error['firstName'] = form_error('firstName');
                $error['forName'] = form_error('forName');
                $error['lastName'] = form_error('lastName');
                $error['email'] = form_error('email');
                $error['mobileTelephone'] = form_error('mobileTelephone');
                $error['streetAddress'] = form_error('streetAddress');
                $error['town'] = form_error('town');
                $error['country_selector_code'] = form_error('country_selector_code');
                $error['postalCode'] = form_error('postalCode');
                $error['faxNumber'] = form_error('faxNumber');
                $error['is_unique'] = form_error('is_unique');
                $error['is_unique'] = form_error('is_unique');
               // echo json_encode($error);
            }
        } 
    

    public function phoneVarification() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('varificationCode', 'Validation Code', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->load->model('Phonevalidation_model');
            if ($this->Phonevalidation_model->check()) {
                echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('phnVal'));
            } else {
                echo 'falsee';
            }
        } else {
            echo 'form Validation Error';
        }
    }

    public function addSkills() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('language1', 'First Language', 'trim|xss_clean|required|required');
        $this->form_validation->set_rules('lang1_level', 'Level', 'trim|xss_clean|required');
        $this->form_validation->set_rules('language2', 'Second Language', 'trim|xss_clean|required|required');
        $this->form_validation->set_rules('lang2_level', 'Level', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {

            $skills_head_id = 1;
            $uid = Common::user('id');

            $lang1 = $this->input->post('language1');
            $lang1_level = $this->input->post('lang1_level');
            $lang2 = $this->input->post('language2');
            $lang2_level = $this->input->post('lang2_level');

            $data = array(
                array(
                    'skills_head_id' => $skills_head_id,
                    'user_id' => $uid,
                    'skills_id' => $lang1,
                    'fluency_level' => $lang1_level,
                ),
                array(
                    'skills_head_id' => $skills_head_id,
                    'user_id' => $uid,
                    'skills_id' => $lang2,
                    'fluency_level' => $lang2_level,
                )
            );
            $where = array('user_id' => $uid, 'skills_head_id' => 1);
            $this->db->where($where);
            $this->db->delete('skills_dist');
            $status = Common::save('skills_dist', $data);

            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('langskills'));

            $languageDetails = Common::findByColumn('skills_dist', $where);
            //echo json_encode($languageDetails);
        } else {
            echo 'Form validation Error';
        }
    }

    public function addComSkills() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('comSkillOne', 'Skills One', 'trim|xss_clean|required');
        $this->form_validation->set_rules('last_used_1', 'Level1 ', 'trim|xss_clean');
        $this->form_validation->set_rules('com_level_1', 'First Language', 'trim|xss_clean|required');

        $this->form_validation->set_rules('comSkillTwo', 'Skills Two', 'trim|xss_clean|required');
        $this->form_validation->set_rules('last_used_2', 'Level2 ', 'trim|xss_clean');
        $this->form_validation->set_rules('com_level_2', 'Second Language', 'trim|xss_clean|required');

        $this->form_validation->set_rules('comSkillThree', 'Skills Three', 'trim|xss_clean|required');
        $this->form_validation->set_rules('last_used_3', 'Level3 ', 'trim|xss_clean');
        $this->form_validation->set_rules('com_level_3', 'Third Language', 'trim|xss_clean|required');

        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $skills_head_id = 2;
            $uid = Common::user('id');

            $comSkillOne = $this->input->post('comSkillOne');
            $com_level_1 = $this->input->post('com_level_1');
            $last_used_1 = $this->input->post('last_used_1');

            $comSkillTwo = $this->input->post('comSkillTwo');
            $com_level_2 = $this->input->post('com_level_2');
            $last_used_2 = $this->input->post('last_used_3');

            $comSkillThree = $this->input->post('comSkillThree');
            $com_level_3 = $this->input->post('com_level_3');
            $last_used_3 = $this->input->post('last_used_3');

            $data = array(
                array(
                    'user_id' => $uid,
                    'skills_head_id' => $skills_head_id,
                    'skills_id' => $comSkillOne,
                    'fluency_level' => $com_level_1,
                    'current_status' => $last_used_1
                ),
                array(
                    'user_id' => $uid,
                    'skills_head_id' => $skills_head_id,
                    'skills_id' => $comSkillTwo,
                    'fluency_level' => $com_level_2,
                    'current_status' => $last_used_2
                ),
                array(
                    'user_id' => $uid,
                    'skills_head_id' => $skills_head_id,
                    'skills_id' => $comSkillThree,
                    'fluency_level' => $com_level_3,
                    'current_status' => $last_used_3
                )
            );
            $where = array('user_id' => $uid, 'skills_head_id' => 2);
            $this->db->where($where);
            $this->db->delete('skills_dist');
            $status = Common::save('skills_dist', $data);
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('comSkills'));

            $comSkillsDetails = Common::findByColumn('skills_dist', $where);
            // echo json_encode($comSkillsDetails);
        } else {
            echo 'Form validation Error';
        }
    }

    public function addEmpHistory() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('roll', 'Roll', 'trim|xss_clean|required');
        $this->form_validation->set_rules('companyName', 'Company Name ', 'trim|xss_clean');
        $this->form_validation->set_rules('industrySector', 'Industry Sector ', 'trim|xss_clean');
        $this->form_validation->set_rules('from', 'From', 'trim|xss_clean|required');
        $this->form_validation->set_rules('to', 'To', 'trim|xss_clean');
        $this->form_validation->set_rules('current', 'Current', 'trim|xss_clean');
        $this->form_validation->set_rules('system', 'System', 'trim|xss_clean|required');
        $this->form_validation->set_rules('rollDescription', 'Role Description & Tasks ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('achievement', 'Achievement & Accomplishment ', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->load->model('Emphistory_model');
            $this->Emphistory_model->add();
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('empHis'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function educaton() {
        $this->load->library('form_validation');
        $this->load->model('Education_model');
        $this->load->model('Voluntary_work_model');
        $this->form_validation->set_rules('qualification', 'Qualification', 'trim|xss_clean|required');
        $this->form_validation->set_rules('subjectStudied', 'Subject Studied', 'trim|xss_clean|required');
        $this->form_validation->set_rules('universityName', 'University Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('grade', 'Grade', 'trim|xss_clean|required');
        $this->form_validation->set_rules('passin_year', 'Passing year', 'trim|xss_clean|required');
        $this->form_validation->set_rules('voluntryWork[0]', 'First Voluntary Work or Causes', 'trim|xss_clean');
        $this->form_validation->set_rules('voluntryWork[1]', 'Seond Voluntary Work or Causes', 'trim|xss_clean');
        $this->form_validation->set_rules('voluntryWork[2]', 'Third Voluntary Work or Causes', 'trim|xss_clean');
        $this->form_validation->set_rules('voluntryWork[3]', 'Fourth Voluntary Work or Causes', 'trim|xss_clean');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->Education_model->add();
            $this->Voluntary_work_model->add();
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('education'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function personal() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('citizenship', 'Citizenship', 'trim|xss_clean');
        $this->form_validation->set_rules('passport', 'Passport', 'trim|xss_clean');
        $this->form_validation->set_rules('businessName', 'Your Business Name', 'trim|xss_clean');
        $this->form_validation->set_rules('companyStructure', 'Company Structure', 'trim|xss_clean');
        $this->form_validation->set_rules('docfile', 'Docfile', 'trim|xss_clean');
        $this->form_validation->set_rules('companyNumber', 'Company Number', 'trim|xss_clean');
        $this->form_validation->set_rules('uniqueTaxReference', 'Unique Tax Reference', 'trim|xss_clean');
        $this->form_validation->set_rules('vatRegistered', 'VAT Registered ?', 'trim|xss_clean');
        $this->form_validation->set_rules('doYouRequireAnAcc', 'Do You require an Account?', 'trim|xss_clean');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->load->model('Personal_model');
            $this->Personal_model->add();
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('personal'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function emergency() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstName', 'First Name', 'trim|xss_clean');
        $this->form_validation->set_rules('lastName', 'Last Name', 'trim|xss_clean');
        $this->form_validation->set_rules('relationship', 'Relationship', 'trim|xss_clean');
        $this->form_validation->set_rules('homePhone', 'Home Phone', 'trim|xss_clean');
        $this->form_validation->set_rules('mobilePhone', 'Mobile Phone', 'trim|xss_clean');
        if ($this->form_validation->run()) {
            $this->load->model('Emergency_model');
            $this->Emergency_model->add();
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('emgctc'));
        } else {
            echo 'Form validation Error';
        }
    }

    /* mahe vai      */

    public function jobs_preferences() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rollInterested', 'Roles your are interested in', 'trim|xss_clean|required');
        $this->form_validation->set_rules('preferedWlocation', 'Prefered work location ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('minimumRate', 'Minimum Rate', 'trim|xss_clean|required');
        $this->form_validation->set_rules('preferedRate', 'Preferred Rate ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('roleType', 'Role Type ( Temporary, Temp to Perm )', 'trim|xss_clean|required');
        $this->form_validation->set_rules('roleDuration', 'Role Duration', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workStartTime', 'When Can you start', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workStatus', 'Are you currently Employed', 'trim|xss_clean|required');
        $this->form_validation->set_rules('benefits', 'Benefits', 'trim|xss_clean|required');
        $this->form_validation->set_rules('pensionsPlan', 'Pensions Plan ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('healthInsurance', 'Health Insurance ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('lifeInsurance', 'Life Insurance ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('bonusPayment', 'Bonus Payment', 'trim|xss_clean|required');
        $this->form_validation->set_rules('contractFuture', 'Contract Future', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->load->model('Job_prefer_model');
            $this->Job_prefer_model->add();
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('jobpref'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function preferred_workplace_industries() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('preferedcompanySize', ' Your preferred workplace ', 'trim|xss_clean|required');
        $this->form_validation->set_rules('preferedIndustry[]', 'Jobs Industries Preferences', 'trim|xss_clean|required');
        $this->form_validation->set_rules('IndustryType', 'Add More Industry', 'trim|xss_clean');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            // Add job preference 
            $this->load->model('Prefered_work_place_industry_model');
            $this->Prefered_work_place_industry_model->add();
            // Add progress bar 
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('preWorInd'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function self_video() {
        echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('selfVideo'));
    }

    public function quiz() {
        echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('quiz'));
    }

    public function references() {
        $this->load->model('Reference_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required');
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|xss_clean|required');
        $this->form_validation->set_rules('relation', 'Relationship', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<span class="text-red field-error">', '</span>');
        if ($this->form_validation->run()) {
            $this->Reference_model->add();

            // Add progress bar 
            echo $this->Progress_model->add($this->Progress_settings_model->findProgressIdByName('reference'));
        } else {
            echo 'Form validation Error';
        }
    }

    public function delete() {
        $id = $this->input->post('id');
        $this->db->where('skills_dist_id', $id);
        $this->db->delete('skills_dist');
        redirect('Profile/skills');
    }

    public function joinDate() {
        $date_session = $_SESSION['userdata']->registration_date;
        $date = new DateTime($date_session);
        $sart_date = $date->format('M, Y');
        return $sart_date;
    }

    public function template($data) {
        $this->load->model('Profile_model');
        $data['basicinfo'] = $this->Profile_model->basic_info($_SESSION['email']);
        $this->load->view('common/header_home', $data);
        //$this->load->view('common/admin_sidebar');
        $this->load->view($data['view_name'], $data);
        $this->load->view('common/footer_after_log');
    }

    public function to() {
        $this->db->get();
    }

}
