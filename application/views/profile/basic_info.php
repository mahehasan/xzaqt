<div class="content">
    <form id="form-basicInfo" role="form" action="<?php echo base_url(); ?>candidate/profilesetup/basicinfo" method="post">
        <div class="row">
            <hr>  
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="title">Title</label> 
                    <?php echo form_error('title'); ?>
                    <input name="title"  type="text" value="<?php echo Common::basicPost('job_title', 'title'); ?>" class="form-control" id="title" placeholder="Title" required>
                </div>      
                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <?php echo form_error('firstName'); ?>
                    <input name="firstName"  type="text" value="<?php echo Common::basicPost('first_name', 'firstName'); ?>" class="form-control" id="firstName" placeholder="First Name" required>
                </div>   
                <div class="form-group">
                    <label for="forName">Forename(s)</label>
                    <?php echo form_error('forName'); ?>
                    <input name="forName" value="<?php echo Common::basicPost('fore_name', 'forName'); ?>" type="text" class="form-control" id="forName" placeholder="Forename(s)" required>
                </div>  
                <div class="form-group">
                    <label for="lastName">Last Name</label>
                    <?php echo form_error('firstName'); ?>
                    <input name="lastName" value="<?php echo Common::basicPost('last_name', 'lastName'); ?>" type="text" class="form-control" id="lastName" placeholder="Last Name" required>
                </div>                                                  
<!--                <div class="form-group">
                    <label for="email">Email</label>
                    <?php //echo form_error('email'); ?>
                    <input name="email" value="<?php //echo $this->session->userdata('email'); // echo Common::basicPost('email', 'email'); ?>" type="text" class="form-control" id="email" placeholder="Email" disabled required>
                </div> -->
                <div class="form-group">
                    <label for="dob">Date Of Birth</label>
                    <?php echo form_error('dateOfBirth'); ?>
                    <input name="dateOfBirth"  data-date-format='yyyy-mm-dd' value="<?php echo Common::basicPost('dob', 'dateOfBirth'); ?>" type="text" class="form-control" id="dob" required>
                </div>
                <div class="form-group">
                    <label for="mobileTelephone">Mobile Telephone</label>
                    <?php echo form_error('mobileTelephone'); ?>
                    <input name="mobileTelephone" value="<?php echo Common::basicPost('mobile', 'mobileTelephone'); ?>" type="text" class="form-control" id="mobileTelephone" placeholder="Mobile Telephone" required>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="streetAddress">Street Address</label>
                    <?php echo form_error('streetAddress'); ?>
                    <input name="streetAddress" value="<?php echo Common::basicPost('street_address', 'streetAddress'); ?>" type="text" class="form-control" id="streetAddress" placeholder="Street Address" required>
                </div>
                <div class="form-group">
                    <label for="address2">Address 2</label>
                    <?php echo form_error('address2'); ?>
                    <input name="address2" value="<?php echo Common::basicPost('address_2', 'address2'); ?>" type="text" class="form-control" id="address2" placeholder="Second Address" required>
                </div>
                <div class="form-group">
                    <label for="town">Town</label>
                    <?php echo form_error('town'); ?>
                    <input name="town" value="<?php echo Common::basicPost('city', 'town'); ?>" type="text" class="form-control" id="town" placeholder="Town" required>
                </div>
                <div class="form-group">
                    <label for="country">Country</label>
                    <?php echo form_error('country'); ?>
                    <div class="form-item">
                        <input class="country_selector" id="country_selector" type="text">
                        <label for="country_selector" style="display:none;">Select a country here...</label>
                    </div>
                    <div class="form-item" style="display:none;">
                        <input type="text" id="country_selector_code" name="country_selector_code" data-countrycodeinput="1" readonly="readonly" placeholder="Selected country code will appear here" />
                        <label for="country_selector_code">...and the selected country code will be updated here</label>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="postalCode">Postal code</label>
                    <?php echo form_error('postalCode'); ?>
                    <input name="postalCode" value="<?php echo Common::basicPost('postal_code', 'postalCode'); ?>" type="text" class="form-control" id="postalCode" placeholder="Postal Code" required>
                </div>    
                <div class="form-group">
                    <label for="faxNumber">Fax Number</label>
                    <?php echo form_error('faxNumber'); ?>
                    <input name="faxNumber" value="<?php echo Common::basicPost('fax_number', 'faxNumber'); ?>" type="text" class="form-control" id="faxNumber" placeholder="Fax Number" required>
                </div>                  
            </div>                                            
            <div class="col-md-1"></div>                   
        </div>
        <div class="row text-center">
            <p class="text-center">Please make sure enter right working phone number you will receive a code verification in few seconds after submitting this form</p>                                            
            <button name="savenext" value="save-basicinfo" class="btn btn-primary" type="submit" id="save-basicinfo" >Save & Next </button>
                    
        </div>
    </form>
</div>

<!-- Select Country -->
<script src="<?php echo BASE_URL; ?>assets/plugins/countryselect/countrySelect.min.js"></script> 
<script src="<?php echo BASE_URL; ?>assets/js/profile/basicinfo.js"></script>
