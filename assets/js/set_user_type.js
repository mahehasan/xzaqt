$(function () {
    var user_type;

    $('#emp-dash-candidate').on('click', function () {
        user_type = $(this).val();
    });

    $('#emp-dash-employer').on('click', function () {
        user_type = $(this).val();
    });
    $('#set-user-type').on('submit', function (e) {
        e.preventDefault();
        var data = 'user_type=' + user_type;
        var url = BASE_URL + "hauth/setusertype_validation";

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (results) {
              //  console.log(results);
                if(results == 1){
                    var url = BASE_URL + "employer/dashboard";
                    window.location.href = url;
                } else if(results == 2){
                    var url = BASE_URL + "candidate/dashboard";
                    window.location.href = url;                    
                } else {
                    var url = BASE_URL;
                    window.location.href = url;
                }
                                
                //console.log(results);
//                messageDiv.fadeIn("slow");
//                messageDiv.find(".validation_text").text('Education history added successfully and ready to add another');
//                messageDiv.delay(2000).fadeOut("slow");
//                $('span.show-barprogerss').empty();
//                $('span.show-barprogerss').append(progress);
            }
        });

    });


});

