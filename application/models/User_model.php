<?php

class User_model extends CI_Model {

    private $tableName = 'users';

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function insert_entry($data) {
        $this->db->insert('source', $data);
    }

    public function totalrow() {
        $query = $this->db->get('source');
        return count($query->result());
    }

    public function get_data($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by("id", "desc")->get("source");
        return $query->result();
    }

    public function getUserDetails($email) {
        $query = $this->db->where('email', $email)->get('users');
        return $query->result();
    }

    public function savelinkdin() {
        $table = $this->tableName;
        $user_profile = (array) $_SESSION['user_profile'];
        $email = $user_profile['email'];

        $this->db->where('email', $email);
        $query = $this->db->get($this->tableName);
        $user_exist_or_not = $query->num_rows();

        if ($user_exist_or_not) {
            $data = array(
                'email' => $user_profile['email'],
                'by_linkdin' => 1
            );
            $this->db->where('email', $email);
            $this->db->update('users', $data);
        } else {
            $data = array(
                'email' => $user_profile['email'],
                'type' => $_SESSION['user_type'],
                'by_linkdin' => 1
            );
            $this->db->insert($table, $data);
        }
    }

    public function setuser_type() {
        $table = $this->tableName;
        $email = $_SESSION['user_profile']['email']; //$this->input->post('email');
        $type =  $_SESSION['user_type']; // $this->input->post('type'); 
        $data = array(
            'email' => $email,
            'type' => $type,
            'by_linkdin' => 1,
        );
        $this->save($data);
    }

    public function userExist() {
        $query = $this->db->where('email', $_SESSION['email'])
                ->get($this->tableName);
        if ($query->num_rows()>0) {
            $result = $query->result();
            return $result[0];
        } else {
            return false;
        }
    }
    

    public function typeExist() {
        $query = $this->db->select('type')
                ->where('email', $_SESSION['email'])
                ->get($this->tableName);
        $result_obj = $query->result();
        if ($this->db->affected_rows()) {
            $result = $result_obj[0]->type;
            return $result;
        } else {
            return false;
        }
    }

    // addcUser function description 
    //type = 3 company user

    public function addCUser() {
        $user = Common::userId();
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $jobPosition = $this->input->post('jobPosition');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = array(
            'type' => 3,
            'email' => $email,
            'password' => md5($password),
        );
        $this->save($data);
    }

    private function save($data) {
        $this->db->insert($this->tableName, $data);
    }

}
