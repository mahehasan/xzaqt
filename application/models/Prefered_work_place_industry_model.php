<?php

class Prefered_work_place_industry_model extends CI_Model {

    private $tableName = 'prefered_wordplace_industry';
    
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $uid = Common::user('id');
        $preferedcompanySize = $this->input->post('preferedcompanySize');
        $prefered_job_industry = $this->input->post('preferedIndustry');
        
        $data = array();
        $i= 0; $j=0; $k=0;
        foreach ( $prefered_job_industry as $value) {
            $data[$i++]['user_id'] = $uid;
            $data[$j++]['organization_size'] = $preferedcompanySize;
            $data[$k++]['prefered_job_industry'] = $value; 
        }
        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
