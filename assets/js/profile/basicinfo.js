$(document).ready(function () {
    $('#form-basicInfo').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    stringLength: {
                        min: 2,
                        message: 'Fill at least 2  character'
                    },
                    notEmpty: {
                        message: 'Please fill the field'
                    }
                }
            },            
            faxNumber: {
                validators: {
                    stringLength: {
                        min: 2,
                        message: 'Atleast 2  character'
                    },
                    notEmpty: {
                        message: 'Please supply your Fax Number'
                    },
                    numeric: {
                        message: 'Please supply your A number'
                    },
                }
            }, 
            mobileTelephone: {
                validators: {
                    stringLength: {
                        min: 10,
                        message: 'Atleast 10  character'
                    },
                    notEmpty: {
                        message: 'Please supply your Fax Number'
                    },
                    numeric: {
                        message: 'Please supply your A number'
                    },
                }
                
            }
        }
    }).on('success.form.bv', function (e) {
        $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
        $('#form-basicInfo').data('bootstrapValidator').resetForm();
        // Prevent form submission
        e.preventDefault();
        
        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        
    });
    $('#form-basicInfo').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                $('.nav-tabs a[href="#phone-valication"]').tab('show');
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });        
        
    });
});
//$(function () {
//    $('.status-message-emp').hide();
//    $('#form-basicInfo').on('submit', function (e) {
//        e.preventDefault();
//        var data = $(this).serialize();
//        var url = $(this).attr('action');
//        // ajax submition 
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: data,
//            cache: false,
//            dataType: 'json',
//            success: function (results) {
//                var progress = results + "%";
//                $('#pregressbar').css("width", progress);
//                $('.nav-tabs a[href="#phone-valication"]').tab('show');
//                $('span.show-barprogerss').empty();
//                $('span.show-barprogerss').append(progress);
//            }
//        });
//
//    });


//});

