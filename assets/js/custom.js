
$(function () {

    $('div.login-box').parent().addClass('hold-transition login-page');
    //$('input').iCheck({ checkboxClass: 'icheckbox_square-blue', radioClass: 'iradio_square-blue', increaseArea: '20%'});
    var update_div = $('#update_warning').parent().parent();
    update_div.on('click', function () {
        $(this).hide();
    });
    $('.skill-dist-id').on('click', function () {

        var skill_dist_id = $("[name='skills_list_id']");
        var language = $("[name='language']");
        var reading = $("[name='reading']");
        var writing = $("[name='writing']");
        skill_dist_id.val($(this).val());
        var td = $(this).parent().parent().children();
        var td1 = td.first().attr('value');
        var td2 = td.first().next().attr('value');
        var td3 = td.first().next().next().attr('value');
        language.val(td1).change();
        reading.val(td2).change();
        writing.val(td3).change();
    });
    $('.fluency-progress span').on('click', function () {
        var obj = $(this).closest($('.custom-grid'));
        obj.find('span').removeClass('active-pro');
        $(this).addClass('active-pro');
//        $(this).addClass('active-pro');
//        var fluency_level = $(this).attr('value');
//        console.log(obj.find('span'));
    });

    //The Calender
    if ($('#calendar').length != 0) {
        $('#calendar').datepicker();
    }
    if ($('.daterange').length != 0) {
        $('.daterange').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
        }, function (start, end) {
            window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
    }

    /* login function */

    $('.sign-pass-btn').on('click', function () {
        $('div#login-pass-content').show('slow', function () {});
        $('div#reset-pass-content').hide();
        $('div#register-pass-content').hide();
    });
    $('a.reset-pass-btn').on('click', function () {
        $('div#login-pass-content').hide();
        $('div#reset-pass-content').show('slow', function () {});
        $('div#register-pass-content').hide();
    });
    $('a.register-btn').on('click', function () {
        $('div#login-pass-content').hide();
        $('div#reset-pass-content').hide();
        $('div#register-pass-content').show('slow', function () {});
    });
    $('button.loginbox-close').on('click', function () {
        $('div#login-pass-content').hide();
        $('div#reset-pass-content').hide();
        $('div#register-pass-content').hide();
    });

    $('button#get-started-asemp').on('click', function () {
        $('.nav-tabs a[href="#employer-registration"]').tab('show');
        $('div#login-pass-content').hide()
        $('div#reset-pass-content').hide();
        $('div#register-pass-content').show('slow', function () {});
        
            // ajax submition 
            data = 'user_type=' + 1;
            var url = BASE_URL + 'hauth';
            // ajax submition 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                cache: false,
                dataType: 'json',
                success: function (result) {
                    result;
                    
                }
            });          
        
        
    });
    $('button#get-started-ascand').on('click', function () {
        $('.nav-tabs a[href="#candidate-registration"]').tab('show');
        $('div#login-pass-content').hide();
        $('div#reset-pass-content').hide();
        $('div#register-pass-content').show('slow', function () {});

            // ajax submition 
            data = 'user_type=' + 2;
            var url = BASE_URL + 'hauth';
            // ajax submition 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                cache: false,
                dataType: 'json',
                success: function (result) {
                    result;
                }
            });        
       
    });
});

