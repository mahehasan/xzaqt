<?php

class Profile_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function user_details() {
        $query = $this->db->get('users');
        return $query->result();
    }

    public function getSkills($skill, $user_id) {
        $this->db->select('sd.skills_dist_id, sd.user_id, sd.skills_head_id, 
            sh.skills_head_name as skills_head, sd.language_country_id as language_id, 
            lc.language_name, sd.reading_label, sd.writing_label');
        $this->db->from('skills_dist sd');
        $this->db->join('skills_head sh', 'sd.skills_head_id = sh.skills_head_id', 'inner');
        $this->db->join('language_country lc', 'lc.id = sd.language_country_id', 'inner');
        $this->db->where('sh.skills_head_name', $skill);
        $this->db->where('sd.user_id', $user_id);
        $this->db->order_by('lc.language_name', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getLanguage() {

        $query = $this->db->get('language_country');
        return $query->result();
    }

    public function basic_info($email) {
        $query = $this->db->where('email', $email)
                ->get('basic_info');
        return $query->result();
    }

    public function basic() {
        $where = array('bi.user_id' => $_SESSION['id']);
        $query = $this->db->where($where)->get('basic_info as bi');
        $result = $query->result();
        if ($query->num_rows()) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function langskills_public() {
        $where = array('sl.user_id' => $_SESSION['id'], 'sl.skills_head_id' => 1);
        $query = $this->db->where($where)
                ->select('sl.id,  fluency_level, l.name')
                ->from('skills_dist as sl')
                ->join('settings_languages as l', 'l.id = sl.skills_id')
                ->get();

//        echo $this->db->last_query();
//        die();
        $result = $query->result();
        if ($query->num_rows()) {
            return $result;
        } else {
            return false;
        }
    }

    public function comskills_public() {
        $where = array('sc.user_id' => $_SESSION['id'], 'sc.skills_head_id' => 2);
        $query = $this->db->where($where)
                ->select('sc.id,  sc.fluency_level, c.name')
                ->from('skills_dist as sc')
                ->join('settings_computer_skills as c', 'c.id = sc.skills_id')
                ->get();

        //    echo $this->db->last_query();

        $result = $query->result();
        if ($query->num_rows()) {
            return $result;
        } else {
            return false;
        }
    }

    public function introduction_video() {
        $where = array('user_id' => $_SESSION['id'], 'type_id' => 1);
        $query = $this->db->where($where)
                ->get('upload_img_desc');
        $result = $query->result();
        if ($query->num_rows()) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function employe_history() {
        $where = array('user_id' => $_SESSION['id']);
        $query = $this->db->where($where)
                ->get('employee_history');
        $result = $query->result();
        if ($query->num_rows()) {
            return $result;
        } else {
            return false;
        }
    }
    public function education() {
        $where = array('user_id' => $_SESSION['id']);
        $query = $this->db->where($where)
                ->get('education');
        $result = $query->result();
        if ($query->num_rows()) {
            return $result;
        } else {
            return false;
        }
    }
    public function voluntary_work_causes() {
        $where = array('user_id' => $_SESSION['id']);
        $query = $this->db->where($where)
                ->get('voluntary_work_causes');
        $result = $query->result();
        if ($query->num_rows()) {
            return $result;
        } else {
            return false;
        }
    }
}
