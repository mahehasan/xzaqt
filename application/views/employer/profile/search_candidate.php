<div class="container emp-dashboard-main-content">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <h2 class="filterTitle">Filters</h2>
                <div class="filter">
                    <div class="form-group">
                        <label class="label filterlabel">Country</label>
                        <input id="search-country" type="text">
                    </div>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <span class="glyphicon glyphicon-minus"></span>
                                        Industries
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="" method="">
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Fashion</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Technology</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Medicine</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Civil Engineering</label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Skills
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="" method="">
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Skill 1</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Skill 2</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Skill 3</label>
                                        </div>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Skill 4</label>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="" placeholder="Add skills here">
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Qualifications
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Coming Soon...
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Years of ecperience
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Coming Soon...
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Recent graduates
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    text
                                </div>
                            </div>
                        </div>
                    </div>
                    <a id="search-filters" class="btn apply_btn">Search</a>
                </div>

            </div>

            <div class="col-md-9">
                <div class="col-md-8">
                    <h3 class="candidates">Showing 1-20 of 100,000 candidates</h3>
                </div>

                <div class="col-md-4">
                    <div class="form-group pull-right CandidateSort">
                        <label class="col-md-4 filterlabel">Sort By</label>
                        <div class="col-md-8">
                            <select class="form-control" name="">
                                <option>Relevance</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="Candidate_rowA">
                            <div class="CandidateTop">
                                <div class="col-md-3">
                                    <img class="img-responsive" src="../images/clients.png" alt="clients" />
                                </div>

                                <div class="col-md-9">
                                    <div class="col-md-6">
                                        <h1>Nina Jane</h1>
                                        <p><span>Interested in</span> : Product Manager</p>
                                        <p><span>Previous</span> : Porduct Manager at Google</p>
                                        <p><span>Location </span>: Los Angles, USA</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="Candidatehourly">
                                            <h6>Hourly rate</h6>
                                            <h5>$50<span>/h</span></h5>
                                        </div>
                                        <p>10 assignment completed</p>
                                        <button type="button" class="btn btn-default btn-lg str_btn">
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn apply_btn" href="#">INVITE TO APPLY</a>
                                        <a class="btn sort_btn" href="#">SHORTLIST</a>
                                    </div>
                                </div>
                            </div>

                            <div class="CondidateBottom">
                                <div class="col-md-6">
                                    <p><span>Skills</span> :  Product Management, Microsoft office, Team managing</p>
                                    <p><span>Industries</span> : Fashion, Technology.</p>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-user" aria-hidden="true"><a class="profile_link" href="#">VIEW FULL PROFILE</a></i>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-video-camera" aria-hidden="true"><a class="profile_link" href="#">VIEW VIDEO</a></i>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Candidate_rowA">
                            <div class="CandidateTop">
                                <div class="col-md-3">
                                    <img class="img-responsive" src="../images/clients.png" alt="clients" />
                                </div>

                                <div class="col-md-9">
                                    <div class="col-md-6">
                                        <h1>Nina Jane</h1>
                                        <p><span>Interested in</span> : Product Manager</p>
                                        <p><span>Previous</span> : Porduct Manager at Google</p>
                                        <p><span>Location </span>: Los Angles, USA</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="Candidatehourly">
                                            <h6>Hourly rate</h6>
                                            <h5>$50<span>/h</span></h5>
                                        </div>
                                        <p>10 assignment completed</p>
                                        <button type="button" class="btn btn-default btn-lg str_btn">
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn apply_btn" href="#">INVITE TO APPLY</a>
                                        <a class="btn sort_btn" href="#">SHORTLIST</a>
                                    </div>
                                </div>
                            </div>

                            <div class="CondidateBottom">
                                <div class="col-md-6">
                                    <p><span>Skills</span> :  Product Management, Microsoft office, Team managing</p>
                                    <p><span>Industries</span> : Fashion, Technology.</p>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-user" aria-hidden="true"><a class="profile_link" href="#">VIEW FULL PROFILE</a></i>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-video-camera" aria-hidden="true"><a class="profile_link" href="#">VIEW VIDEO</a></i>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Candidate_rowA">
                            <div class="CandidateTop">
                                <div class="col-md-3">
                                    <img class="img-responsive" src="../images/clients.png" alt="clients" />
                                </div>

                                <div class="col-md-9">
                                    <div class="col-md-6">
                                        <h1>Nina Jane</h1>
                                        <p><span>Interested in</span> : Product Manager</p>
                                        <p><span>Previous</span> : Porduct Manager at Google</p>
                                        <p><span>Location </span>: Los Angles, USA</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="Candidatehourly">
                                            <h6>Hourly rate</h6>
                                            <h5>$50<span>/h</span></h5>
                                        </div>
                                        <p>10 assignment completed</p>
                                        <button type="button" class="btn btn-default btn-lg str_btn">
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn apply_btn" href="#">INVITE TO APPLY</a>
                                        <a class="btn sort_btn" href="#">SHORTLIST</a>
                                    </div>
                                </div>
                            </div>

                            <div class="CondidateBottom">
                                <div class="col-md-6">
                                    <p><span>Skills</span> :  Product Management, Microsoft office, Team managing</p>
                                    <p><span>Industries</span> : Fashion, Technology.</p>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-user" aria-hidden="true"><a class="profile_link" href="#">VIEW FULL PROFILE</a></i>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-video-camera" aria-hidden="true"><a class="profile_link" href="#">VIEW VIDEO</a></i>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Candidate_rowA">
                            <div class="CandidateTop">
                                <div class="col-md-3">
                                    <img class="img-responsive" src="../images/clients.png" alt="clients" />
                                </div>

                                <div class="col-md-9">
                                    <div class="col-md-6">
                                        <h1>Nina Jane</h1>
                                        <p><span>Interested in</span> : Product Manager</p>
                                        <p><span>Previous</span> : Porduct Manager at Google</p>
                                        <p><span>Location </span>: Los Angles, USA</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="Candidatehourly">
                                            <h6>Hourly rate</h6>
                                            <h5>$50<span>/h</span></h5>
                                        </div>
                                        <p>10 assignment completed</p>
                                        <button type="button" class="btn btn-default btn-lg str_btn">
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn apply_btn" href="#">INVITE TO APPLY</a>
                                        <a class="btn sort_btn" href="#">SHORTLIST</a>
                                    </div>
                                </div>
                            </div>

                            <div class="CondidateBottom">
                                <div class="col-md-6">
                                    <p><span>Skills</span> :  Product Management, Microsoft office, Team managing</p>
                                    <p><span>Industries</span> : Fashion, Technology.</p>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-user" aria-hidden="true"><a class="profile_link" href="#">VIEW FULL PROFILE</a></i>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-video-camera" aria-hidden="true"><a class="profile_link" href="#">VIEW VIDEO</a></i>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Candidate_rowA">
                            <div class="CandidateTop">
                                <div class="col-md-3">
                                    <img class="img-responsive" src="../images/clients.png" alt="clients" />
                                </div>

                                <div class="col-md-9">
                                    <div class="col-md-6">
                                        <h1>Nina Jane</h1>
                                        <p><span>Interested in</span> : Product Manager</p>
                                        <p><span>Previous</span> : Porduct Manager at Google</p>
                                        <p><span>Location </span>: Los Angles, USA</p>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="Candidatehourly">
                                            <h6>Hourly rate</h6>
                                            <h5>$50<span>/h</span></h5>
                                        </div>
                                        <p>10 assignment completed</p>
                                        <button type="button" class="btn btn-default btn-lg str_btn">
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                            <span class="glyphicon glyphicon-star star_can" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn apply_btn" href="#">INVITE TO APPLY</a>
                                        <a class="btn sort_btn" href="#">SHORTLIST</a>
                                    </div>
                                </div>
                            </div>

                            <div class="CondidateBottom">
                                <div class="col-md-6">
                                    <p><span>Skills</span> :  Product Management, Microsoft office, Team managing</p>
                                    <p><span>Industries</span> : Fashion, Technology.</p>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-user" aria-hidden="true"><a class="profile_link" href="#">VIEW FULL PROFILE</a></i>
                                </div>

                                <div class="col-md-3">
                                    <i class="fa fa-video-camera" aria-hidden="true"><a class="profile_link" href="#">VIEW VIDEO</a></i>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- Select Country -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/countryselect/countrySelect.min.js"></script>
    <script>
        if ($("#search-country").length != 0) {
            $("#search-country").countrySelect({
                defaultCountry: "gb",
                //onlyCountries: ['gb'],
                preferredCountries: ['ca', 'gb', 'us', 'jp']
            });
        }
    </script>
    <script src="<?php echo BASE_URL; ?>assets/js/employer/candidatesearch.js"></script>