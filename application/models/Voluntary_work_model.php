<?php

class Voluntary_work_model extends CI_Model {

    private $tableName = 'voluntary_work_causes';

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        
        $uid = Common::user('id');
        $voluntryWork1 = $this->input->post('voluntryWork[0]');
        $voluntryWork2 = $this->input->post('voluntryWork[1]');
        $voluntryWork3 = $this->input->post('voluntryWork[2]');
        $voluntryWork4 = $this->input->post('voluntryWork[3]');
       
        $data = array(
                        array(
                            'user_id' => $uid,
                            'name' => $voluntryWork1,
                        ),
                        array(
                            'user_id' => $uid,
                            'name' => $voluntryWork2,
                        ),
                        array(
                            'user_id' => $uid,
                            'name' => $voluntryWork3,
                        ),
                        array(
                            'user_id' => $uid,
                            'name' => $voluntryWork4
                        )
                );

        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
