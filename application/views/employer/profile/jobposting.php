<div class="container">

    <div class="col-md-12">
        <h2 class="margin-top-50 text-bold"> Job Posting </h2>
    </div>
    <div class="emp-job-posting">
        <div class="col-md-8">
            <h4 class="text-bold">General Information </h4>
            <hr> 
           
             <?php  $attributes = array('class' => 'email', 'id' => 'form-jobposting');
        			echo form_open('employer/jobposting/add', $attributes); ?>
            
            
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="companyName">Company Name</label> 
                            <?php echo form_error('companyName'); ?>
                            <input name="companyName"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" id="companyName" required>
                        </div>                                                                  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="sectorIndustry">Sector & Industry</label> 
                            <?php echo form_error('sectorIndustry'); ?>
                            <input name="sectorIndustry"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" id="sectorIndustry"required>
                        </div>                                                                  
                    </div>                    
                </div> 

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="jobTitle"> Job Title </label> 
                            <?php echo form_error('jobTitle'); ?>
                            <input name="jobTitle"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" id="jobTitle" required>
                        </div>                                                                  
                    </div>                   
                </div>                

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="numberOfVacancies">Number Of Vacancies </label> 
                            <?php echo form_error('numberOfVacancies'); ?>
                            <input name="numberOfVacancies"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" id="numberOfVacancies" required>
                        </div>                                                                  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="workType"> Work Type </label> 
                            <?php echo form_error('workType'); ?>

                            <select name="workType" class="form-control" id="workType" required>
                                <option value=""> Select  </option>
                                <option value="1"> Contract </option>
                                <option value="2"> Permanent </option>
                            </select>

                        </div>                                                                  
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-md-12"> 
                        <label for="contractLength"> Contract Length </label> 
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="from" class="col-sm-4  text-right control-label">From</label>
                                <div class="col-sm-8">
                                    <?php echo form_error('contractLength'); ?>
                                    <select name="lengthfrom" class="form-control" id="from" required>
                                        <option value=""> Select  </option>
                                        <option value="1"> 1 Month </option>
                                        <option value="2"> 2 Months </option>
                                    </select>
                                </div>                                
                            </div>
                        </div>                                                                  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="to" class="col-sm-4  text-right control-label">To</label>
                                <div class="col-sm-8">
                                    <?php echo form_error('to'); ?>
                                    <select name="lengthto" class="form-control" id="to" required>
                                        <option value=""> Select  </option>
                                        <option value="1"> 1 Month </option>
                                        <option value="2"> 2 Months </option>
                                    </select>
                                </div>                                
                            </div>
                        </div>                                                                   
                    </div>                    
                </div>   

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-bold"> Location </h4>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="country"> Country </label> 
                            <?php echo form_error('country'); ?>
                            <select name="country" class="form-control" id="country" required>
                                <option value=""> Select  </option>
                                <option value="1"> Contract </option>
                                <option value="2"> Permanent </option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stateProvince"> State or province </label> 
                            <?php echo form_error('stateProvince'); ?>
                            <select name="stateProvince" class="form-control" id="stateProvince" required>
                                <option value=""> Select  </option>
                                <option value="1"> Contract </option>
                                <option value="2"> Permanent </option>
                            </select>
                        </div>  
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="city"> City </label> 
                            <?php echo form_error('city'); ?>
                            <select name="city" class="form-control" id="city" required>
                                <option value=""> Select  </option>
                                <option value="1"> Contract </option>
                                <option value="2"> Permanent </option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="postcode"> Postcode </label> 
                            <?php echo form_error('postCode'); ?>
                            <input name="postCode"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" required>
                        </div>  
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-bold"> Date and Time </h4>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="desiredStartDate"> Desired start date </label> 
                            <?php echo form_error('desiredStartDate'); ?>
                            <input name="startdate"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" placeholder="calender" required>
                        </div>  
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hoursPerWeek"> Hours per week </label> 
                            <?php echo form_error('hoursPerWeek'); ?>
                            <input name="hoursPerWeek"  type="text" value="<?php echo Common::iifPost($object = NULL, $fieldName = NULL); ?>" class="form-control" required>
                        </div> 
                    </div>                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="city"> Salary Range </label> 
                            <?php echo form_error('city'); ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label for="min" class="col-sm-4  text-right control-label">Min</label>
                                        <div class="col-sm-8">
                                            <?php echo form_error('min'); ?>
                                            <input type="text" name="salarymin" class="form-control" id="min">    
                                        </div>                                
                                    </div>                               
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <label for="max" class="col-sm-4  text-right control-label">Max</label>
                                        <div class="col-sm-8">
                                            <?php echo form_error('max'); ?>
                                            <input type="text" name="salarymax" class="form-control" id="max">    
                                        </div>                                
                                    </div>    
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="from" class="control-label">Payment Type</label>
                                    <?php echo form_error('paymentType'); ?>
                                    <select name="paymentType" class="form-control" id="paymentType" required>
                                        <option value="">Select</option>
                                        <option value="1"> Hourly </option>
                                        <option value="2"> Fixed </option>
                                    </select>                               

                                </div>                                                                  
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">

                                    <label for="to" class="control-label"> Rebate/guarantee sought </label>
                                    <?php echo form_error('guaranteeSought'); ?>
                                    <select name="guaranteeSought" class="form-control" id="guaranteeSought" required>
                                        <option value=""> Select  </option>
                                        <option value="1"> NO Rebate </option>
                                        <option value="2"> 2 Months </option>
                                    </select>                      

                                </div>                                                                   
                            </div>                    
                        </div>                        
                    </div>                    
                </div>                
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-bold"> Job Details </h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="to" class="control-label"> Essential skills </label>
                            <?php echo form_error('essentialSkills'); ?>
                            <select name="essentialSkills" class="form-control" id="essentialSkills" required>
                                <option value=""> Select  </option>
                                <option value="1"> NO Rebate </option>
                                <option value="2"> 2 Months </option>
                            </select>                      
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="to" class="control-label"> Nice to have </label>
                            <?php echo form_error('niceToHave'); ?>
                            <input type="text" name="niceToHave" class="form-control" id="niceToHave" required>            
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="to" class="control-label"> IT Literacy </label>
                                <?php echo form_error('itLiteracy'); ?>
                                <input type="text" name="itLiteracy" class="form-control" id="itLiteracy" required>            
                            </div>                     
                        </div>                        
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="to" class="control-label"> Job Description </label>
                                <?php echo form_error('jobDescription'); ?>
                                <textarea rows="8" type="text" name="jobDescription" class="form-control" id="jobDescription" required></textarea>             
                            </div>                     
                        </div>                        
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center margin-top-25 margin-bottom-50 ">
                            <button class="btn bg-olive-active"> SAVE </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
                <h4 class="sidebar-top-text text-bold text-center">Use our templetes to maximize<br> 
                    your job conversation</h4>
            </div>
            <div class="col-md-12">
                <div class="widget bg-white">
                    <div class="row">
                        <div class="col-md-8"><h4 class="text-bold text-center">Project Manager</h4></div>
                        <div class="col-md-4"><button class="btn btn-sm btn-theme-2"> Choose </button></div>                              
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur molestie mi, vitae egestas 
                                tellus vulputate ac. Donec vel lorem sed nisi semper finibus id quis augue. Donec lacinia mauris id 
                                vulputate sodales...
                            </p>
                        </div>                              
                    </div>                    
                </div>
                <h4 class="text-bold saved-temp-top text-center"> Your Saved Template </h4>
                <div class="widget bg-white">
                    <div class="row">
                        <div class="col-md-8"><h4 class="text-bold text-center">Project Manager</h4></div>
                        <div class="col-md-4"><button class="btn btn-sm bg-olive-active "> Choose </button></div>                              
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur molestie mi, vitae egestas 
                                tellus vulputate ac. Donec vel lorem sed nisi semper finibus id quis augue. Donec lacinia mauris id 
                                vulputate sodales...
                        </div>                              
                    </div>                    
                </div>       
                <div class="widget bg-white">
                    <div class="row">
                        <div class="col-md-8"><h4 class="text-bold text-center">Project Manager</h4></div>
                        <div class="col-md-4"><button class="btn btn-sm bg-olive-active "> Choose </button></div>                              
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur molestie mi, vitae egestas 
                                tellus vulputate ac. Donec vel lorem sed nisi semper finibus id quis augue. Donec lacinia mauris id 
                                vulputate sodales....
                            </p>
                        </div>                              
                    </div>                    
                </div>
                <div class="widget bg-white">
                    <div class="row">
                        <div class="col-md-8"><h4 class="text-bold text-center">Project Manager</h4></div>
                        <div class="col-md-4"><button class="btn btn-sm bg-olive-active "> Choose </button></div>                              
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur molestie mi, vitae egestas 
                                tellus vulputate ac. Donec vel lorem sed nisi semper finibus id quis augue. Donec lacinia mauris id 
                                vulputate sodales....
                            </p>
                        </div>                              
                    </div>                    
                </div>
                <h4 class="text-bold text-skills text-center margin-top-25 "> Based on your job title, we generate
                    these must have skills.
                    Just click on + to add the skill to you job</h4>
                <div class="widget bg-white">
                    <div class="row">
                        <div class="col-md-8"><h4 class="text-bold text-skills text-left"> Technical Skills </h4></div>
                        <div class="col-md-4"><button class="btn btn-sm bg-olive-active "> Generate </button></div>                              
                        <div class="col-md-12">
                            <div class="tech-skills panel-group" id="" role="tablist" aria-multiselectable="true">
                                
                            
                                
                            </div>                    
                        </div>                              
                    </div>                    

                </div>
            </div>            
        </div>
    </div>   
</div>