<div class="content">  
    <div class="row">

        <div class="col-md-5 col-md-offset-4 text-center">
            <img width="40%" src="<?php echo BASE_IMG; ?>mobile_varification_img.png" alt="Mobile varification">
        </div>        
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <p class="phone-varification">Enter the <span class="highlight-txt"> 4 Digits</span> you have just recievd in your phone</p>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div id="phone-validation-fail" class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Validation! Message : </strong> <span class="validation_text">Validation code is not code is not correct</span>
            </div>
            <div id="phone-validation-resend" class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Validation! Message : </strong> <span class="validation_text"></span>
            </div>            
        </div>
        <div class="col-md-3"></div>
    </div>
    <form id="form-phonevalidation" action="<?php echo BASE_URL; ?>candidate/profilesetup/phoneVarification" method="post">
        <div class="row">
            <div class="col-md-5 col-md-offset-4">
                <div class="form-group">
                    <label for="forName"> Your Phone </label>
                    <?php echo form_error('mobile'); ?>
                    <input name="mobile" value="<?php echo Common::basicPost('mobile', 'mobile'); ?>" type="text" class="form-control" id="mobile" placeholder="Mobile" disabled>
                </div> 
                <div class="form-group">
                    <label for="forName"> Validation Code </label>
                    <?php echo form_error('mobile'); ?>
                    <input name="varificationCode" value="" type="text" class="form-control" id="mobile" placeholder="Mobile" required>
                </div>
            </div>   
        </div> 
        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <button id="code-resend" class="btn btn-theme" name="resend"> Resend </button>
                <button class="btn btn-theme save-role-preference" name="save"> Save & Next </button>
            </div>
        </div>
    </form>   
</div>
<script src="<?php echo BASE_URL; ?>assets/js/profile/phonevalidation.js"></script>