<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="login-box">
    <div class="linkdin-login text-center">
       You're almost Ready
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php
        if (validation_errors()) {
            echo '<div class="alert alert-error alert-dismissable">' .
            '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            echo form_error('email'); 
            echo '</div>';
        }
        if (isset($_SESSION['registration_success'])) {
            echo '    <div class="alert alert-success">
		          <strong>Success!</strong> A email is sent to your account.
		        </div>';
            unset($_SESSION['registration_success']);
        }
        ?>    
        <?php
        $attributes = array('role' => 'form', 'data-toggle' => "validator", 'id' => 'form-registration');
        echo form_open('hauth/setusertype', $attributes);
        ?>
        <div class="form-group">
            
            <input type="email" value="<?php echo Common::checkSession('email'); ?>" name="email" class="form-control" id="email" placeholder="Email" required>
        </div>
        <div class="form-group">
             <label for="email"><?php echo form_error('type'); ?> </label>
            <select  class="form-control" name="type" required="">
                <option value="" disabled selected hidden> Select User Type  </option>
                <option value="2" <?php if(Common::checkTypeSession('user_type') == 2 ){echo 'selected' ;} ?>> Candidate </option>
                <option value="1" <?php if(Common::checkTypeSession('user_type') == 1 ){echo 'selected' ;} ?>> Employer </option>
            </select>
        </div>        
        <div class="row">
            <div class="col-xs-8">
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat"> Get Start </button>
            </div>
            <!-- /.col -->
        </div>

        </form>
        <div class=""> <a href="<?php echo base_url() . 'hauth/reset_password'; ?>">I forgot my password</a><br></div>
    </div>
    <!-- /.login-box-body -->
    
</div>
