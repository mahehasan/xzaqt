$(function () {
    var submitType;
    var messageDiv = $("div#xzaqt-message-alert");
    $("button[name='addEdu']").on('click', function () {
        submitType = $(this).text();
        // remove space 
        submitType = submitType.replace(/\s/g, '');

        //make string lower
        submitType = submitType.toLowerCase();
        console.log(submitType);
    });

    $("button[name='addEduNext']").on('click', function () {
        submitType = $(this).text();

        // remove space 
        submitType = submitType.replace(/\s/g, '');

        //make string lower
        submitType = submitType.toLowerCase();
        console.log(submitType);
    });
    $('.status-message-emp').hide();
    $('#form-education').on('submit', function (e) {
        e.preventDefault();

        var data = $(this).serialize();
        var url = $(this).attr('action');
        //var url = BASE_URL + "profilesetup/educaton";

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                if (submitType == 'save&next') {
                    $('.nav-tabs a[href="#role-preference"]').tab('show');
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Education history added successfully. Add job preferences');
                    messageDiv.delay(2000).fadeOut("slow");
                } else {
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Education history added successfully and ready to add another');
                    messageDiv.delay(2000).fadeOut("slow");
                }
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });

    });


});

