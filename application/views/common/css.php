  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/select2/select2.min.css"> 
  <!-- Select Country -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/countryselect/countrySelect.min.css"> 
  <!--iCheck -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/fullcalendar/fullcalendar.print.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/AdminLTE.min.css"> 
  
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/skins/skin-black.min.css">
  <!-- my style -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/nav-menu.css">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/mystyle.min.css">
  
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">                                                               
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url(); ?>assets/js/html5shiv.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
  <![endif]-->
  