<div class="content">
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-7">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class="fa fa-fw fa-industry"></i></span>
                <span class="thm-text "> Jobs Industries Preferences </span>
            </div>
        </div>
        <div class="col-md-5">

        </div>
    </div>
    <form role="form" action="<?php echo base_url(); ?>candidate/profile/addemploye" method="post">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                    <div class="col-md-10">
                        <div class="row">
                            <?php foreach ($industry_type as $industry) { ?>
                            <div class="col-md-3 preferedIndustry"> 
                                <div class="form-group">
                                    <p>
                                        <label> <input name="preferedIndustry[]" value="<?php echo Common::iifMsg($industry,'id'); ?>" type="hidden"> <?php echo Common::iifMsg($industry, 'name'); ?></label>
                                    </p>
                                </div>
                            </div> 
                            <?php } ?>
                        </div>
                    </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div  class="row industry_type "> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div  class="col-md-12 text-center"> 
                                <p>Are you looking for More Industry? Submit them in the field bellow ( separated by comma )</p>
                                <div class="form-group">
                                    <?php echo form_error('qualification'); ?>
                                   <div class="row"> 
                                       <div class="col-md-3"> </div>
                                       <div class="col-md-6">
                                       <input name="Industry Type"  type="text" value="" class="form-control" id="qualification">
                                       </div>
                                       <div class="col-md-3"> </div>
                                       
                                   </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div class="row"> 
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <button class="btn btn-theme" name="save"> Save & Next </button>
                </div>
            </div>
        </div>
    </form>  
</div> <!-- Employment History End -->  