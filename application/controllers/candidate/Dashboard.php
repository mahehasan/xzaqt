<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        Common::canview();
        Common::goToEmp();
        //  $this->load->model('show_model');
        
    }

    public function index() { 
        if ($this->session->userdata('isUserConnected')) {
            $this->homedata();
        } 
    }

    public function homedata() {        
        
        $data['title'] = 'Home';
        $data['profile_pic'] = Common::showProfile();
        $this->load->model('Profile_model'); 
        $data['basicinfo'] = $this->Profile_model->basic_info($_SESSION['email']);
        $this->load->view('common/header_daschboard', $data);
        $this->load->view('dashboard/admin_sidebar');
        $this->load->view('dashboard/dashboard',$data);
        $this->load->view('common/footer_dashboard');
    }
    public static function iif($array,$object){
         if(isset($array) && count($array) > 0){ 
             echo $array[0]->$object; 
         }
    }  
}
