<div class="content">
    <div class="row margin-bottom-50">
        <div class="col-md-1"> </div>
        <div class="col-md-8">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class=" fa fa-fw fa-plus"></i></span>
                <span class="thm-text "> Personal Info   </span>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <div class="col-md-1"> </div>
            <div class="col-md-5 border-left">
                <form id="form-personalInfo" role="form" action="<?php echo base_url(); ?>candidate/profilesetup/personal" method="post">
                    <div class="form-group">
                        <label for="country">Citizenship</label>
                        <?php echo form_error('Citizenship'); ?>
                        <div class="form-item">
                            <input id="citizenship" id="country_selector" type="text" >
                            <label for="citizenship" style="display:none;">Select a country here...</label>
                        </div>
                        <div class="form-item" style="display:none;">
                            <input type="text" id="citizenship_code" name="citizenship" data-countrycodeinput="1" readonly="readonly" placeholder="Selected country code will appear here" />
                            <label for="citizenship_code">...and the selected country code will be updated here</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passport"> Passport Number  </label> 
                        <?php echo form_error('passport'); ?>
                        <input name="passport"  type="text" value="" class="form-control" id="passport" required>
                    </div>  
                    <div class="form-group">
                        <label for="businessName"> Your Business Name </label> 
                        <?php echo form_error('businessName'); ?>
                        <input name="businessName"  type="text" value="" class="form-control" id="businessName" required>
                    </div>                     
                    <div class="form-group">
                        <label for="companyStructure"> Company Structure </label> 
                        <?php echo form_error('companyStructure'); ?>
                        <input name="companyStructure"  type="text" value="" class="form-control" id="companyStructure" required>
                    </div>
                    <div class="form-group">
                        <div class="upload-doc text-center">
                            <p class="draganddrop "> Click on the button or <br>
                                Drag & Drop a document files here 
                                <br> Allow File <span class="text-sm text-black text-bold "> pdf, doc, docx</span>
                            </p>
                            <div class="uploading-msg">
                            </div>
                            <div id="personal-info-group" class="form-group">
                                <label class="file-upload">            
                                    <input name="file" id="personal-doc" type="file" class="fileupload file-input" data-url="<?php echo BASE_URL ?>upload/personaldoc" >Upload a Doc
                                </label>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="companyNumber"> Company Number </label> 
                        <?php echo form_error('companyNumber'); ?>
                        <input name="companyNumber"  type="text" value="" class="form-control" id="companyNumber" required>
                    </div> 
                    <div class="form-group">
                        <label for="uniqueTaxReference"> Unique Tax Reference </label> 
                        <?php echo form_error('uniqueTaxReference'); ?>
                        <input name="uniqueTaxReference"  type="text" value="" class="form-control" id="uniqueTaxReference" required>
                    </div>  
                    <div class="form-group">
                        <div class="row">
                            <div class='col-md-7'>
                                <label for="vatRegistered"> VAT Registered ?  </label> 
                                <?php echo form_error('vatRegistered'); ?> 
                            </div>
                            <div class='col-md-5'>
                                <input name="vatRegistered"  type="radio" value="1" class="form-control"> &nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp;
                                <input name="vatRegistered"  type="radio" value="0" class="form-control"> &nbsp; No                                
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <div class='col-md-7'>
                                <label for="doYouRequireAnAcc"> Do You require an Account?  </label> 
                                <?php echo form_error('doYouRequireAnAcc'); ?> 
                            </div>
                            <div class='col-md-5'>
                                <input name="doYouRequireAnAcc"  type="radio" value="1" class="form-control" required> &nbsp; Yes &nbsp; &nbsp; &nbsp; &nbsp;
                                <input name="doYouRequireAnAcc"  type="radio" value="0" class="form-control" required> &nbsp; No                                
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-12 text-center margin-bottom-50 margin-top-50">
                        <button id="save-personalInfo" class="btn btn-primary" value="savenext" name="save"> Save & Next </button>
                    </div>
                </form>
            </div>
            <script> var BASE_URL = "<?= BASE_URL; ?>";</script>
            <div class="col-md-5">
                <form id="form-emergency" role="form" action="<?php echo base_url(); ?>candidate/profilesetup/emergency" method="post">
                    <div class="row">
                        <div class="form-group text-center reference-2nd">
                            <div>
                                <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/reference-icon.png" width="80" height="95">
                            </div>
                            <div class="margin-10  ">
                                <p class="theme-text-color"> In an Emergency case! </p>
                                <p> How you would like we contact ?  </p>
                            </div>
                        </div>   
                    </div>

                    <div class="form-group">
                        <label for="firstName"> First Name </label> 
                        <?php echo form_error('firstName'); ?>
                        <input name="firstName"  type="text" value="" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="lastName"> Last Name </label> 
                        <?php echo form_error('lastName'); ?>
                        <input name="lastName"  type="text" value="" class="form-control" >
                    </div>  
                    <div class="form-group">
                        <label for="relationship"> Relationship </label> 
                        <?php echo form_error('relationship'); ?>
                        <input name="relationship"  type="text" value="" class="form-control" >
                    </div>  
                    <div class="form-group">
                        <label for="homePhone"> Home phone </label> 
                        <?php echo form_error('homePhone'); ?>
                        <input name="homePhone"  type="text" value="" class="form-control" >
                    </div> 
                    <div class="form-group">
                        <label for="mobilePhone"> Mobile phone </label> 
                        <?php echo form_error('mobilePhone'); ?>
                        <input name="mobilePhone"  type="text" value="" class="form-control" >
                    </div>                     
                    <div class="form-group text-center">
                        <button class="btn btn-theme-2 " value="addEmerGency" name="addEmemCtc"> Add Other Contact  </button>

                    </div>
                </form>
            </div>
            <div class="col-md-1"> </div>
        </div> 
    </div>
    <div class="row"> 

    </div>

</div> <!-- Employment History End -->  

<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/vendor/jquery.ui.widget.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.iframe-transport.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.fileupload.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/js/profile/personalinfo.js"></script>
<script src="<?php echo BASE_URL; ?>assets/js/profile/emergency.js"></script>
<script>
                $('#personal-doc').fileupload({
                    dataType: 'html',
                    add: function (e, data) {
                        data.context = $('<p/>').append('<img src="<?php echo BASE_IMG ?>icon/loader.gif" alt="uploading....">').appendTo($('div.uploading-msg'));
                        data.submit();
                        //$('#personal-info-group').hide();
                    },
                    done: function (e, data) {
                        $('div.uploading-msg p').remove();
                        //$('#personal-info-group').show();
                        var messageDiv = $("div#xzaqt-message-alert");
                        var result = data._response.result;
                        if (result == 1) {
                            result = 'Document Successfully';
                            if (messageDiv.hasClass("alert-warning")) {
                                messageDiv.removeClass("alert-warning")
                                        .addClass('alert-success');
                            }
                        } else {
                            if (messageDiv.hasClass("alert-success")) {
                                messageDiv.removeClass("alert-success")
                                        .addClass('alert-warning');
                            }
                        }
                        messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
                        messageDiv.delay(2000).fadeOut("slow");
                    }
                });

                $('#personal-doc').fileupload({
                    dropZone: $('#dropzone')
                });

                $(document).bind('dragover', function (e) {
                    var dropZone = $('#dropzone'),
                            timeout = window.dropZoneTimeout;
                    if (!timeout) {
                        dropZone.addClass('in');
                    } else {
                        clearTimeout(timeout);
                    }
                    var found = false,
                            node = e.target;
                    do {
                        if (node === dropZone[0]) {
                            found = true;
                            break;
                        }
                        node = node.parentNode;
                    } while (node != null);
                    if (found) {
                        dropZone.addClass('hover');
                    } else {
                        dropZone.removeClass('hover');
                    }
                    window.dropZoneTimeout = setTimeout(function () {
                        window.dropZoneTimeout = null;
                        dropZone.removeClass('in hover');
                    }, 100);
                });

</script>
