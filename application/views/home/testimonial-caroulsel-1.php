<section id="carousel-1">    				
    <div class="row">
        <div class="col-md-12">

            <div class="carousel slide" id="employer-carousel" data-ride="carousel" data-interval="3000">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#employer-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#employer-carousel" data-slide-to="1"></li>
                    <li data-target="#employer-carousel" data-slide-to="2"></li>
                    <li data-target="#employer-carousel" data-slide-to="3"></li>
                    <li data-target="#employer-carousel" data-slide-to="4"></li>

                </ol>
                <!-- Carousel items -->
                <div class="carousel-container">
                    <div class=" carousel-inner">
                        <div class="active item">
                            <div class="profile-circle"> <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>emp.png"> </div>
                            <blockquote> 
                                <p> <i class="pull-left fa fa-quote-left"></i> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                            </blockquote>
                            <h4> HR Manager at Google </h4>
                        </div>
                        <div class="item">
                            <div class="profile-circle"> <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>emp.png"> </div>
                            <blockquote> 
                                <p> <i class="pull-left fa fa-quote-left"></i> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                            </blockquote>
                            <h4> HR Manager at Google </h4>
                        </div>                    
                        <div class="item">
                            <div class="profile-circle"> <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>emp.png"> </div>
                            <blockquote> 
                                <p> <i class="pull-left fa fa-quote-left"></i> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                            </blockquote>
                            <h4> HR Manager at Google </h4>
                        </div>
                        <div class="item">
                            <div class="profile-circle"> <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>emp.png"> </div>
                            <blockquote> 
                                <p> <i class="pull-left fa fa-quote-left"></i> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                            </blockquote>
                            <h4> HR Manager at Google </h4>
                        </div>  
                        <div class="item">
                            <div class="profile-circle"> <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>emp.png"> </div>
                            <blockquote> 
                                <p> <i class="pull-left fa fa-quote-left"></i> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                            </blockquote>
                            <h4> HR Manager at Google </h4>
                        </div>                      
                    </div>
                </div>

            </div>							
        </div>
    </div>
</section>