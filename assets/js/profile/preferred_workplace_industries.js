$(function () {
    var messageDiv = $("div#xzaqt-message-alert");
    $('.preferedIndustry label').on('click', function(){
       if($(this).hasClass( "preferedIndustry-checked")){
           $(this).removeClass("preferedIndustry-checked");
       } else{
           $(this).addClass('preferedIndustry-checked');
       }    
    });
    
    
    $('#preferred-workplace-industries-frm').on('submit',function(e){
        e.preventDefault();
        var data = $(this).serialize();
       
        var url = $(this).attr('action');
                                          
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
            	var progress = results+"%";
            	$('#pregressbar').css("width", progress);
            	$('#content-your-preferred-workplace-industries').hide();
   		$('#content-your-self-video').show();
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);     
                messageDiv.fadeIn("slow").find(".validation_text").text('Prefered Job Industry and workplace added Successfully ');
                messageDiv.delay(1700).fadeOut("slow");
            }
        });
        
    });
   
   
});

