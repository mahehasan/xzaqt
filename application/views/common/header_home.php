<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> XZAQT | <?php echo $title; ?> </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php require_once 'home.css.php'; ?>
    </head>
    <body>
        <header id="top" class=" navbar navbar-static-top bs-docs-nav" role="banner">
            <nav class="navbar navbar-thm navbar-xzaqt navbar-fixed-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class=" navbar-toggle xzq-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="logo" href="#">
                            <img  src="<?php echo BASE_URL; ?>assets/images/logo.png" alt="XZAQT"  >
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="#"> Employers </a></li>
                            <li><a href="#"> Candidates </a></li>
                            <li><a href="#"> How it works </a></li>
                            <li><a href="#"> Pricing </a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">

                            <li>   </li>
                            <li><a id="contact-us" href="#"> Contact us  </a></li>
                            <!--<li><button class="btn btn-flat btn-success"> Book a Demo </button> </li>-->
                            <li>
                                <?php if (isset($_SESSION['email']) != 1) { ?>

                                    <button id="sign-in" class="sign-pass-btn btn  btn-block btn-flat"> Sign In </button>
                                <?php } else { ?>
                                    <a id="log-out" class="btn bg-red " href="<?php echo BASE_URL ?>hauth/logout"> Logout </a>
                                <?php } ?>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
                <?php
                if ($this->session->userdata('isUserConnected') == 1 && $this->session->userdata('user_type') == 1) {
                    if ($show2ndmenu) {
                        ?>
                        <div class="container-fluid">
                            <div class="row">
                                <div id="emp-dashboard-menu" class="navbar navbar-default " role="navigation">
                                    <div class="container-fluid">
                                        <div class="container">
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-emp">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>
                                            <div class="collapse navbar-collapse navbar-emp">
                                                <ul class="nav navbar-nav ">
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-suitcase"></i> Job facts</a></li>
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-calendar"></i> calendar</a></li>
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-clock-o"></i> Timesheets approval</a></li>
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-money"></i> Invoices</a></li>
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-line-chart"></i> Feedback stats</a></li>
                                                    <li><a href="#"><i class="font-icon-style fa fa-fw fa-star-o"></i> Feedback requests</a></li>                                            
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                }
                ?>
            </nav> 


            <script> var BASE_URL = "<?php echo BASE_URL; ?>";</script>            
            <script src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>            
        </header>