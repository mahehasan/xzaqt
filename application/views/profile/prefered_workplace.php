<div class="content" id="content-your-preferred-workplace-industries" style="display: none;">
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-7">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class="fa fa-fw fa-star"></i></span>
                <span class="thm-text "> Your preferred workplace </span>
            </div>
        </div>
        <div class="col-md-5">

        </div>
    </div>
    <form role="form" action="<?php echo BASE_URL ?>candidate/profilesetup/preferred_workplace_industries" method="post" id="preferred-workplace-industries-frm">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-10">
                    <div class="col-md-3">
                        <div class="form-group">
                        <div class="preferedcompanySize text-center">
                        <img name="preferedcompanySize" src="<?php echo BASE_URL; ?>assets/images/theme/icon/small-business-icon.png" alt="SB">
                        <label> Small Business </label>
                        <p> ( up to 500 headcounts ) </p>
                        <input name="preferedcompanySize" value="1"  type="radio">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <div class="preferedcompanySize text-center">
                        <img  src="<?php echo BASE_URL; ?>assets/images/theme/icon/medium-sized.png" alt="SB">
                        <label> Medium Sized </label>
                        <p> ( 500-5000 people ) </p>
                        <input name="preferedcompanySize" value="2"  type="radio">
                        </div>
                        </div>
                    </div>                   
                    <div class="col-md-3">
                        <div class="form-group">
                        <div class="preferedcompanySize text-center">
                        <img name="preferedcompanySize" src="<?php echo BASE_URL; ?>assets/images/theme/icon/a-large-corporation.png" alt="SB">
                        <label> A Large Corporation </label>
                        <p style="color:#fff"> . </p>
                        <input name="preferedcompanySize" value="3"  type="radio">
                        </div>
                        </div>
                    </div>                   
                    <div class="col-md-3">
                        <div class="form-group">
                        <div class="preferedcompanySize text-center">
                        <img name="preferedcompanySize" src="<?php echo BASE_URL; ?>assets/images/theme/icon/global-business.png" alt="SB">
                        <label> A Global Corporation </label>
                        <p style="color:#fff"> . </p>
                        <input name="preferedcompanySize" value="4"  type="radio">
                        </div>
                        </div>
                    </div>                     
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-7">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class="fa fa-fw fa-industry"></i></span>
                <span class="thm-text "> Jobs Industries Preferences </span>
            </div>
        </div>
        <div class="col-md-5">

        </div>
    </div>
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                    <div class="col-md-10">
                        <div class="row">
                            <?php foreach ($industry_type as $industry) { ?>
                            <div class="col-md-3 preferedIndustry"> 
                                <div class="form-group">
                                    <p>
                                    
                                    <label> <input type="checkbox"  name="preferedIndustry[]" value="<?php echo Common::iifMsg($industry,'id'); ?>" > <?php echo Common::iifMsg($industry, 'name'); ?></label>
                                    </p>
                                </div>
                            </div> 
                            <?php } ?>
                        </div>
                    </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div  class="row industry_type "> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div  class="col-md-12 text-center"> 
                                <p>Are you looking for More Industry? Submit them in the field bellow ( separated by comma )</p>
                                <div class="form-group">
                                    <?php echo form_error('IndustryType'); ?>
                                   <div class="row"> 
                                       <div class="col-md-3"> </div>
                                       <div class="col-md-6">
                                       <input name="IndustryType"  type="text" value="" class="form-control" id="qualification">
                                       </div>
                                       <div class="col-md-3"> </div>
                                       
                                   </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        
        <div class="row"> 
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <button type="submit" class="btn btn-theme" name="save" id="save-preferred-workplace-industries"> Save & Next </button>
                </div>
            </div>
        </div>
	</form>
</div> <!-- Employment History End -->  
<script src="<?php echo BASE_URL; ?>assets/js/profile/preferred_workplace_industries.js"></script>