<div class="month-overview">
<div class="row">
    <div class="col-md-6">
        <canvas id="pieChart" style="height:190px"></canvas>                    
    </div>
    <div class="col-md-6">
        <ul class="list-unstyled">
            <li> <i style="color: #7eb66f;" class="fa fa-fw fa-circle"></i> invited to apply or interview <br> <span>40%</span>  </li>
            <li> <i style="color: #dd5f32" class="fa fa-fw fa-circle"></i> Jobs completed  <br> <span>30%</span> </li>
            <li> <i style="color: #69bac8" class="fa fa-fw fa-circle"></i> Running Job 30 <br> <span>30%</span></li>
        </ul>
    </div>  
</div>
</div>