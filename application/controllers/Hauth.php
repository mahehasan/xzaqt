<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class HAuth extends CI_Controller {

    public function __construct() {
        // Constructor to auto-load HybridAuthLib
        parent::__construct();
        $this->load->library('HybridAuthLib');
        $this->load->helper('cookie');
        
    }

    public function index() {
        $user_type = $this->input->post('user_type');
        $this->session->set_userdata('user_type', $user_type);
        $data['title'] = 'Login';
        $this->load->view('common/header_before_log', $data);
        $this->load->view('hauth/login_view', $this->logdata());
        $this->load->view('common/footer_before_log');
    }

    public function login($provider = null) {

        if ($provider) {
            log_message('debug', "controllers.HAuth.login($provider) called");
            try {
                log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');

                if ($this->hybridauthlib->providerEnabled($provider)) {
                    log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
                    $service = $this->hybridauthlib->authenticate($provider);
                    //$this->session->set_userdata('isUserConnected', $service->isUserConnected());
                    if ($service->isUserConnected()) {
                        $this->session->set_userdata('isUserConnected', $service->isUserConnected());
                        $user_profile = $service->getUserProfile();
                        // $user_profile = (array) $_SESSION['user_profile'];
                        $user_profile = (array) $user_profile;
                        $this->session->set_userdata('user_profile', $user_profile);
                        $email = $_SESSION['user_profile']['email'];
                        $this->session->set_userdata('email', $email);
                        // check existor not 
                        $this->load->model('User_model');
                        $user_type = $this->User_model->typeExist();
                        $user_data = $this->User_model->userExist();
//                        echo '<pre>';
//                        print_r($user_data);
//                        echo $user_data->email;
                        //echo $user_data->type;
//                        echo '</pre>';
                        if ($user_data) {

                            $this->session->set_userdata('email', $user_data->email);
                            $this->session->set_userdata('id', $user_data->id);
                            $this->session->set_userdata('user_type', $user_data->type);
                            //Common::redirectTo($user_data->type);
                            Common::redirectTo($_SESSION['user_type']);
                        } else {
                            // redirect('hauth/linkdin_register');
                            $this->User_model->setuser_type();

                            Common::redirectTo($_SESSION['user_type']);
                        }

                        log_message('debug', 'controller.HAuth.login: user authenticated.');

                        log_message('info', 'controllers.HAuth.login: user profile:' . PHP_EOL . print_r($user_profile, TRUE));

                        $data['user_profile'] = $user_profile;
                        //$this->'hauth/done', $data);
                    } else { // Cannot authenticate user
                        show_error('Cannot authenticate user');
                    }
                } else { // This service is not enabled.
                    log_message('error', 'controllers.HAuth.login: This provider is not enabled (' . $provider . ')');
                    show_404($_SERVER['REQUEST_URI']);
                }
            } catch (Exception $e) {
                $error = 'Unexpected error';
                switch ($e->getCode()) {
                    case 0 : $error = 'Unspecified error.';
                        break;
                    case 1 : $error = 'Hybriauth configuration error.';
                        break;
                    case 2 : $error = 'Provider not properly configured.';
                        break;
                    case 3 : $error = 'Unknown or disabled provider.';
                        break;
                    case 4 : $error = 'Missing provider application credentials.';
                        break;
                    case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                        //redirect();
                        if (isset($service)) {
                            log_message('debug', 'controllers.HAuth.login: logging out from service.');
                            $service->logout();
                        }
                        show_error('User has cancelled the authentication or the provider refused the connection.');
                        break;
                    case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                        break;
                    case 7 : $error = 'User not connected to the provider.';
                        break;
                }

                if (isset($service)) {
                    $service->logout();
                }

                log_message('error', 'controllers.HAuth.login: ' . $error);
                show_error('Error authenticating user.');
            }
        } else {
            redirect();
        }
    }

    public function login_email() {

        $data['title'] = 'Login By Email';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|callback_validate_credential');
        $this->form_validation->set_rules('type', 'Registration Type', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_message('type', 'required', 'Select User Type');
        $this->form_validation->set_rules('password', 'password', 'required|md5|trim');

        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $user_type = $this->input->post('type');
            $session_data = array('email' => $email, 'user_type' => $user_type, 'isUserConnected' => 1);
            $this->session->set_userdata($session_data);
            Common::redirectTo($user_type);
        } else {
            $this->load->view('common/header_before_log', $data);
            $this->load->view('hauth/login_view', $this->logdata());
            $this->load->view('common/footer_before_log');
        }
    }

    public function register() {

        $data['title'] = 'Registration Page';
        $this->load->view('common/header_before_log', $data);
        $this->load->view('hauth/register_view', $this->logdata());
        $this->load->view('common/footer_before_log');
    }

    public function linkdin_register() {
        if (isset($_SESSION['isUserConnected'])) {
            $data['title'] = 'Registration Page';
            $this->load->view('common/header_before_log', $data);
            $this->load->view('hauth/linkdin_register_view');
            $this->load->view('common/footer_before_log');
        } else {
            redirect();
        }
    }

    public function setusertype() {
        if (isset($_SESSION['isUserConnected'])) {
            $this->load->library('form_validation');
            $this->load->model('Login_model');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email|is_unique[users.email]');
            $this->form_validation->set_message('is_unique', 'You already registered');
            $this->form_validation->set_rules('type', 'Registration Type', 'required|trim|xss_clean|numeric');

            if ($this->form_validation->run()) {
                $this->load->model('User_model');
                $this->User_model->setuser_type();
//                $this->session->set_userdata('email', $user_data->email);
//                $this->session->set_userdata('user_type', $user_data->type);
                Common::redirectTo($this->input->post('type'));
            } else {

                $data['title'] = 'Registration Page';
                $this->load->view('common/header_before_log', $data);
                $this->load->view('hauth/linkdin_register_view');
                $this->load->view('common/footer_before_log');
            }
        } else {
            redirect();
        }
    }

    public function register_validation() {
        $data['title'] = 'Registration Page';
        $this->load->library('form_validation');
        $this->load->model('Login_model');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('type', 'Registration Type', 'required|trim|xss_clean|numeric');
        $this->form_validation->set_message('type', 'required', 'Select User Type');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        $this->form_validation->set_rules('cpassword', 'Password', 'required|trim|matches[password]');
        if ($this->form_validation->run()) {

            $email = $this->input->post('email');
            // generate random key 
            $key = md5(uniqid());
            // message 
            $message = "<h2>Thank You for Signing up! </h2>";
            $message .= "<p><a href='" . base_url() . "hauth/register_user/$key'>Click here</a> to confirm your account </p>";
            // prepare email
            $this->load->library('email');
            $this->email->from('admin@jcb120.com', 'jcb120.com');
            $this->email->to($email);
            $this->email->subject('Confirm your account');
            $this->email->message($message);
            $this->email->set_mailtype('html');
            // save it database 
            
            if ($this->Login_model->add_temp_users($key)) {
                $registration_success = "Registered ";
                // send email
                if ($this->email->send()) {
                    $this->session->set_userdata('registration_success', $registration_success);
                } else {
                    echo "Could not send the massage";
                }
            } else {
                echo 'Problem Adding to database';
            }
            $this->load->view('common/header_before_log', $data);
            $this->load->view('hauth/register_view', $this->logdata());
            $this->load->view('common/footer_before_log');
        } else {
            $this->load->view('common/header_before_log', $data);
            $this->load->view('hauth/register_view', $this->logdata());
            $this->load->view('common/footer_before_log');
        }
    }

    public function logdata() {
        // Send to the view all permitted services as a user profile if authenticated
        $login_data['providers'] = $this->hybridauthlib->getProviders();
        foreach ($login_data['providers'] as $provider => $d) {
            if ($d['connected'] == 1) {
                $login_data['providers'][$provider]['user_profile'] = $this->hybridauthlib->authenticate($provider)->getUserProfile();
            }
        }
        return $login_data;
    }

    public function logout($provider = null) {
//        $service = $this->hybridauthlib->authenticate($provider);
//        $service->logout();
        session_destroy();
        //$this->session->sess_destroy();
        redirect();
    }

    public function validate_credential() {
        $this->load->model('login_model');
        if ($this->login_model->can_log_in()) {
            return true;
        } else {
            $this->form_validation->set_message('validate_credential', 'incorrect username/password');
            return false;
        }
    }

    public function endpoint() {

        log_message('debug', 'controllers.HAuth.endpoint called.');
        log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: ' . print_r($_REQUEST, TRUE));

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
            $_GET = $_REQUEST;
        }

        log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
        require_once APPPATH . '/third_party/hybridauth/index.php';
    }

    public function members() {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->view('members');
        } else {
            redirect('main/restricted');
        }
    }

    public function restricted() {
        $this->load->view('restricted_view');
    }

    public function signup() {
        $this->load->model('Login_model');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
        $this->form_validation->set_message('is_unique', "That email address already exists");
        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            // generate random key 
            $key = md5(uniqid());
            // message 
            $message = "<h2>Thank You for Signing up! </h2>";
            $message .= "<p><a href='" . base_url() . "hauth/register_user/$key'>Click here</a> to confirm your account </p>";
            // prepare email
            $this->load->library('email');
            $this->email->from('admin@jcb120.com', 'jcb120.com');
            $this->email->to($email);
            $this->email->subject('Confirm your account');
            $this->email->message($message);
            $this->email->set_mailtype('html');
            // save it database 
            if ($this->Login_model->add_temp_users($key)) {
                // send email
                if ($this->email->send()) {
                    echo "The email has beed sent!";
                } else {
                    echo "Could not send the massage";
                }
            } else {
                echo 'Problem Adding to database';
            }
        } else {
            $this->load->view('signup_view');
        }
    }

    public function register_user() {
        $key = $this->uri->segment(3);
        $data['title'] = 'Registration Confirmation';
        $this->load->model('login_model');
        if ($this->login_model->is_valid_key($key)) {
            if ($data['user_data'] = $this->login_model->add_user($key)) {
                $this->session->set_userdata('success', 'Account Varified Successfully');
                $this->load->view('common/header_before_log', $data);
                $this->load->view('hauth/login_view', $this->logdata());
                $this->load->view('common/footer_before_log');
            } else {
                echo "failed to add user, please try again.   ";
            }
        } else {
            redirect('hauth/register');
        }
    }

    public function reset_password() {
        $data = '';
        $data['title'] = 'Reset Password';
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_message('callback_check_exist_email', "That email address is not exists");

        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            $this->load->model('Login_model');

            if ($userdata = $this->Login_model->check_email($email)) {
                // print_r($userdata);
                $this->load->library('email');
                // generate random key 
                $key = md5(uniqid());
                $id = '';
                $id = $userdata[0]->id;

                if ($temp_data = $this->Login_model->add_temp_users($key)) {
                    // message 
                    $message = "<h2>Reset Password! </h2>";
                    $message .= "<p><a href='" . base_url() . "hauth/reset/$id/$key'>Click here</a> to reset Your password  </p>";
                    // prepare email
                    $this->email->from('admin@jcb120.com', 'jcb120.com');
                    $this->email->to($email);
                    $this->email->subject('Reset Password from jcb120.com');
                    $this->email->message($message);
                    $this->email->set_mailtype('html');

                    if ($this->email->send()) {
                        $data['email_exist_message'] = 'Please Check your email<br>'
                                . 'a emai is sent to your email';
                    }
                }
            } else {
                $data['email_exist_message'] = 'Email is not exist';
            }
        }
        // $this->load->view('hauth/reset_pass', $this->logdata());
        $this->load->view('common/header_before_log', $data);
        $this->load->view('hauth/reset_pass');
        $this->load->view('common/footer_before_log');
    }

    public function reset($id = NULL, $key = NULL) {
        $data['title'] = 'Reset Email';
        $this->load->model('Login_model');
        $data = '';
        $data['id'] = $id;
        $data['reset_data'] = $this->Login_model->get_email($id);
        // $this->logdata()
        if ($id != NULL && $key != NULL) {
            if ($this->Login_model->check_temp_data($key)) {
                $data['title'] = 'Reset Email';
                // $this->load->view('final_reset_view', $data); 
                $this->load->view('common/header_before_log', $data);
                $this->load->view('hauth/final_reset_view');
                $this->load->view('common/footer_before_log');
            } else {
                echo 'Invalid Key';
            }
        } else {
            redirect('hauth/login');
        }
    }

    public function reset_end() {
        $this->load->model('Login_model');
        $this->form_validation->set_rules('id', 'Id', 'required|numeric|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
        if ($this->form_validation->run()) {
            $id = $this->input->post('id');
            $password = $this->input->post('password');
            $data = array(
                'password' => md5($password),
            );
            $this->db->where('id', $id);
            $this->db->update('users', $data);
//            redirect('hauth/login');

            $this->load->view('common/header_before_log', $data);
            $this->load->view('hauth/login_view', $this->logdata());
            $this->load->view('common/footer_before_log');
        } else {
            echo 'Form Validation Error';
        }
    }

    public function setusertype_validation() {
        $user_type = $this->input->post('user_type');
        $this->session->set_userdata('user_type', $user_type);
        if (isset($_SESSION['isUserConnected'])) {
            $this->form_validation->set_rules('user_type', 'User Type', 'required|trim|numeric');
            $this->form_validation->set_message('numeric', "Set Correect Type of user");
            if ($this->form_validation->run()) {
                if ($this->form_validation->run()) {
                    $this->load->model('User_model');
                    if ($this->User_model->setuser_type()) {
                        $user_type = $this->input->post('user_type');
                        $this->session->set_userdata('user_type', $user_type);
                        echo $user_type;
                        // Common::redirectTo($this->input->post('user_type'));
                    } else {
                        redirect();
                    }
                } else {
                    redirect();
                }
            }
        }
    }

    /* End of file hauth.php */
    /* Location: ./application/controllers/hauth.php */
}
