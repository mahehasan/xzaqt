$(function () {
    
    $('.status-message-emp').hide();
    var btn;
    $("[name='addAnother']").on('click', function () {
        btn = $(this).val();
    });
    $("[name='save']").on('click', function () {
        btn = $(this).val();
    });

    $('#form-personalInfo').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        data = data + '&btn_type=' + btn
        var url = $(this).attr('action');

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                var url = BASE_URL + "candidate";
                window.location.href = url;
                $('div#pregressbar').append(progress);
            }
        });

    });


});

