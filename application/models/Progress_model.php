<?php

class Progress_model extends CI_Model {

    private $tableName = 'progress_bar';
    private $uid;

    public function __construct() {
        // Call the CI_Model constructor
        $this->uid = Common::user('id');
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add($progress_id) {
        $uid = $this->uid;
        $where = array('user_id' => $uid, 'progress_id' => $progress_id);
        $this->db->where($where);
        $this->db->delete($this->tableName);

        $data = array('user_id' => $uid, 'progress_id' => $progress_id);
        $this->db->insert($this->tableName, $data);
        echo Common::progress();
    }

    public function progresss() {
        $uid = Common::user('id');
        $this->db->where('user_id', $uid);
        $query = $this->db->get('progress_bar');
        $result = $query->result();
        return $result;
    }

    public function save() {
        
    }

}
