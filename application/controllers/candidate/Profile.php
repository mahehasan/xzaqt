<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('profile_model');
    }

    public function index() {
        if ($this->session->userdata('isUserConnected')) {
            $this->indexdata();
        } else {
            redirect('hauth');
        }
    }

    public function indexdata() {
        $this->load->model('Profile_model');
        $data['title'] = 'Profile Public';
        $data['view_name'] = 'profile/profile_public';
        $data['profile'] = $this->Profile_model->basic();
        $data['language'] = $this->Profile_model->langskills_public();
        $data['comskills'] = $this->Profile_model->comskills_public();
        $data['video'] = $this->Profile_model->introduction_video();
        $data['emp_history'] = $this->Profile_model->employe_history();
        $data['education'] = $this->Profile_model->education();
        $data['voluntary_work_causes'] = $this->Profile_model->voluntary_work_causes();
        if ($data['profile']) {
            $today = date_create(date("Y-m-d"));
            $dateOfBirth = date_create($data['profile']->dob);
            $interval = date_diff($dateOfBirth, $today);
            $data['profile']->age = $interval->format('%Y Years %M Month and %D Days');
        } else {
            $data['profile']->age = 'Not Set';
        }
        Template::set_view('common/header_home', $data);
        Template::set_view('profile/profile_public');
        Template::set_view('common/footer_home');
    }

    public function template($data) {
        $this->load->model('Profile_model');
        $this->load->view('common/header_after_reg', $data);
        $this->load->view($data['view_name'], $data);
        $this->load->view('common/footer_after_log');
    }

}
