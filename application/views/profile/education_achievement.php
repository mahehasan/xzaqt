<div class="content">
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-8">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class=" fa fa-fw fa-graduation-cap"></i></span>
                <span class="thm-text "> Education achievement  </span>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>

    <form id="form-education" role="form" action="<?php echo base_url(); ?>candidate/profilesetup/educaton" method="post">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-5 border-left">
                    <div class="form-group">
                        <label for="qualification"> Qualification Name </label> 
                        <?php echo form_error('qualification'); ?>
                        <input name="qualification"  type="text" value="<?php echo $this->input->post('qualification'); ?>" class="form-control" id="qualification" placeholder="Qualification" required>
                    </div>
                    <div class="form-group">
                        <label for="subjectStudied"> Subject Studied </label> 
                        <?php echo form_error('subjectStudied'); ?>
                        <input name="subjectStudied"  type="text" value="<?php echo $this->input->post('subjectStudied'); ?>" class="form-control" id="subjectStudied" placeholder="Subject Studied" required>
                    </div>
                    <div class="form-group">
                        <label for="universityName"> University Name </label> 
                        <?php echo form_error('universityName'); ?>
                        <input name="universityName"  type="text" value="<?php echo $this->input->post('universityName'); ?>" class="form-control" id="roll" placeholder="University Name" required>
                    </div>  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo form_error('grade'); ?>
                                <label>Grade</label>
                                <select name="grade" class="form-control select2" id="grade" required style="width: 100%;" required>
                                    <option value="" > Select a Value </option>
                                    <?php foreach ($grade as $row) { ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->grade . ' (' . $row->numerical_grade . ')'; ?></option>   
                                    <?php } ?>

                                </select>                                
                            </div>                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year of completion </label>
                                <select name="passin_year" class="form-control select2" style="width: 100%;" >
                                    <?php
                                    $i = date('Y');
                                    $v = '';
                                    while ($i > 1900) {
                                        $v = $i;
                                        ?> 

                                        <option value="<?php echo $v; ?>"><?php echo $i--; ?></option>
<?php } ?>
                                </select>
                            </div>                              
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="voluntryWork"> Voluntary work or causes </label> 
<?php echo form_error('voluntryWork'); ?>
                        <div class="row">
                            <label class="col-sm-1 control-label">1</label>
                            <div class="col-sm-11">
                                <input name="voluntryWork[]" value="<?php echo $this->input->post('voluntryWork[0]'); ?>" type="text" class="form-control"><br>
                            </div>                        
                        </div>
                        <div class="row"><br>
                            <label class="col-sm-1 control-label">2</label>
                            <div class="col-sm-11">
                                <input name="voluntryWork[]" value="<?php echo $this->input->post('voluntryWork[1]'); ?>" type="text" class="form-control"> <br>
                            </div>                        
                        </div>
                        <div class="row"><br>
                            <label class="col-sm-1 control-label">3</label>
                            <div class="col-sm-11">
                                <input name="voluntryWork[]" value="<?php echo $this->input->post('voluntryWork[2]'); ?>"  type="text" class="form-control"><br>
                            </div>                        
                        </div>
                        <div class="row"><br>
                            <label class="col-sm-1 control-label">4</label>
                            <div class="col-sm-11">
                                <input name="voluntryWork[]" value="<?php echo $this->input->post('voluntryWork[3]'); ?>" type="text" class="form-control"><br>
                            </div>         
                        </div>                 
                    </div>   
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div class="row"> 
            <div class="col-md-12 text-center">
                <button class="btn btn-theme" name="addEdu"> Add Another</button>
                <button class="btn btn-theme" name="addEduNext"> Save & Next </button>
            </div>
        </div>
    </form>

</div> <!-- Employment History End -->  
<script src="<?php echo BASE_URL; ?>assets/js/profile/education.js"></script>