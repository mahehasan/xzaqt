<div class="content" id="content-jobs-preferences">
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-7">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class="fa fa-fw fa-star"></i></span>
                <span class="thm-text ">  Jobs Preferences </span>
            </div>
        </div>
        <div class="col-md-5">

        </div>
    </div>
    <form role="form" action="<?php echo BASE_URL; ?>candidate/profilesetup/jobs_preferences" method="post" id="jobs-preferences-frm">
    	<div id="jobs-preferences">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-5">
                    
                    <div class="form-group">
                        <label for="rollInterested">Roles your are interested in </label> 
                        <?php echo form_error('rollInterested'); ?>
                        <input name="rollInterested"  type="text" value="" class="form-control" id="rollInterested" required>
                    </div>              
                    <div class="form-group">
                        <label for="preferedWlocation"> Prefered work location  </label> 
                        <?php echo form_error('preferedWlocation'); ?>
                        <input name="preferedWlocation"  type="text" value="" class="form-control" id="preferedWlocation"  required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="minimumRate"> Minimum Rate  </label> 
                            <?php echo form_error('preferedWlocation'); ?>
                            <input name="minimumRate"  type="text" value="" class="form-control" id="minimumRate"  required>
                        </div>                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="preferedRate"> Preferred Rate  </label> 
                                <?php echo form_error('preferedRate'); ?>
                                <input name="preferedRate"  type="text" value="" class="form-control" id="preferedRate" required>
                            </div>                             
                        </div>
                    </div>
                    
                    <div class="form-group">
                            <label for="roleType"> Role Type ( Temporary, Temp to Perm )  </label> 
                            <?php echo form_error('roleType'); ?>
                            <input name="roleType"  type="text" value="" class="form-control" id="roleType" required>
                    </div>                             
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="roleDuration"> Role Duration  </label> 
                            <?php echo form_error('roleDuration'); ?>
                            <input name="roleDuration"  type="text" value="" class="form-control" id="roleDuration"  required>
                        </div>                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="workStartTime"> When Can you start </label> 
                                <?php echo form_error('workStartTime'); ?>
                                <div class="input-group date">
                                    <input name="workStartTime" class="form-control" id="workStartTime" type="text" required>
                                    <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="form-group">
                        <?php echo form_error('workStatus'); ?>
                        <div class="input-group date">
                            <label>
                                Are you currently Employed &nbsp; &nbsp; &nbsp;  <input value="1" type="radio" class="minimal-red" name="workStatus" required > Yes 
                                                            <input value="0" type="radio" class="minimal-red" name="workStatus" required > No
                            </label>
                        </div>
                    </div>                                       
                    
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="benefits"> Benefits  </label> 
                        <?php echo form_error('benefits'); ?>
                        <input name="benefits"  type="text"  class="form-control" id="benefits"  required>
                    </div>
                    <div class="form-group">
                        <label for="pensionsPlan"> Pensions Plan  </label> 
                        <?php echo form_error('pensionsPlan'); ?>
                        <input name="pensionsPlan"  type="text"  class="form-control" id="pensionsPlan"  required>
                    </div> 
                    <div class="form-group">
                        <label for="healthInsurance"> Health Insurance  </label> 
                        <?php echo form_error('healthInsurance'); ?>
                        <input name="healthInsurance"  type="text"  class="form-control" id="healthInsurance"  required>
                    </div>                     
                    <div class="form-group">
                        <label for="lifeInsurance"> Life Insurance  </label> 
                        <?php echo form_error('lifeInsurance'); ?>
                        <input name="lifeInsurance"  type="text"  class="form-control" id="lifeInsurance"  required>
                    </div>   
                    <div class="form-group">
                        <label for="bonusPayment"> Bonus Payment  </label> 
                        <?php echo form_error('bonusPayment'); ?>
                        <input name="bonusPayment"  type="text"  class="form-control" id="bonusPayment"  required>
                    </div>    
                    <div class="form-group">
                        <?php echo form_error('workStatus'); ?>
                        <div class="input-group date">
                            <label>
                                If Yes is your contract <br>likely to be renewed?  &nbsp; &nbsp; &nbsp;  <input value="1" type="radio" class="minimal-red" name="contractFuture" required > Yes 
                                                            <input value="0" type="radio" class="minimal-red" name="contractFuture" required > No
                            </label>
                        </div>
                    </div>                           
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        
        </div>
        <div class="row"> 
            <div class="col-md-12 text-center">
                <button class="btn btn-theme" value="addJobPref" name="addJobPref"> Add Another Jobs Preferences</button>
                <button type="submit" class="btn btn-theme" value="addJobPrefNext" name="addJobPrefNext" id="save-jobs-preferences"> Save & Next </button>
            </div>
        </div>
    </form>
</div> <!-- Employment History End -->
<script src="<?php echo BASE_URL; ?>assets/js/profile/jobs_preference.js"></script>  