<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Employer
 *
 * @author farhad
 */
class Jobposting extends CI_Controller {

    //put your code here
    private $to;

    public function __construct() {
        parent::__construct();
        Common::canview();
        Common::goToCan();

        //$this->load->model('profile_model');
    }

    public function index() {

        $this->create();
    }

    public function create() {
        $data['title'] = 'Post A Job';
        $data['show2ndmenu'] = false;
        $header = 'common/header_home';
        $body = 'employer/profile/jobposting';
        $footer = 'common/footer_after_log';
        Template::set_view($header, $data);
        Template::set_view($body);
        Template::set_view($footer);
    }

    public function add() {
        // formvalidation
        $this->form_validation->set_rules('companyName', 'Company Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('sectorIndustry', 'Sector and Industry', 'trim|xss_clean|required');
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|xss_clean|required');
        $this->form_validation->set_rules('numberOfVacancies', 'Number Of Vacancies', 'trim|xss_clean');
        $this->form_validation->set_rules('workType', 'Work Type', 'trim|xss_clean|required');
        $this->form_validation->set_rules('lengthfrom', 'From', 'trim|xss_clean|required');
        $this->form_validation->set_rules('lengthto', 'To', 'trim|xss_clean|required');
        $this->form_validation->set_rules('country', 'Country', 'trim|xss_clean|required');
        $this->form_validation->set_rules('stateProvince', 'State Province', 'trim|xss_clean');
        $this->form_validation->set_rules('city', 'City', 'trim|xss_clean|required');
        $this->form_validation->set_rules('postCode', 'PostCode', 'trim|xss_clean');
        $this->form_validation->set_rules('startdate', 'Start Date', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hoursPerWeek', 'Hours Per Week', 'trim|xss_clean|required');
        $this->form_validation->set_rules('salarymin', 'Salary Min', 'trim|xss_clean|required');
        $this->form_validation->set_rules('salarymax', 'Salary Max', 'trim|xss_clean|required');
        $this->form_validation->set_rules('paymentType', 'Payment Type', 'trim|xss_clean|required');
        $this->form_validation->set_rules('guaranteeSought', 'Guarantee Sought', 'trim|xss_clean');
        $this->form_validation->set_rules('essentialSkills', 'Essential Skills', 'trim|xss_clean');
        $this->form_validation->set_rules('niceToHave', 'Nice To Have', 'trim|xss_clean');
        $this->form_validation->set_rules('itLiteracy', 'ItLiteracy', 'trim|xss_clean');
        $this->form_validation->set_rules('jobDescription', 'Job Description', 'trim|xss_clean');

        //$this->form_validation->set_message('is_unique', 'This email is already exist');

        if ($this->form_validation->run()) {
            $this->load->model('Jobposting_model');
            if ($this->Jobposting_model->add()) {
                redirect('employer/dashboard');
            }
        } else {
            $this->index();
        }
    }

}
