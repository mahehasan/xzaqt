<?php

class Personal_model extends CI_Model {

    private $tableName = 'personal_info';

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add(){
        $uid = Common::user('id');
        $citizen = $this->input->post('citizenship');
        $passport = $this->input->post('passport');
        $businessName = $this->input->post('businessName');
        $companyStructure = $this->input->post('companyStructure');
        $docfile = $this->input->post('docfile');
        $companyNumber = $this->input->post('companyNumber');
        $uniqueTaxReference = $this->input->post('uniqueTaxReference');
        $vatRegistered = $this->input->post('vatRegistered');
        $doYouRequireAnAcc = $this->input->post('doYouRequireAnAcc');

        $data = array(
            'user_id' => $uid,
            'country' => $citizen,
            'passport' => $passport,
            'business_name' => $businessName,
            'docfile' => $docfile,            
            'company_structure' => $companyStructure,
            'company_number' => $companyNumber,
            'unique_tax_reference' => $uniqueTaxReference,
            'vat_registered' => $vatRegistered,
            'do_you_require_an_account' => $doYouRequireAnAcc
        );
        Common::save($this->tableName, $data);
    }


}
