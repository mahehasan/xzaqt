<?php

/**
 * Description of Common_lib
 *
 * @author farhad
 */
class Common {

    //put your code here

    public static $ci;
    public static $basicInfo;

    public function __construct() {
        self::$ci = & get_instance();
        self::$ci->load->database();
    }

    public static function save($table, $data) {
        $array_num = count($data);
        $array_num_recursive = count($data, COUNT_RECURSIVE);

        if ($array_num == $array_num_recursive) {
            $insert = self::$ci->db->insert($table, $data);
            return $insert;
        } else {
            $insert = self::$ci->db->insert_batch($table, $data);
            return $insert;
        }
    }

    public static function add($table, $data) {
        $array_num = count($data);
        $array_num_recursive = count($data, COUNT_RECURSIVE);

        if ($array_num == $array_num_recursive) {
            $insert = self::$ci->db->insert($table, $data);
            return $insert;
        } else {
            $insert = self::$ci->db->insert_batch($table, $data);
            return $insert;
        }
    }

    public static function edit($table, $data, $where) {
        self::$ci->db->where($where);
        return self::$ci->db->update($table, $data);
    }

    public static function findByEmail($table) {
        if (isset($_SESSION['email'])) {
            $query = self::$ci->db->where('email', $_SESSION['email'])
                    ->get($table);
            $dataByEmail = $query->result();
            if (count($dataByEmail)) {
                return $dataByEmail[0];
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public static function findByEmailBul($table) {
        if (isset($_SESSION['email'])) {
            $query = self::$ci->db->where('email', $_SESSION['email'])
                    ->get($table);
            $dataByEmail = $query->result();
            if (count($dataByEmail)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public static function basicInfo() {
        self::$basicInfo = self::findByEmail('basic_info');
        return self::$basicInfo;
    }

    public static function iifPost($object = NULL, $fieldName = NULL) {
        if (isset($object->$fieldName)) {
            return $object->$fieldName;
        } else {
            if ($fieldName) {
                return self::$ci->input->post($fieldName);
            }
        }
    }

    public static function basic($propertyName = NULL, $errorMsg = NULL) {
        if (isset(self::basicInfo()->$propertyName)) {
            return self::basicInfo()->$propertyName;
        } else {
            return "$errorMsg";
        }
    }

    public static function basicPost($propertyName = NULL, $inputName = NULL) {
        if (isset(self::basicInfo()->$propertyName)) {
            return self::basicInfo()->$propertyName;
        } else {
            if ($inputName) {
                return self::$ci->input->post($inputName);
            }
        }
    }

    public static function users() {
        return self::findByEmail('users');
    }

    public static function user($propertyName) {
        if (isset(self::users()->$propertyName)) {
            return self::users()->$propertyName;
        }
    }

    public static function userId() {
        $userId = self::user('id');
        return $userId;
    }

    public static function iifMsg($object, $propertyName, $errorMsg = NULL) {
        if (isset($object->$propertyName)) {
            return $object->$propertyName;
        } else {
            return $errorMsg;
        }
    }

    public static function findAll($table) {
        $query = self::$ci->db->get($table);
        $allData = $query->result();
        return $allData;
    }

    public static function findByColumn($table, $where) {
        $query = self::$ci->db->where($where)->get($table);
        $data = $query->result();
        if ($query->num_rows()) {
            return $data;
        } else {
            return FALSE;
        }
    }

    public static function skillSet($object, $index, $fluency_level, $checked = NULL) {
        if ($object) {
            if ($object[$index]->fluency_level == $fluency_level) {
                if ($checked == NULL) {
                    return 'active-pro';
                } else {
                    return $checked;
                }
            }
        } else {
            echo false;
        }
    }

    public static function skillCheck($object, $index) {
        if ($object) {
            if ($object[$index]->current_status == 1) {
                return 'checked';
            }
        } else {
            echo false;
        }
    }

    public static function msg() {
        $min = 1000;
        $max = 9999;
        $rand = rand($min, $max);
        return $rand;
    }

    public static function sms($to = NULL, $message = NULL) {
        self::$ci->config->load('twilio');
        self::$ci->load->library('twilio');
        $from = self::$ci->config->item('number');
        $response = self::$ci->twilio->sms($from, $to, $message);
        if ($response->IsError) {
            return 'Error: ' . $response->ErrorMessage;
        } else {
            return 'Sent message to ' . $to;
        }
    }

    public static function leftProfileSettings() {
        $uid = self::user('id');
        $query = self::$ci->db->select('progress_id')->where('user_id', $uid)->get('progress_bar');
        $progress_settings = $query->result();
        $progress_bar_settings = array();
        foreach ($progress_settings as $progress) {
            $progress_bar_set_skills[] = $progress->progress;
        }


        $query = self::$ci->db->select('progress_id')->where('user_id', $uid)->get('progress_bar');
        $progress_bar_skills = $query->result();
        $progress_bar_set_skills = array();
        foreach ($progress_bar_skills as $progress_bar_skill) {
            $progress_bar_set_skills[] = $progress_bar_skill->progress_id;
        }

        return $progress_bar_set_skills;
    }

    public static function progress() {
        $uid = self::user('id');
        $query = self::$ci->db->where('user_id', $uid)
                ->select("p.user_id, SUM(s.progress) as totalProgress")
                ->from('progress_bar as p')
                ->join('settings_progress as s', 'p.progress_id = s.id')
                ->get();
        $progress = $query->result();
        $totalProgress = $progress[0]->totalProgress;
        return $totalProgress;
    }

    public static function maxProgress() {
        $uid = self::user('id');
        $query = self::$ci->db->where('user_id', $uid)
                ->select("user_id, MAX(progress_id) as maxPrrogress")
                ->get('progress_bar');
        $progress = $query->result();
        $maxprogress = $progress[0]->maxPrrogress;
        return $maxprogress;
    }

    public static function canview() {
        if (isset($_SESSION['email']) != 1) {
            redirect();
        }
    }

    public static function whereTogo() {
        if (self::progress() == 100) {
            redirect('candidate/dashboard');
        } else {
            redirect('candidate/profilesetup');
        }
    }

    public static function redirectTo($user_type = NULL) {
        if ($user_type == 1) {
            self::$ci->load->model('employer/Registration_model');
            if (self::$ci->Registration_model->status()) {
                redirect('employer/dashboard');
            } else {
                redirect('employer/registration');
            }
        } elseif ($user_type == 2) {
            self::whereTogo();
        } else {
            session_destroy();
            redirect();
        }
    }

    public static function goToEmp() {
       
        self::$ci->load->model('User_model');
        $user_type = self::$ci->User_model->typeExist();
        if ($user_type == 1) {
            redirect('employer/dashboard');
        }
    }

    public static function goToCan() {
        self::$ci->load->model('User_model');
        $user_type = self::$ci->User_model->typeExist();
        if ($user_type == 2) {
            self::whereTogo();
        }
    }

    public static function checkSession($session_name) {
        if (isset($_SESSION['user_profile'][$session_name])) {
            return $_SESSION['user_profile'][$session_name];
        }
    }

    public static function checkTypeSession($session_name) {
        if (isset($_SESSION[$session_name])) {
            return $_SESSION[$session_name];
        }
    }
    
    public static function showProfile() {
        $userId = self::userId();
        $table = 'upload_img_desc';
        $where = array('user_id' => $userId, 'type_id' => 6);
        $query = self::$ci->db->select('id, user_id, name')
                ->where($where)
                ->get($table);
        $result = $query->result();
        
        if ($result) {
            return 'uploads/'.$result[0]->name;
          
        } else {
            return false;
        }
    }
    
    public static function labeltoText($label) {
        $array = array(
            1 => 'Elementary Proficiency',
            2 => 'Limited Working Proficiency',
            3 => 'Minimum Professional Proficiency',
            4 => 'Full Professional Proficiency',
            5 => 'Native or Bilingual Proficiency',
        );
        return $array[$label];
    }
    
    

}
