$(function () {

    $('#self-video-frm').on('submit', function (e) {
        e.preventDefault();
        var data = $('#self-video-frm').serialize();

        var url = $(this).attr('action');

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType:'html',
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                $('#content-your-self-video').hide();
                $('#content-your-quiz').show();
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);

            }
        });
    });
});

