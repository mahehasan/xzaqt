<?php 
if(isset($_SESSION['registration_success'])){
    echo '<section class="content">
    <div class="alert alert-error alert-dismissable callout callout-info">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    <h4> Registered succesfully</h4>'."<p><a href= ".base_url()."hauth>Click here To login</a></p>";
    echo '</div>';
  unset($_SESSION['registration_success']);      
}

?>
<div class="login-box">
    <div class="login-logo">
        Reset Form
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php if(validation_errors()){
            echo '<div class="alert alert-error alert-dismissable">'.
            '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            echo validation_errors();
            echo '</div>';
        } ?>
        <?php  
            $attributes = array('role'=>'form', 'onsubmit' => 'return validateForm()');
            echo form_open('hauth/reset_password', $attributes);
        ?>
        <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-6">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Email</button>
            </div>
            <!-- /.col -->
        </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
