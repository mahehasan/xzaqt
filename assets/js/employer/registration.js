$(document).ready(function () {
    var formToCheck = $('#form-registration');
    formToCheck.bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            faxNumber: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your Fax Number'
                    },
                    numeric: {
                        message: 'Please supply your A number'
                    },
                }
            },
        }
    })
            .on('success.form.bv', function (e) {
                $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
//                $.post($form.attr('action'), $form.serialize(), function (result) {
//                    
//                });
            });

    $('.status-message-emp').hide();
//    formToCheck.on('submit', function (e) {
//        e.preventDefault();
//        var url = $(this).attr('action');
//        var data = $(this).serialize();
//        // data = data + '&createNewUser=' + submitValue
//        // ajax submition 
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: data,
//            cache: false,
//            success: function (results) {
//                
//                 $('div.uploading-msg p').remove();
//                $('#file-upload-group').show();
//                var messageDiv = $("div#xzaqt-message-alert");
//                var result = results;
//                if (result == 1) {
//                    result = 'Company video presentation uploaded successfully';
//                    if (messageDiv.hasClass("alert-warning")) {
//                        messageDiv.removeClass("alert-warning")
//                                .addClass('alert-success');
//                    }
//                } else {
//                    if (messageDiv.hasClass("alert-success")) {
//                        messageDiv.removeClass("alert-success")
//                                .addClass('alert-warning');
//                    }
//                }
//
//                messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
//                messageDiv.delay(3000).fadeOut("slow");              
//  
//            }
//        });
//
//    });


    $('#form-companyuser-registration').on('hidden.bs.modal', function () {
        $('#form-companyuser-registration').formValidation('resetForm', true);
    });

    $('#form-companyuser-registration').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstName: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your At Least 2 text '
                    }
                }
            },
            lastName: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your At Least 2 text '
                    }

                }
            },
            jobPosition: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your Fax Number'
                    }
                }
            },
            email: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: ' Please fill the field'
                    },
                    emailAddress: {
                        message: 'Please insert an email address '
                    },
                }
            },
            password: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Please put at least 6 character'
                    },
                    notEmpty: {
                        message: 'Please fill the field'
                    }

                }
            }, cpassword: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Please put at least 6 character'
                    },
                    notEmpty: {
                        message: 'Please fill the field'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm must be the same'
                    }

                }
            },
        }
    })
            .on('success.form.bv', function (e, data) {
                $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                $('#form-companyuser-registration').data('bootstrapValidator').resetForm();

                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);


                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {

                });
            });
    $('span.email-error').hide();
    $('input#email').on('click', function () {
        $('span.email-error').text(' ');

    });
    $('#form-companyuser-registration').on('submit', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var submitValue = $('#button-create-user').attr('name');
        var data = $(this).serialize();
        data = data + '&createNewUser=' + submitValue

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: "json",
            success: function (results) {
                var response = results;
               
                $('span.email-error').text(' ');
                $('.email-error').append(response.email);
                $('span.email-error').show();
                $('#add-company-user-form').modal('show')

            }
        });
    });


    /**************** Video Presentation upload start ******************/
    if ($('#company-video-presentation').length != 0) {
        var messageDiv = $("div#xzaqt-message-alert");
        console.log(messageDiv)
        var companyVideoUpload = $('#company-video-presentation');
        var lodinDiv = $('input#company-video-presentation').closest('div').parent().find('.uploading-msg');
        console.log(lodinDiv);
        companyVideoUpload.fileupload({
            dataType: 'html',
            add: function (e, data) {
                data.context = $('<p/>').append('<img src=' + IMG + 'icon/loader.gif' + ' ' + "alt='uploading....'>").appendTo(lodinDiv);
                data.submit();
                $('#file-upload-group').hide();
            },
            done: function (e, data) {
                $('div.uploading-msg p').remove();
                $('#file-upload-group').show();
                var messageDiv = $("div#xzaqt-message-alert");
                var result = data._response.result;
                if (result == 1) {
                    result = 'Company video presentation uploaded successfully';
                    if (messageDiv.hasClass("alert-warning")) {
                        messageDiv.removeClass("alert-warning")
                                .addClass('alert-success');
                    }
                } else {
                    if (messageDiv.hasClass("alert-success")) {
                        messageDiv.removeClass("alert-success")
                                .addClass('alert-warning');
                    }
                }

                messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
                messageDiv.delay(3000).fadeOut("slow");

            }
        });

        companyVideoUpload.fileupload({
            dropZone: $('#dropzone')
        });

        $(document).bind('dragover', function (e) {
            var dropZone = $('#dropzone'),
                    timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            var found = false,
                    node = e.target;
            do {
                if (node === dropZone[0]) {
                    found = true;
                    break;
                }
                node = node.parentNode;
            } while (node != null);
            if (found) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });
    }
    /**************** Video Presentation upload end*********************/

    /**************** Video Presentation upload start ******************/
    if ($('#company-picture').length != 0) {
        var companyPicture = $('#company-picture');
        var lodinDiv2 = $('input#company-picture').closest('div').parent().find('.uploading-msg');
        companyPicture.fileupload({
            dataType: 'html',
            add: function (e, data) {
                data.context = $('<p/>').append('<img src=' + IMG + 'icon/loader.gif' + ' ' + "alt='uploading....'>").appendTo(lodinDiv2);
                data.submit();
                $('#file-upload-group').hide();
            },
            done: function (e, data) {
                $('div.uploading-msg p').remove();
                $('#file-upload-group').show();
                var messageDiv = $("div#xzaqt-message-alert");
                var result = data._response.result;
                if (result == 1) {
                    result = 'Company Image Uploaded Successfully';
                    if (messageDiv.hasClass("alert-warning")) {
                        messageDiv.removeClass("alert-warning")
                                .addClass('alert-success');
                    }
                } else {
                    if (messageDiv.hasClass("alert-success")) {
                        messageDiv.removeClass("alert-success")
                                .addClass('alert-warning');
                    }
                }
                messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
                messageDiv.delay(3000).fadeOut("slow");
            }
        });

        companyPicture.fileupload({
            dropZone: $('#dropzone')
        });

        $(document).bind('dragover', function (e) {
            var dropZone = $('#dropzone'),
                    timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            var found = false,
                    node = e.target;
            do {
                if (node === dropZone[0]) {
                    found = true;
                    break;
                }
                node = node.parentNode;
            } while (node != null);
            if (found) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });
    }
    /**************** Video Presentation upload end*********************/





});
