<div class="login-box">
    <div class="login-logo">
        Register Form
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php
        if (validation_errors()) {
            echo '<div class="alert alert-error alert-dismissable">' .
            '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            echo validation_errors();
            echo '</div>';
        }
        if (isset($_SESSION['registration_success'])) {
            echo '    <div class="alert alert-success">
		          <strong>Success!</strong> A email is sent to your account.
		        </div>';
            unset($_SESSION['registration_success']);
        }
        ?>    
        <?php
        $attributes = array('role' => 'form', 'data-toggle' => "validator", 'id' => 'form-registration');
        echo form_open('hauth/register_validation', $attributes);
        ?>
        <div class="form-group">
            <input value="<?php echo $this->input->post('email');  ?>" type="email" name="email" class="form-control" id="email" placeholder="Email" required>
<!--            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
        </div>
        <div class="form-group">
            <select  class="form-control" name="type" required="">
                <option value="" disabled selected hidden> Select User Type  </option>
                <option value="2" <?php if($this->input->post('type') == 2){ echo 'selected'; }  ?>> Candidate </option>
                <option value="1" <?php if($this->input->post('type') == 1){ echo 'selected'; }  ?>> Employer </option>
            </select>
        </div>        
        <div for="password" class="form-group">
            <input  type="password" value="<?php echo $this->input->post('password');  ?>" name="password" class="form-control" id="password" placeholder="Password" required>
        </div>
        <div for="cpassword" class="form-group">
            <input type="password" value="<?php echo $this->input->post('cpassword');  ?>" name="cpassword" id="cpassword" data-match="#password" data-match-error="Whoops, these don't match" class="form-control" placeholder="Retype password" required>
<!--            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
        </div>
        <div class="row">
            <div class="col-xs-8">
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat"> Register </button>
            </div>
            <!-- /.col -->
        </div>

        </form>

        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <!--<a href="<?php //echo base_url();  ?>hauth/" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Sign in using Linkedin</a>-->
            <?php
            foreach ($providers as $provider => $data) {
                if ($data['connected']) {
                    echo anchor('hauth/logout/' . $provider, '<i class="fa fa-linkedin"></i>Log out using Linkedin ' . $provider, array('class' => 'overlay_form btn btn-block btn-social btn-linkedin btn-flat connected'));
                } else {
                    echo anchor('hauth/login/' . $provider, '<i class="fa fa-linkedin"></i>Sign in using Linkedin', array('class' => 'overlay_form btn btn-block btn-social btn-linkedin btn-flat'));
                }
            }
            ?>
        </div>
        <!-- /.social-auth-links -->

        <a href="<?php echo base_url() . 'hauth/reset_password'; ?>">I forgot my password</a><br>

    </div>
    <!-- /.login-box-body -->
</div>
