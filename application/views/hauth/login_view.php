<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="login-box">
  <div class="login-logo">
    Login
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    
    <?php if(validation_errors()){
        echo '<div class="alert alert-error alert-dismissable">'.
        '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
        echo validation_errors();
        echo '</div>';
    } 
        if (isset($_SESSION['success'])) {
            echo '<div class="alert alert-success">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		 <strong>Success!</strong> . You have varified successfully. </div>';
            unset($_SESSION['success']);
        }    
    
    ?>
  
      <?php  
            $attributes = array('role'=>'form', 'onsubmit' => 'return validateForm()');
            echo form_open('hauth/login_email', $attributes);
       ?>
      <div class="form-group has-feedback">
          <input value="<?php if(isset($user_data)){ echo $user_data['email']; } ?>" type="email" name="email" class="form-control" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
        <div class="form-group">
            <select  class="form-control" name="type" required="">
                <option value="" disabled selected hidden> Select User Type  </option>
                <option value="2" <?php if(isset($user_data)){ if($user_data['type'] == 2){ echo 'selected';}} ?>> Candidate </option>
                <option value="1" <?php if(isset($user_data)){ if($user_data['type'] == 1){ echo 'selected';}} ?>> Employer </option>
            </select>
        </div>   
      <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
                <input class="remember" type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <!--<a href="<?php //echo base_url();?>hauth/" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Sign in using Linkedin</a>-->
      <?php 
            foreach($providers as $provider => $data) {
                    if ($data['connected']) {
                            echo anchor('hauth/logout/'.$provider,'<i class="fa fa-linkedin"></i>Log out using Linkedin '.$provider, array('class' => 'btn btn-block btn-social btn-linkedin btn-flat connected'));
                    } else {
                            echo anchor('hauth/login/'.$provider,'<i class="fa fa-linkedin"></i>Sign in using Linkedin', array('class' => 'btn btn-block btn-social btn-linkedin btn-flat'));
                    }
            }
      ?>
    </div>
    <!-- /.social-auth-links -->

    <a href="<?php echo base_url();?>hauth/reset_password">I forgot my password</a><br>
    <a href="<?php echo base_url();?>hauth/register" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>