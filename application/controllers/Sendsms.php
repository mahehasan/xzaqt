<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sendsms extends CI_Controller {

    private $to;
    private $table;
    private $uid;

    public function __construct() {
        Common::canview();
        parent::__construct();
        $this->to = Common::basic('mobile');
        $this->msg = Common::msg();
        $this->uid = Common::userId();
        $this->table = 'basic_info';
    }

    public function index() {

        $this->load->library('twilio');
        $from = '+442033225743';
        $to = $this->to;
        $message = $this->msg;
        $table = $this->table;
        $uid = $this->uid;
        $response = $this->twilio->sms($from, $to, $message);
        if ($response->IsError) {
            echo 'Error: ' . $response->ErrorMessage;
        } else {
            echo ' send to ' . $to;
        }
        $data = array('validation_code' => $this->msg, 'mobile_validation' => 0);
        $where = array('user_id' => $uid, 'mobile' => $table);

        $this->db->where($where);
        $this->db->update($table, $data);
       
        
    }

    public function send() {

//        echo $this->to;
//        echo $this->msg;

//		$this->load->library('twilio');
//
//		$from = '+442033225743';
//		$to = '+8801729963312';
//		$message = '1478';
//
//		$response = $this->twilio->sms($from, $to, $message);
//
//
//		if($response->IsError)
//			echo 'Error: ' . $response->ErrorMessage;
//		else
//			echo 'Sent message to ' . $to;
    }

}

/* End of file twilio_demo.php */