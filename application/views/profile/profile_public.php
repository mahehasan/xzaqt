<!-- Full Width Column -->
<div class="container">
    <section class="profile-content profile-info margin-top-50">
        <div class="row">
            <div class="col-md-12">
                <div class="row" >
                    <div class="col-md-2">
                        <div class="public-profile-img text-center">
                            <img src="<?php
                            echo BASE_URL;
                            if (Common::showProfile()) {
                                echo Common::showProfile();
                            } else {
                                echo 'assets/images/avatar_img.png';
                            }
                            ?>">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>
                                    <?php echo Common::iifMsg($profile, 'first_name', 'First Name') . ' '; ?>
                                    <?php echo Common::iifMsg($profile, 'last_name', 'Last Name') ?>

                                </h1>
                                <ul>
                                    <li> <span> Interested in : </span> <?php echo Common::iifMsg($profile, 'job_title', 'JOb Title') . ' '; ?> </li>
                                    <li> <span> Previous : </span> Product Manager at Google </li>
                                    <li> <span> Location : </span> 
                                        <?php
                                        echo Common::iifMsg($profile, 'city', 'Please Set BasicInfo') . ', ';
                                        echo Common::iifMsg($profile, 'country', 'Please Set BasicInfo');
                                        ?>
                                    </li>
                                    <li> <span> Age : </span> 
                                        <?php
                                         echo Common::iifMsg($profile, 'age', 'Please Set BasicInfo');
                                        ?>
                                    </li>                                      
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li> <span> Job awarded :  </span> 11 </li>
                                    <li> <span> Recommendation :  </span>  99% &nbsp;   
                                        <span> Reviews: </span> 9 
                                        <span class="review"> &nbsp; 
                                            <i class="fa fa-fw fa-star"></i>
                                            <i class="fa fa-fw fa-star"></i>
                                            <i class="fa fa-fw fa-star"></i>
                                            <i class="fa fa-fw fa-star"></i>
                                            <i class="fa fa-fw fa-star-half-empty"></i>
                                        </span>
                                    </li>
                                </ul>                                    
                            </div>   
                        </div>
                    </div>
                    <div class="col-md-3"> 
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="hourly-rate text-center">
                                    Hourly Rate <br>
                                    <span>$ 50<sub>/h</sub> </span> 
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <a class="btn btn-theme btn-block"> Invite to interview </a>
                                <a class="btn btn-theme-2 btn-block"> Invite to apply </a>
                                <a class="btn btn-theme-3 btn-block"> Hire the candidate </a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>            
    </section>

    <section class="profile-content margin-top-25 summary-of-strength">
        <div class="row">
            <div class="col-md-12">
                <span class=""> Summary of strengths test : </span> Personaable, Humourous, Adaptable, Personable, Humourous, Adaptable
            </div>
        </div>
    </section>  

    <section class="first-set">
        <div class="row" >
            <div class="col-md-6">  
                <h3><i class="fa fa-fw fa-video-camera "></i>  View <!-- her --> introduction </h3> 
                <div class="profile-video">

                    <?php if ($video) { ?>
                        <video width="100%" height="100%" controls>
                            <source src="<?php echo BASE_URL . 'uploads/' . $video->name; ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video> 
                    <?php } else { ?>
                        <img height="100%" width="100%" src="<?php echo BASE_IMG; ?>video.png" alt="VIDEO">
                    <?php } ?>

                </div>
            </div>
            <div class="col-md-6"> 
                <h3><i class="fa fa-fw fa-flash "></i> Skills</h3>   
                <div class="profile-content profile-skills-list">
                    <?php foreach ($comskills as $comskill) { ?>
                        <span class="btn btn-theme"><?php echo Common::iifMsg($comskill, 'name') ?></span>                 
                    <?php } ?>

                </div>

                <div class="row">
                    <div class="col-md-12 languages">
                        <h3><i class="fa fa-fw fa-comment "></i> Languages </h3> 

                        <div class="profile-content col-md-12 languages">
                            <div class="row text-center">
                                <?php foreach ($language as $lang) { ?>
                                    <div class="col-md-4 ">
                                        <img height="25%" width="25%" src="<?php echo BASE_IMG; ?>flags/flag.png ">
                                        <h4> <?php echo Common::iifMsg($lang, 'name') ?></h4>
                                        <p>  <?php echo Common::labeltoText(Common::iifMsg($lang, 'fluency_level')) ?> </p>    
                                    </div> 
                                <?php } ?>                                                       
                            </div>
                        </div>  
                    </div> 
                </div>
            </div>       
        </div>
    </section>        
    <section class="second-set">
        <div class="row" >
            <div class="col-md-6">  
                <h3><i class="fa fa-fw fa-video-camera "></i>  Employments </h3> 
                <div class="profile-content">
                    <ul class="timeline">
                        <?php
                        if ($emp_history) {
                            foreach ($emp_history as $history) {
                                ?> 

                                <li>
                                    <i class="fa"></i>
                                    <div class="timeline-item">
                                        <span class="time"></i> 
                                            <?php
                                            $from_date = Common::iifMsg($history, 'from_date', 'from');
                                            echo substr($from_date, -4);
                                            ?>  
                                            <?php
                                            $to_date = Common::iifMsg($history, 'to_date', 'from');
                                            if (substr($to_date, -4)) {
                                                echo ' - ' . substr($to_date, -4);
                                            }
                                            ?>   
                                        </span>
                                        <div class="timeline-header">
                                            <h3> <?php echo Common::iifMsg($history, 'roll') ?>  </h3>
                                            <span> <?php echo Common::iifMsg($history, 'company_name') ?> </span>
                                        </div>
                                        <div class="timeline-body">
                                            Tasks & achievements : 


                                            <dl class="dl-horizontal text-left ">
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd>  <?php echo Common::iifMsg($history, 'achievement_accomplishment') ?>  </dd>
                                            </dl>
                                            <!-- <dl class="dl-horizontal text-left ">
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd> Fill roles quickly with our  </dd>
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd> Fill roles quickly with our   </dd>
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd> Fill roles quickly with our   </dd>
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd> Fill roles quickly with our  </dd>
                                            </dl>-->
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-6"> 
                <h3><i class="fa fa-fw fa-graduation-cap "></i>  Education </h3> 
                <div class="profile-content">
                    <ul class="timeline">
                        <?php
                        if ($education) {
                            foreach ($education as $edu) {
                                ?> 
                                <li>
                                    <i class="fa"></i>
                                    <div class="timeline-item">
                                        <span class="time"></i> 
                                            <?php
                                            $from_date = Common::iifMsg($edu, 'passing_year', 'Passing Year');
                                            echo substr($from_date, -4);
                                            ?>  
                                            <?php
//                                            $to_date = Common::iifMsg($history, 'to_date', 'from');
//                                            if (substr($to_date, -4)) {
//                                                echo ' - ' . substr($to_date, -4);
//                                            }
                                            ?>  


                                        </span>
                                        <div class="timeline-header">
                                            <h3> <?php echo Common::iifMsg($edu, 'qualification_name'); ?>  </h3>
                                            <span> <?php echo Common::iifMsg($edu, 'university_name'); ?>   </span>
                                        </div>
                                        <div class="timeline-body">
                                            Subject Studied :
                                            <dl class="dl-horizontal text-left ">
                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd>  <?php echo Common::iifMsg($edu, 'subject_studied'); ?>  </dd>
                                                
                                                
<!--                                                <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                                <dd> Fill roles quickly with our  </dd>                                                -->
                                                
                                            </dl>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>                       
<!--                        <li>
                            <i class="fa"></i>
                            <div class="timeline-item">
                                <span class="time"></i> 2003 - 2005</span>
                                <div class="timeline-header">
                                    <h3> Bachelor in Economics  </h3>
                                    <span> New York University </span>
                                </div>
                                <div class="timeline-body">
                                    Subject Studied : 
                                    <dl class="dl-horizontal text-left ">
                                        <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                        <dd> Fill roles quickly with our  </dd>
                                        <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                        <dd> Fill roles quickly with our   </dd>
                                        <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                        <dd> Fill roles quickly with our   </dd>
                                        <dt> <span class="glyphicon glyphicon-ok"></span> </dt>
                                        <dd> Fill roles quickly with our  </dd>
                                    </dl>
                                </div>
                            </div>
                        </li>-->
                    </ul>
                </div>
            </div>       
        </div> 
    </section>
    <section class="third-set">
        <div class="row" >
            <div class="col-md-12">  
                <h3><i class="fa fa-fw fa-star-o "></i>  Top Skills </h3> 
                <div class="row">
                    <div class="col-md-3">
                        <div class="profile-content">
                            <div class="panel">
                                <div class="panel-heading text-center">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-skill-test" style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>                                           

                                </div>
                                <div class="panel-body">
                                    <p>4.75/5</p>
                                    <p>Management</p> 
                                    <p>Skills Test</p>
                                </div>
                                <div class="panel-footer">
                                    <p>Top 1%</p>
                                    <p> Taken 2 years ago </p>
                                </div>
                            </div>                          
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="profile-content">
                            <div class="panel">
                                <div class="panel-heading text-center">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-skill-test" style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>                                           

                                </div>
                                <div class="panel-body">
                                    <p>4.5/5</p>
                                    <p> Economics </p> 
                                    <p>Skills Test</p>
                                </div>
                                <div class="panel-footer">
                                    <p>Top 1%</p>
                                    <p> Taken 1 years ago </p>
                                </div>
                            </div>                          
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="profile-content">
                            <div class="panel">
                                <div class="panel-heading text-center">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-skill-test" style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>                                           

                                </div>
                                <div class="panel-body">
                                    <p>5/5</p>
                                    <p>Communication</p> 
                                    <p>Skills Test</p>
                                </div>
                                <div class="panel-footer">
                                    <p>Top 1%</p>
                                    <p> Taken 2 years ago </p>
                                </div>
                            </div>                          
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="profile-content">
                            <div class="panel">
                                <div class="panel-heading text-center">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-skill-test" style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>                                           

                                </div>
                                <div class="panel-body">
                                    <p>4.75/5</p>
                                    <p>Management</p> 
                                    <p>Skills Test</p>
                                </div>
                                <div class="panel-footer">
                                    <p>Top 1%</p>
                                    <p> Taken 2 years ago </p>
                                </div>
                            </div>                          
                        </div>
                    </div>                           
                </div>
            </div>

        </div> 
    </section>   
    <section class="fourth-set">
        <div class="row">  
            <div class="col-md-12">  
                <h3><i class="fa fa-fw fa-heart-o "></i>  Voluntary causes and other experience </h3> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="profile-content">
                    <dl class="dl-horizontal text-left ">
                        <?php if($voluntary_work_causes){
                                foreach ($voluntary_work_causes as $cause) {   ?>
                        <dt><span class="glyphicon glyphicon-ok"></span></dt>
                        <dd> <?php echo Common::iifMsg($cause, 'name') ?> </dd>
                        <?php 
                            }}
                        ?>
                    </dl>
                </div>
            </div>
        </div>
    </section>
</div>

