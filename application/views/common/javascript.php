<!-- jQuery 2.2.0 -->
<script src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

<!-- iCheck -->
<script src="<?php echo BASE_URL; ?>assets/plugins/iCheck/icheck.min.js"></script>


<!-- Select Country -->
<script src="<?php echo BASE_URL; ?>assets/plugins/countryselect/countrySelect.min.js"></script> 
<script src="<?php echo BASE_URL; ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- select 2-->
<script src="<?php echo BASE_URL; ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.fileupload.js"></script>

<!-- date and calender -->
<script src="<?php echo BASE_URL; ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- bootstrap datepicker -->
<script src="<?php echo BASE_URL; ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/bootstrap-validatior/bootstrapvalidator.min.js"></script>
<script src="<?php echo BASE_URL ?>assets/js/employer/registration.js"></script>
<script src="<?php echo BASE_URL; ?>assets/js/custom.js"></script>


