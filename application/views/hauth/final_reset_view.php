<?php 
if(isset($_SESSION['registration_success'])){
    echo '<section class="content">
    <div class="alert alert-error alert-dismissable callout callout-info">
    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
    <h4> Registered succesfully</h4>'."<p><a href= ".base_url()."hauth>Click here To login</a></p>";
    echo '</div>';
  unset($_SESSION['registration_success']);      
}

?>
<div class="login-box">
    <div class="login-logo">
         Reset Password
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <!--<p class="login-box-msg">Sign in to start your session</p>-->

        <?php if(validation_errors()){
            echo '<div class="alert alert-error alert-dismissable">'.
            '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
            echo validation_errors();
            echo '</div>';
        } ?>
        <?php  
            $attributes = array('role'=>'form', 'onsubmit' => 'return validateForm()');
            echo form_open('hauth/reset_end', $attributes);
        ?>
        <div class="form-group has-feedback">
            <input id="id" class="form-control input-sm" type="hidden" name="id" value="<?php if(isset($id)){echo $id;}?>">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="cpassword" class="form-control" placeholder="Retype password" required>
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox"> Remember Me
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
        </form>

        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <!--<a href="<?php //echo base_url(); ?>hauth/" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Sign in using Linkedin</a>-->
            <?php
//            foreach ($providers as $provider => $data) {
//                if ($data['connected']) {
//                    echo anchor('hauth/logout/' . $provider, '<i class="fa fa-linkedin"></i>Log out using Linkedin ' . $provider, array('class' => 'btn btn-block btn-social btn-linkedin btn-flat connected'));
//                } else {
//                    echo anchor('hauth/login/' . $provider, '<i class="fa fa-linkedin"></i>Sign in using Linkedin', array('class' => 'btn btn-block btn-social btn-linkedin btn-flat'));
//                }
//            }
            ?>
        </div>
        <!-- /.social-auth-links -->

        <a href="<?php echo base_url().'hauth/reset_password';?>">I forgot my password</a><br>

    </div>
    <!-- /.login-box-body -->
</div>
