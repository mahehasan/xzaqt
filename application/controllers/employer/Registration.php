<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Employer
 *
 * @author farhad
 */
class Registration extends CI_Controller {

    //put your code here
    private $to;

    public function __construct() {
        parent::__construct();
        Common::canview();
        Common::goToCan();
        $this->load->model('employer/Registration_model');
    }

    public function index() {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        die();
        
        $data['title'] = 'Registration';
        $data['show2ndmenu'] = false;
        $header = 'common/header_home';
        $body = 'employer/profile/registration';
        $footer = 'common/footer_after_log';
        Template::set_view($header, $data);
        Template::set_view($body);
        Template::set_view($footer);
    }

    public function create() {
        $this->form_validation->set_rules('companyName', 'Company Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('industry', 'Industry', 'trim|xss_clean|required');
        $this->form_validation->set_rules('structure', 'Structure', 'trim|xss_clean|required');
        $this->form_validation->set_rules('preferedcompanySize', 'Prefered Company Size', 'trim|numeric|xss_clean|required');
        $this->form_validation->set_rules('mainTelephone', 'Main Telephone', 'trim|xss_clean|required');
        $this->form_validation->set_rules('website', 'Website', 'trim|xss_clean|required');
        $this->form_validation->set_rules('buildingHouseNo', 'Building House No', 'trim|xss_clean|required');
        $this->form_validation->set_rules('streetName', 'Street Name', 'trim|xss_clean|required');
        $this->form_validation->set_rules('addressLine', 'Address Line', 'trim|xss_clean|required');
        $this->form_validation->set_rules('postalTown', 'Postal Town', 'trim|xss_clean|required');
        $this->form_validation->set_rules('companyBiography', 'Company Biography', 'trim|xss_clean|required');
        $this->form_validation->set_rules('cultureAndValues', 'Culture And Values', 'trim|xss_clean|required');

        //$this->form_validation->set_message('is_unique', 'This email is already exist');

        if ($this->form_validation->run()) {
            if($this->Registration_model->add()){
                redirect('employer/dashboard');
            } 
        } else {
            $this->index();
        }
        
    }

    public function createCompanyUser() {
        if (isset($_POST['createNewUser'])) {
            $this->form_validation->set_rules('firstName', 'First Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|xss_clean|required');
            $this->form_validation->set_rules('jobPosition', 'Job Position', 'trim|xss_clean|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|required|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password Confirmation', 'trim|xss_clean|required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
            $this->form_validation->set_message('is_unique', 'This email is already exist');
            if ($this->form_validation->run()) {
                $this->load->model('User_model');
                $this->User_model->addCUser();
            } else {
                $error = array();
                $error['firstName'] =  form_error('firstName');
                $error['lastName'] =  form_error('lastName');
                $error['jobPosition'] =  form_error('jobPosition');
                $error['email'] =  form_error('email');
                $error['password'] =  form_error('password');
                $error['cpassword'] =  form_error('cpassword');
                echo json_encode($error);
            }
        } else{
        redirect();
        }
    }

}
