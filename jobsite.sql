-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2016 at 01:45 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `ch_basic_info`
--

CREATE TABLE `ch_basic_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_title` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `fore_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `image` varchar(50) NOT NULL,
  `email` varchar(45) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `validation_code` int(4) NOT NULL DEFAULT '1000',
  `mobile_validation` int(10) NOT NULL,
  `street_address` varchar(30) NOT NULL,
  `address_2` varchar(250) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(2) NOT NULL,
  `postal_code` varchar(25) NOT NULL,
  `fax_number` varchar(25) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_basic_info`
--

INSERT INTO `ch_basic_info` (`id`, `user_id`, `job_title`, `first_name`, `fore_name`, `last_name`, `dob`, `image`, `email`, `mobile`, `validation_code`, `mobile_validation`, `street_address`, `address_2`, `city`, `country`, `postal_code`, `fax_number`, `registration_date`) VALUES
(1, 1, 'Project Manager', 'SM Farhad', 'Shah', 'Hossain', '1989-07-20', '', 'farhad1556@gmail.com', '01729963312', 9145, 0, 'street address', 'Address 2', 'Dhaka', 'gb', '1250', '12634156', '2016-08-19 13:14:24'),
(2, 2, 'Database Administration', 'Jeeku', 'Mohammad', 'Khan', '1996-06-01', '', 'jeeku@gmail.com', '07824470253', 5582, 0, 'Street Address', 'Second Address 2', 'Cumilla', 'gb', '1207', '123456', '2016-08-21 09:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `ch_company_user_details`
--

CREATE TABLE `ch_company_user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `jobposition` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ch_education`
--

CREATE TABLE `ch_education` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `qualification_name` varchar(255) NOT NULL,
  `subject_studied` varchar(255) NOT NULL,
  `university_name` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `passing_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_education`
--

INSERT INTO `ch_education` (`id`, `user_id`, `qualification_name`, `subject_studied`, `university_name`, `grade`, `passing_year`) VALUES
(1, 1, 'SSC', 'PHYSICS, CHEMISTRY, MATH, BIOLOGY', 'Rangpur High School', 8, 2004),
(2, 1, 'HSC', 'PHYSICS, CHEMISTRY, MATH, BIOLOGY', 'Lions School & College', 8, 2007),
(3, 1, 'BA(Hons)', 'Tourism, Airlines Business, Tour Operation', 'IBAIS University', 8, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `ch_emergency_contact`
--

CREATE TABLE `ch_emergency_contact` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `relationship` varchar(255) NOT NULL,
  `home_phone` varchar(255) NOT NULL,
  `mobile_phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_emergency_contact`
--

INSERT INTO `ch_emergency_contact` (`id`, `user_id`, `first_name`, `last_name`, `relationship`, `home_phone`, `mobile_phone`) VALUES
(1, 2, 'fgh', 'fgh', 'fgh', 'fgh', 'fgh'),
(2, 2, 'ghjk', 'hjk', 'hgjk', 'hgjk', 'hgjk'),
(3, 2, 'ert', 'ert', 'ert', 'ert', 'ert'),
(4, 1, 'SM Farhad', 'Hossain', 'Myself', '01729963312', '01729963312'),
(5, 1, 'SM Farhad', 'Hossain', 'Myself', '01729963312', '01729963312');

-- --------------------------------------------------------

--
-- Table structure for table `ch_employee_history`
--

CREATE TABLE `ch_employee_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `roll` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `industry_sector` varchar(255) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `current` tinyint(4) DEFAULT '0',
  `operating_system_used` varchar(255) NOT NULL,
  `role_description_tasks` varchar(255) NOT NULL,
  `achievement_accomplishment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_employee_history`
--

INSERT INTO `ch_employee_history` (`id`, `user_id`, `roll`, `company_name`, `industry_sector`, `from_date`, `to_date`, `current`, `operating_system_used`, `role_description_tasks`, `achievement_accomplishment`) VALUES
(1, 1, 'Project Manager', 'Google', 'IT', '08/01/2014', '08/01/2015', 1, 'linux', 'Manage Management role Description', 'Achievement & Accomplishment'),
(2, 1, 'Project Manager', 'Best IT Company', 'IT', '03/01/2015', '03/01/2016', 0, 'linux', 'Role description & Tasks description Field', 'Best Employee Awarad'),
(3, 2, 'Autocad Designer', 'Hawor IT', 'IT', '01/08/2014', '01/08/2015', 0, 'linux', 'Role description & Tasks', 'Achievement & Accomplishment'),
(4, 2, 'Architech', 'Arban Design', 'Architech Firm', '01/08/2015', '01/01/2016', 1, 'linux', 'Role description & Tasks', 'Achievement & Accomplishment');

-- --------------------------------------------------------

--
-- Table structure for table `ch_employer_registration`
--

CREATE TABLE `ch_employer_registration` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(11) NOT NULL,
  `Industry_type` varchar(255) NOT NULL,
  `structure` varchar(255) NOT NULL,
  `prefered_company_size` varchar(255) NOT NULL,
  `main_telephone` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `building_house_no` varchar(255) NOT NULL,
  `street_name` varchar(255) NOT NULL,
  `address_line` varchar(50) NOT NULL,
  `postal_town` varchar(12) NOT NULL,
  `company_biography` varchar(2000) NOT NULL,
  `culture_and_values` varchar(2000) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ch_jobposting`
--

CREATE TABLE `ch_jobposting` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `companyName` text NOT NULL,
  `sectorIndustry` text NOT NULL,
  `jobTitle` text NOT NULL,
  `numberOfVacancies` varchar(255) NOT NULL,
  `workType` int(11) NOT NULL,
  `lengthfrom` int(11) NOT NULL,
  `lengthto` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `stateProvince` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `postCode` text NOT NULL,
  `startdate` date NOT NULL,
  `hoursPerWeek` text NOT NULL,
  `salarymin` text NOT NULL,
  `salarymax` text NOT NULL,
  `paymentType` int(11) NOT NULL,
  `guaranteeSought` int(11) NOT NULL,
  `essentialSkills` int(11) NOT NULL,
  `niceToHave` text NOT NULL,
  `itLiteracy` text NOT NULL,
  `jobDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_jobposting`
--

INSERT INTO `ch_jobposting` (`id`, `user_id`, `companyName`, `sectorIndustry`, `jobTitle`, `numberOfVacancies`, `workType`, `lengthfrom`, `lengthto`, `country`, `stateProvince`, `city`, `postCode`, `startdate`, `hoursPerWeek`, `salarymin`, `salarymax`, `paymentType`, `guaranteeSought`, `essentialSkills`, `niceToHave`, `itLiteracy`, `jobDescription`) VALUES
(1, 3, 'company', 'sectror', 'jobtitle', '2', 2, 1, 1, 2, 1, 1, 'dsfgsdf', '0000-00-00', '232', '23', '23', 1, 1, 2, 'fdsf', 'sdf', 'sdfsdf'),
(2, 3, 'company', 'sectror', 'jobtitle', '2', 2, 1, 2, 2, 1, 2, '1250', '0000-00-00', '1233', '12', '13', 1, 2, 2, 'Nice to have', 'yes', 'Jobdescription'),
(3, 1, '123', '123', '123', '123', 2, 1, 1, 1, 1, 1, '123', '0000-00-00', '123', '12', '123', 1, 1, 1, 'dfgh', 'dfgh', 'sdfgd');

-- --------------------------------------------------------

--
-- Table structure for table `ch_job_preference`
--

CREATE TABLE `ch_job_preference` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `roll_interested` varchar(255) NOT NULL,
  `prefered_wlocation` varchar(255) NOT NULL,
  `minimum_rate` int(255) NOT NULL,
  `prefered_rate` varchar(255) NOT NULL,
  `role_type` varchar(255) NOT NULL,
  `role_duration` varchar(255) NOT NULL,
  `work_start_time` varchar(255) NOT NULL,
  `work_status` varchar(255) NOT NULL,
  `benefits` varchar(255) NOT NULL,
  `pensions_plan` varchar(255) NOT NULL,
  `health_insurance` varchar(255) NOT NULL,
  `life_insurance` varchar(255) NOT NULL,
  `bonus_payment` varchar(255) NOT NULL,
  `contract_future` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_job_preference`
--

INSERT INTO `ch_job_preference` (`id`, `user_id`, `roll_interested`, `prefered_wlocation`, `minimum_rate`, `prefered_rate`, `role_type`, `role_duration`, `work_start_time`, `work_status`, `benefits`, `pensions_plan`, `health_insurance`, `life_insurance`, `bonus_payment`, `contract_future`) VALUES
(1, 1, 'Project Manager', 'Dhaka', 10, '15', 'Permanent', '2', '09/01/2016', '1', 'Benefits', 'Personal Plan', 'Yes', 'Life Insurance', 'Yes', '1'),
(2, 1, 'Software Developer', 'Dhaka', 10, '15', 'Permanent', '2', '09/01/2016', '1', 'Benefits', 'Personal Plan', 'Yes', 'Life Insurance', 'Yes', '1'),
(3, 1, 'Database Administration', 'Dhaka', 10, '15', 'Permanent', '2', '09/01/2016', '1', 'Benefits', 'Personal Plan', 'Yes', 'Life Insurance', 'Yes', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ch_personal_info`
--

CREATE TABLE `ch_personal_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `docfile` varchar(255) DEFAULT NULL,
  `company_structure` varchar(255) DEFAULT NULL,
  `company_number` varchar(255) DEFAULT NULL,
  `unique_tax_reference` varchar(255) DEFAULT NULL,
  `vat_registered` varchar(255) DEFAULT NULL,
  `do_you_require_an_account` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_personal_info`
--

INSERT INTO `ch_personal_info` (`id`, `user_id`, `country`, `passport`, `business_name`, `docfile`, `company_structure`, `company_number`, `unique_tax_reference`, `vat_registered`, `do_you_require_an_account`) VALUES
(5, 1, 'gb', 'BG 16546498', 'Website', NULL, 'Medium', '01729963312', '12345', '1', 1),
(6, 2, 'bd', 'AG 16546498', 'Civil', NULL, 'Medium ', '01729963312', '1234534', '1', 1),
(7, 2, 'bd', 'BG 16546498', 'Software', NULL, 'Small ', '01729963312', '1234534', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ch_prefered_wordplace_industry`
--

CREATE TABLE `ch_prefered_wordplace_industry` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `organization_size` int(11) NOT NULL,
  `prefered_job_industry` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_prefered_wordplace_industry`
--

INSERT INTO `ch_prefered_wordplace_industry` (`id`, `user_id`, `organization_size`, `prefered_job_industry`) VALUES
(1, 2, 2, 2),
(2, 2, 2, 3),
(3, 2, 2, 4),
(4, 2, 3, 2),
(5, 2, 3, 3),
(6, 2, 2, 2),
(7, 2, 2, 3),
(8, 2, 2, 4),
(9, 1, 1, 1),
(10, 1, 1, 2),
(11, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ch_progress_bar`
--

CREATE TABLE `ch_progress_bar` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress_id` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_progress_bar`
--

INSERT INTO `ch_progress_bar` (`id`, `user_id`, `progress_id`, `active`) VALUES
(10, 1, 1, 0),
(11, 1, 2, 0),
(14, 1, 3, 0),
(15, 1, 4, 0),
(16, 1, 5, 0),
(20, 1, 6, 0),
(23, 1, 7, 0),
(24, 1, 8, 0),
(25, 1, 9, 0),
(26, 1, 10, 0),
(27, 1, 11, 0),
(29, 1, 13, 0),
(30, 1, 12, 0),
(32, 2, 1, 0),
(33, 2, 2, 0),
(35, 2, 4, 0),
(36, 2, 3, 0),
(38, 2, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ch_references`
--

CREATE TABLE `ch_references` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `job_title` varchar(150) NOT NULL,
  `relationship` varchar(150) NOT NULL,
  `file_path` varchar(150) NOT NULL,
  `create_date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_references`
--

INSERT INTO `ch_references` (`id`, `user_id`, `name`, `email`, `job_title`, `relationship`, `file_path`, `create_date`) VALUES
(1, 2, 'dsfg', 'dsfg', 'sdfg', 'sdfg', '', 1471355752),
(2, 2, 'sdf', 'farhad1556@gmail.com', 'dsfsd', 'sdfsd', '', 1471612865),
(3, 1, 'SM Farhad Hossain', 'farhad1556@gmail.com', 'Software Developer', 'MySelf', '', 1471770471);

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_computer_skills`
--

CREATE TABLE `ch_settings_computer_skills` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_settings_computer_skills`
--

INSERT INTO `ch_settings_computer_skills` (`id`, `name`) VALUES
(1, 'Adobe Illustrator'),
(2, 'Adobe InDesign'),
(3, 'Adobe Photoshop'),
(4, 'Analytics'),
(5, 'Android'),
(6, 'APIs'),
(7, 'Art Design'),
(8, 'AutoCAD'),
(9, 'Backup Management'),
(10, 'C'),
(11, 'C++'),
(12, ' Certifications'),
(13, 'Client Server'),
(14, 'Client Support'),
(15, 'Configuration'),
(16, 'Content Managment'),
(17, 'Content Management Systems (CMS)'),
(18, 'Corel Draw'),
(19, 'Corel Word Perfect'),
(20, 'CSS'),
(21, 'Data Analytics'),
(22, 'Desktop Publishing'),
(23, 'Design'),
(24, 'Diagnostics'),
(25, 'Documentation'),
(26, 'End User Support'),
(27, 'Email'),
(28, 'Engineering'),
(29, 'Excel'),
(30, 'FileMaker Pro'),
(31, 'Fortran'),
(32, 'Graphic Design'),
(33, 'Hardware'),
(34, 'Help Desk'),
(35, 'HTML'),
(36, 'Implementation'),
(37, 'Installation'),
(38, 'Internet'),
(39, 'iOS'),
(40, 'iPhone'),
(41, 'Linux'),
(42, 'Java'),
(43, 'Javascript'),
(44, 'Mac'),
(45, 'Matlab'),
(46, 'Maya'),
(47, 'Microsoft Excel'),
(48, 'Microsoft Office'),
(49, 'Microsoft Outlook'),
(50, 'Microsoft Publisher'),
(51, 'Microsoft Word'),
(52, 'Microsoft Visual'),
(53, 'Mobile'),
(54, 'MySQL'),
(55, 'Networks'),
(56, 'pen Source Software'),
(57, 'Oracle'),
(58, 'Perl'),
(59, 'PHP'),
(60, 'Presentations'),
(61, 'Processing'),
(62, 'Programming'),
(63, 'PT Modeler'),
(64, 'Python'),
(65, 'QuickBooks'),
(66, 'Ruby'),
(67, 'Shade'),
(68, 'Software'),
(69, 'Spreadsheet'),
(70, 'SQL'),
(71, 'Support'),
(72, 'Systems Administration'),
(73, 'Tech Support'),
(74, 'Troubleshooting'),
(75, 'Unix'),
(76, 'UI / UX'),
(77, 'Web Page Design'),
(78, 'Windows'),
(79, 'Word Processing'),
(80, 'XML'),
(81, 'XHTML');

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_grading_system`
--

CREATE TABLE `ch_settings_grading_system` (
  `id` int(11) NOT NULL,
  `point` decimal(11,2) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `numerical_grade` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_settings_grading_system`
--

INSERT INTO `ch_settings_grading_system` (`id`, `point`, `grade`, `numerical_grade`) VALUES
(1, '2.00', 'D', '40% to 45%'),
(2, '2.25', 'C', '45% to 50%'),
(3, '2.50', 'C+', '50% to 55%'),
(4, '2.75', 'B-', '55% to 60%'),
(5, '3.00', 'B', '60 to 65%'),
(6, '3.25', 'B+', '65% to 70%'),
(7, '3.50', 'A-', '70% to 75%'),
(8, '3.75', 'A', '75% to 80%'),
(9, '4.00', 'A+', '80% or above');

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_industry_type`
--

CREATE TABLE `ch_settings_industry_type` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_settings_industry_type`
--

INSERT INTO `ch_settings_industry_type` (`id`, `name`) VALUES
(1, 'Business services\r\n'),
(2, 'Information technology\r\n'),
(3, 'Manufacturing \r\n'),
(4, 'Health care\r\n'),
(5, 'Finance\r\n'),
(6, 'Retail\r\n'),
(7, 'Accounting and legal\r\n'),
(8, 'Construction & repair\r\n'),
(9, 'Media \r\n'),
(10, 'Restaurants & Food\r\n'),
(11, 'Real Estate\r\n'),
(12, 'Textiles & Fashion');

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_languages`
--

CREATE TABLE `ch_settings_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ch_settings_languages`
--

INSERT INTO `ch_settings_languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_progress`
--

CREATE TABLE `ch_settings_progress` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `progress` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_settings_progress`
--

INSERT INTO `ch_settings_progress` (`id`, `name`, `progress`, `active`) VALUES
(1, 'basicInfo', 20, 0),
(2, 'phnVal', 10, 0),
(3, 'langskills', 5, 0),
(4, 'comSkills', 5, 0),
(5, 'empHis', 10, 0),
(6, 'education', 10, 0),
(7, 'jobpref', 5, 0),
(8, 'preWorInd', 5, 0),
(9, 'selfVideo', 5, 0),
(10, 'quiz', 5, 0),
(11, 'reference', 5, 0),
(12, 'personal', 10, 0),
(13, 'emgctc', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ch_settings_user_types`
--

CREATE TABLE `ch_settings_user_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_settings_user_types`
--

INSERT INTO `ch_settings_user_types` (`id`, `name`) VALUES
(1, 'Candidates'),
(2, 'Employer');

-- --------------------------------------------------------

--
-- Table structure for table `ch_skills_dist`
--

CREATE TABLE `ch_skills_dist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skills_head_id` int(11) NOT NULL,
  `skills_id` varchar(20) NOT NULL,
  `fluency_level` tinyint(4) NOT NULL,
  `current_status` tinyint(4) NOT NULL DEFAULT '0',
  `current_status_text` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_skills_dist`
--

INSERT INTO `ch_skills_dist` (`id`, `user_id`, `skills_head_id`, `skills_id`, `fluency_level`, `current_status`, `current_status_text`) VALUES
(20, 1, 1, '15', 5, 0, ''),
(21, 1, 1, '1', 5, 0, ''),
(22, 1, 2, '6', 3, 0, ''),
(23, 1, 2, '4', 4, 1, ''),
(24, 1, 2, '14', 4, 1, ''),
(27, 2, 2, '9', 2, 0, ''),
(28, 2, 2, '5', 3, 0, ''),
(29, 2, 2, '11', 4, 0, ''),
(30, 2, 1, '3', 3, 0, ''),
(31, 2, 1, '14', 4, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ch_skills_head`
--

CREATE TABLE `ch_skills_head` (
  `id` int(11) NOT NULL,
  `skills_head_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_skills_head`
--

INSERT INTO `ch_skills_head` (`id`, `skills_head_name`) VALUES
(1, 'Language'),
(2, 'Computer');

-- --------------------------------------------------------

--
-- Table structure for table `ch_temp_users`
--

CREATE TABLE `ch_temp_users` (
  `id` int(11) NOT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '2=candidates;1=employers',
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_temp_users`
--

INSERT INTO `ch_temp_users` (`id`, `type`, `email`, `password`, `key`) VALUES
(2, 1, 'farhad1556@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'b135752063b283cffa5f725a9970e7ce');

-- --------------------------------------------------------

--
-- Table structure for table `ch_upload_img_desc`
--

CREATE TABLE `ch_upload_img_desc` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_upload_img_desc`
--

INSERT INTO `ch_upload_img_desc` (`id`, `user_id`, `type_id`, `name`) VALUES
(2, 1, 2, '5ccf154511903714ba9672ed6439efc1.docx'),
(3, 2, 3, 'dadasdasd'),
(5, 2, 10, 'dadasdasd'),
(68, 2, 6, 'eb01b166cc5b6bf85e61ce1697218eaa.png'),
(69, 1, 3, '066da93824dca70ac26e584ef32ffe48.docx'),
(70, 1, 3, 'f1914acf8bdd2ab15b64302426ce29b1.docx'),
(71, 1, 3, '415c7b0247da16f6d8a530dc953b627d.docx'),
(72, 1, 3, 'c5b393e44634da64d37d1e4c6489a5a9.docx'),
(74, 2, 1, '72d580e1e9963729a844fb3fedeb6a51.mp4'),
(75, 1, 2, 'afdb99fad18b272ed22bafcf95de9d19.docx'),
(76, 1, 3, '47a93b87ffc0931af97b819ca2bf4c8c.docx'),
(77, 1, 6, '1a345aae4ff60899576c33b2390a7d55.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ch_users`
--

CREATE TABLE `ch_users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=employers, 2=candidates, 3=companyuser',
  `by_linkdin` tinyint(11) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ch_users`
--

INSERT INTO `ch_users` (`id`, `email`, `type`, `by_linkdin`, `password`, `registration_date`) VALUES
(1, 'farhad1556@gmail.com', 1, 1, '5f4dcc3b5aa765d61d8327deb882cf99', '2016-08-17 21:27:51'),
(2, 'jeeku@gmail.com', 2, 0, '5f4dcc3b5aa765d61d8327deb882cf99', '2016-08-21 08:02:11'),
(4, 'farhadndtl@gmail.com', 2, 0, '5f4dcc3b5aa765d61d8327deb882cf99', '2016-08-21 08:03:40'),
(5, 'rakb@gmail.com', 2, 0, '5f4dcc3b5aa765d61d8327deb882cf99', '2016-08-21 08:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `ch_voluntary_work_causes`
--

CREATE TABLE `ch_voluntary_work_causes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ch_voluntary_work_causes`
--

INSERT INTO `ch_voluntary_work_causes` (`id`, `user_id`, `name`) VALUES
(9, 2, 'Voluntary work or causes 1'),
(10, 2, 'Voluntary work or causes 2'),
(11, 2, 'Voluntary work or causes 3'),
(12, 2, 'Voluntary work or causes 4'),
(13, 1, 'Red Cross Relif'),
(14, 1, 'Blood Donation'),
(15, 1, 'Clothing For Poor People'),
(16, 1, 'Voluntary work or causes 4'),
(17, 1, 'Voluntary work or causes 1'),
(18, 1, 'Blood Donation'),
(19, 1, 'Clothing For Poor People'),
(20, 1, 'Voluntary work or causes 4'),
(21, 1, 'Voluntary work or causes 1'),
(22, 1, 'Blood Donation'),
(23, 1, 'Clothing For Poor People'),
(24, 1, 'Voluntary work or causes 4'),
(25, 1, 'Voluntary work or causes 1'),
(26, 1, 'Blood Donation'),
(27, 1, 'Clothing For Poor People'),
(28, 1, 'Voluntary work or causes 4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ch_basic_info`
--
ALTER TABLE `ch_basic_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `ch_education`
--
ALTER TABLE `ch_education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_emergency_contact`
--
ALTER TABLE `ch_emergency_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_employee_history`
--
ALTER TABLE `ch_employee_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_employer_registration`
--
ALTER TABLE `ch_employer_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_jobposting`
--
ALTER TABLE `ch_jobposting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_job_preference`
--
ALTER TABLE `ch_job_preference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_personal_info`
--
ALTER TABLE `ch_personal_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_prefered_wordplace_industry`
--
ALTER TABLE `ch_prefered_wordplace_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_progress_bar`
--
ALTER TABLE `ch_progress_bar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_references`
--
ALTER TABLE `ch_references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_computer_skills`
--
ALTER TABLE `ch_settings_computer_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_grading_system`
--
ALTER TABLE `ch_settings_grading_system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_industry_type`
--
ALTER TABLE `ch_settings_industry_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_languages`
--
ALTER TABLE `ch_settings_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_progress`
--
ALTER TABLE `ch_settings_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_settings_user_types`
--
ALTER TABLE `ch_settings_user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_skills_dist`
--
ALTER TABLE `ch_skills_dist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_skills_head`
--
ALTER TABLE `ch_skills_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_temp_users`
--
ALTER TABLE `ch_temp_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_upload_img_desc`
--
ALTER TABLE `ch_upload_img_desc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_users`
--
ALTER TABLE `ch_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_voluntary_work_causes`
--
ALTER TABLE `ch_voluntary_work_causes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ch_basic_info`
--
ALTER TABLE `ch_basic_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ch_education`
--
ALTER TABLE `ch_education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ch_emergency_contact`
--
ALTER TABLE `ch_emergency_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ch_employee_history`
--
ALTER TABLE `ch_employee_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ch_employer_registration`
--
ALTER TABLE `ch_employer_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ch_jobposting`
--
ALTER TABLE `ch_jobposting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ch_job_preference`
--
ALTER TABLE `ch_job_preference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ch_personal_info`
--
ALTER TABLE `ch_personal_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ch_prefered_wordplace_industry`
--
ALTER TABLE `ch_prefered_wordplace_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ch_progress_bar`
--
ALTER TABLE `ch_progress_bar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `ch_references`
--
ALTER TABLE `ch_references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ch_settings_computer_skills`
--
ALTER TABLE `ch_settings_computer_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `ch_settings_grading_system`
--
ALTER TABLE `ch_settings_grading_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ch_settings_industry_type`
--
ALTER TABLE `ch_settings_industry_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ch_settings_languages`
--
ALTER TABLE `ch_settings_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `ch_settings_progress`
--
ALTER TABLE `ch_settings_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ch_settings_user_types`
--
ALTER TABLE `ch_settings_user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ch_skills_dist`
--
ALTER TABLE `ch_skills_dist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ch_skills_head`
--
ALTER TABLE `ch_skills_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ch_temp_users`
--
ALTER TABLE `ch_temp_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ch_upload_img_desc`
--
ALTER TABLE `ch_upload_img_desc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `ch_users`
--
ALTER TABLE `ch_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ch_voluntary_work_causes`
--
ALTER TABLE `ch_voluntary_work_causes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
