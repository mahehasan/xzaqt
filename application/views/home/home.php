<?php $listicon = BASE_IMG . 'icon/list-icon.png' ?>
<div class="container-fluid">
    <div class="top-slide">
        <div>
            <div class="container">
                <div class="row">
                    <h1 > Get the right temp quickly and cost effectively </h1>
                </div> 
                <div class="row">
                    <p>
                        Find temporary candidates to suits your staffing needs at a more affordable price than traditional consultants
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-6"> 
                        <button id="get-started-asemp" class="btn btn-info pull-right " > Get Started as an Employers  </button>
                    </div>
                    <div class="col-md-6"> 
                        <!--<a href="<?php // echo BASE_URL ?>candidate/profilesetup" class="btn btn-theme-3"> Get Started as a Candidate   </a>-->
                        <button id="get-started-ascand" class="btn btn-theme-3" > Get Started as an Candidate  </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container text-center steps margin-bottom-50">
    <div class="row margin-bottom-50">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <h1> HOW IT WORKS ? </h1>
                    <p>
                        This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
                        Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
                    </p>
                </div>

            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <img height="90%" width="90%" src="<?php echo BASE_IMG; ?>icon/post-a-job.png" alt="">
                    <h3> POST A JOB </h3>
                </div> 
                <div class="col-md-3">
                    <img height="90%" width="90%" src="<?php echo BASE_IMG; ?>icon/choose-top-candidate.png" alt="">
                    <h3> CHOOSE YOUR TOP CANDIDATES </h3>                
                </div>
                <div class="col-md-3">
                    <img height="90%" width="90%" src="<?php echo BASE_IMG; ?>icon/invites-them.png" alt="">
                    <h3> INVITE THEM TO APPLY </h3>                    
                </div>
                <div class="col-md-3">
                    <img height="90%" width="90%" src="<?php echo BASE_IMG; ?>icon/make-an-offer.png" alt="">
                    <h3> MAKES AN OFFER </h3>                
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<div class="container-fluid why-use ">
    <div class="row text-center margin-top-50 ">
        <div class="col-md-12"> 
            <h1> WHY USE US </h1>
            <p class="margin-top-50">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
            </p>
        </div>
    </div>
    <div class="row margin-top-50">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="row text-center ">
                <div class="col-md-6 dt-style margin-bottom-50"> 
                    <h1 style="color:#69bac8;"> BENEFITS FOR EMPLOYERS </h1>

                    <img class="margin-top-50"  src="<?php echo BASE_IMG; ?>icon/benefits-for-candidates.png" alt="">
                    <dl class="dl-horizontal text-left ">
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Fill roles quickly with our growing database of qualified and experienced candidates</dd>                          
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Reuse your pool of trusted candidates you have previously used</dd>
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd>  No need to read through 100’s of CV, our system does the match </dd>                              
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Recruit people who share your company valuesSave costs (on average 25% less than traditional recruiters)</dd>
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Flexibility as you can choose for us to conduct the process for you (at an additional cost) </dd>
                    </dl>
                    <button class="btn btn-info"> Get Started as an Employers </button>
                </div>

                <div class="col-md-6  dt-style"> 
                    <h1 style="color:#7eb66f;"> BENEFITS FOR CANDIDATES </h1>
                    <img class="margin-top-50"  src="<?php echo BASE_IMG; ?>icon/befefits-of-emp.png" alt="">
                    <dl class="dl-horizontal text-left ">
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Save time and disappointment by applying for jobs only when the employer wants you</dd>                          
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Keep all the money you earn. Your money are safe under our protection system</dd>   
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> We offer employment benefits unlike most agencies. </dd>   
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> You will hear back when you are unsuccessful at securing a job. </dd>   
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> We suggest you the best potential jobs around you, accessible just by one click. </dd>   
                    </dl> 
                    <button class="btn btn-theme-3"> Get Started as an Employers </button>
                </div>
            </div>
        </div>

        <div class="col-md-1"></div>
    </div>

</div>
<div class="container-fluid book text-center">
    <div   class="col-md-12 margin-top-50 margin-bottom-50">
        <h1>BOOK A DEMO FOR FREE</h1>
    </div>
    <div class="row margin-bottom-50">
        <p>
            This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
        </p>
    </div>
    <div class="col-md-12 margin-bottom-50">
        <img  src="<?php echo BASE_IMG; ?>icon/playbutton.png">
    </div> 
    <div class="col-md-12 margin-bottom-50">
        <div class="row">
            <div class="col-md-5">

            </div>
            <div class="col-md-2">
                <button class="btn btn-theme-3 btn-lg btn-block "> Book a Demo </button>
            </div>
            <div class="col-md-5">

            </div>            
        </div>

    </div>      
</div>
<div class="container use-us">
    <div class="row text-center ">
        <div class="col-md-12">
            <div class="col-md-12 margin-bottom-50">
                <h1 class="margin-bottom-50 margin-top-50"> PEOPLE WHO USE US ? </h1>
                <p>
                    This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
                </p>
            </div>
        </div>
    </div>
    <div class="row text-center ">
        <div class="col-md-1"></div>
        <div class="col-md-5 emp-slider ">
            <h2> EMPLOYERS </h2>
            <?php require 'testimonial-caroulsel-1.php'; ?>

        </div>
        <div class="col-md-5 candidate-slider">
            <h2> CANDIDATES </h2>
            <?php require 'testimonial-caroulsel-2.php'; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<div class="container-fluid our-platform">
    <div class="container"> 
        <div class="row margin-top-50">
            <h1> OUR PLATFORM IS EVERY WHEERE </h1>
            <p class="margin-top-50 margin-bottom-50" >This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </p>
        </div>    
        <div class="row">
            <div class="col-md-8"> 
                <div class="row">
                    <dl class="dl-horizontal text-left ">
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Fill roles quickly with our growing database of qualified and experienced candidates </dd>                          
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd> Reuse your pool of trusted candidates you have previously used</dd>
                        <dt><img  src="<?php echo $listicon; ?>" alt=""></dt>
                        <dd>  No need to read through 100’s of CV, our system does the match </dd>                              
                    </dl>                     
                </div>
                <div class="row text-center margin-top-50 ">
                    <div class="col-md-2"></div>
                    <div class="col-md-5 text-right"> <img height="70%" width="70%" src="<?php echo BASE_IMG; ?>/icon/android-button.png" alt=""> </div> 
                    <div class="col-md-5 text-left"> <img height="70%" width="70%" src="<?php echo BASE_IMG; ?>/icon/ios-button.png" alt=""></div> 
                </div>
            </div>
            <div class="col-md-4"> 
                <img width="60%" height="60%" src="<?php echo BASE_IMG ?>icon/mobile-image.png" width="XZAQT">
            </div>
        </div>
    </div>
</div>
<div class="container hire-through">
    <div class="row">
        <h1 class="margin-bottom-50 margin-top-50"> COMPANIES WHO HIRE THROUGH US   </h1>
        <p>
            This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. <br>
            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.        
        </p>        
    </div>
    <div class="row">
        <div class="companies  col-md-12 text-center margin-top-50 margin-bottom-50">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/gm.png" alt="GENERAL MORTORS">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/bmw.png" alt="BMW">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/ibm.png" alt="IBM">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/hp.png" alt="HP">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/3m.png" alt="3M">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/companies/york.png" alt="YORK">
        </div>
    </div>
</div>
<footer class="container-fluid press ">
    <div class="row margin-top-50">
        <h1> we are featured in  </h1>
    </div>
    <div class="row margin-top-50">
        <div class="col-md-12 text-center">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/press/techCrunch.png" alt="Tech Crunch">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/press/giz.png" width="GIZMODO">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/press/informationeek.png" width="Information Week">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/press/mackworld.png" width="Mack World">
            <img width ="auto" height="auto" src="<?php echo BASE_IMG ?>icon/press/venturebeat.png" width="Venture Beat">
        </div>
    </div>
    <div class="row footer-nav">
        <div class="col-md-3"></div>
        <div class="col-md-9">
            <nav class="navbar-footer">
                <ul class="nav navbar-nav">
                    <li> <a href="#"> About </a> </li>
                    <li> <a href="#"> Careers </a> </li>
                    <li> <a href="#"> Blog </a> </li>
                    <li> <a href="#"> FAQ </a> </li>
                    <li> <a href="#"> Services </a> </li>
                    <li> <a href="#"> Terms </a> </li>
                    <li> <a href="#"> Affiliate </a> </li>
                    <li> <a href="#"> Partners Guide </a> </li>
                </ul>
            </nav>            
        </div>
    </div>
    <div class="row margin-bottom-50 margin-top-50">
        <div class="container ">
            <div class="col-md-4">
                <p style="margin-top: 10px;"> 2016 - XZAQT | ALL Rights Reserved.</p>
            </div>
            <div class="col-md-4">
                <p class="text-center"> <img src="<?php echo BASE_IMG; ?>footer-logo.png"> </p>
            </div>
            <div class="col-md-4"> 
                <p class="text-right"  style="margin-top: 10px;"> 
                    FOLLOW US: 
                    <a href="#"> <i class="fa fa-fw fa-twitter"></i> </a> 
                    <a href="#"> <i class="fa fa-fw fa-facebook"></i> </a> 
                    <a href="#"> <i class="fa fa-fw fa-google"></i> </a> 
                </p>
            </div>
        </div>
    </div>
</footer>