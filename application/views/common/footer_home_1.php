<!-- Login Box Start -->
<div id="login-pass-content" class="signup-loginbox overlay">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="col-md-8 col-md-offset-2">
                    <div class="loginbox">
                        <button style="border-radius: 50px; position: absolute" type="button" class="btn btn-flat btn-sm bg-red loginbox-close"  ><i class="fa fa-fw fa-close"></i></button>
                        <div class="panel panel-default">
                            <!-- Login panel contents -->
                            <div class="panel-heading text-center">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"> <a href="#employer-login" aria-controls="profile" role="tab" data-toggle="tab">I'm an Employer</a></li>
                                    <li role="presentation" class="active"><a href="#candidate-login" aria-controls="home" role="tab" data-toggle="tab">I'm a Candidate</a></li>
                                </ul>
                                <div class=" text-center row margin-top-25">
                                    <a href="<?php echo BASE_URL; ?>hauth/login/LinkedIn" class="btn btn-lg  btn-social btn-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        Sign in with LinkedIn
                                    </a>
                                </div>
                                <div class="or row">
                                    <div class="col-md-4"><hr noshade></div>
                                    <div class="col-md-4"><span>Or</span></div>
                                    <div class="col-md-4"><hr noshade></div>
                                </div>
                            </div>
                            <!-- Login Start-->
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="candidate-login"> 
                                        <?php
                                        $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                        echo form_open('hauth/login_email', $attributes);
                                        ?>
                                        <div class="form-group has-feedback">
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <!-- hidden start -->
                                        <div><input type="hidden" name="type" value="2" class="form-control"></div>     
                                        <!-- hidden end -->                                        
                                        <div class="form-group has-feedback">
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-xs-4">
                                                <button class="btn btn-primary btn-block btn-flat">Sign In</button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        </form>                                                   
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="employer-login">
                                        <?php
                                        $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                        echo form_open('hauth/login_email', $attributes);
                                        ?>
                                        <div class="form-group has-feedback">
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <!-- hidden start -->
                                        <div><input type="hidden" name="type" value="1" class="form-control"></div>     
                                        <!-- hidden end -->                                        
                                        <div class="form-group has-feedback">
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-xs-4">
                                                <button class="btn btn-primary btn-block btn-flat">Sign In</button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        </form>                                            


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="reset-pass-btn" href="#">I forgot my password</a><br>
                                        <a class="register-btn" href="#">Register a new membership</a>                                        
                                    </div> 
                                </div>                                         
                            </div>
                            <!--Login End -->

                            <!--Reset Start -->
                            <div class="panel-body">
                                <div class="tab-content">
                                    <?php
                                    if (isset($_SESSION['registration_success'])) {
                                        echo '<section class="content">
                                                <div class="alert alert-error alert-dismissable callout callout-info">
                                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                                <h4> Registered succesfully</h4>' . "<p><a href= " . base_url() . "hauth>Click here To login</a></p>";
                                        echo '</div>';
                                        unset($_SESSION['registration_success']);
                                    }
                                    ?>

                                    <!-- /.login-logo -->

                                    <p class="login-box-msg">Sign in to start your session</p>

                                    <?php
                                    if (validation_errors()) {
                                        echo '<div class="alert alert-error alert-dismissable">' .
                                        '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
                                        echo validation_errors();
                                        echo '</div>';
                                    }
                                    ?>
                                    <?php
                                    $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                    echo form_open('hauth/reset_password', $attributes);
                                    ?>
                                    <div class="form-group has-feedback">
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                    <div class="row">
                                        <!-- /.col -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Email</button>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="register-btn" href="#">Register a new membership</a>       
                                    </div> 
                                </div>                                         
                            </div>                            
                            <!--Reset End -->

                            <!-- Register Start -->
                            <div class="panel panel-default">
                                <!-- Login panel contents -->
                                <div class="panel-heading text-center">
                                    <h1> Reset Form</h1>
                                    <div class="or row">
                                        <div class="col-md-4"><hr noshade></div>
                                        <div class="col-md-4"><span>Or</span></div>
                                        <div class="col-md-4"><hr noshade></div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <?php
                                        if (isset($_SESSION['registration_success'])) {
                                            echo '<section class="content">
                                                <div class="alert alert-error alert-dismissable callout callout-info">
                                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                                <h4> Registered succesfully</h4>' . "<p><a href= " . base_url() . "hauth>Click here To login</a></p>";
                                            echo '</div>';
                                            unset($_SESSION['registration_success']);
                                        }
                                        ?>

                                        <!-- /.login-logo -->

                                        <p class="login-box-msg">Sign in to start your session</p>

                                        <?php
                                        if (validation_errors()) {
                                            echo '<div class="alert alert-error alert-dismissable">' .
                                            '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
                                            echo validation_errors();
                                            echo '</div>';
                                        }
                                        ?>
                                        <?php
                                        $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                        echo form_open('hauth/reset_password', $attributes);
                                        ?>
                                        <div class="form-group has-feedback">
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Email</button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="register-btn" href="#">Register a new membership</a>       
                                        </div> 
                                    </div>                                         
                                </div>
                            </div>                            

                            <!-- Register End -->
                        </div>
                    </div>                            
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Login Box End -->
<!-- reset box start -->
<div id="reset-pass-content" class="signup-loginbox overlay">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="col-md-8 col-md-offset-2">
                    <div class="loginbox">
                        <button style="border-radius: 50px; position: absolute" type="button" class="btn btn-flat btn-sm bg-red loginbox-close"  ><i class="fa fa-fw fa-close"></i></button>
                        <div class="panel panel-default">
                            <!-- Login panel contents -->
                            <div class="panel-heading text-center">
                                <h1> Reset Form</h1>
                                <div class="or row">
                                    <div class="col-md-4"><hr noshade></div>
                                    <div class="col-md-4"><span>Or</span></div>
                                    <div class="col-md-4"><hr noshade></div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <?php
                                    if (isset($_SESSION['registration_success'])) {
                                        echo '<section class="content">
                                                <div class="alert alert-error alert-dismissable callout callout-info">
                                                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                                <h4> Registered succesfully</h4>' . "<p><a href= " . base_url() . "hauth>Click here To login</a></p>";
                                        echo '</div>';
                                        unset($_SESSION['registration_success']);
                                    }
                                    ?>

                                    <!-- /.login-logo -->

                                    <p class="login-box-msg">Sign in to start your session</p>

                                    <?php
                                    if (validation_errors()) {
                                        echo '<div class="alert alert-error alert-dismissable">' .
                                        '<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>';
                                        echo validation_errors();
                                        echo '</div>';
                                    }
                                    ?>
                                    <?php
                                    $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                    echo form_open('hauth/reset_password', $attributes);
                                    ?>
                                    <div class="form-group has-feedback">
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                    <div class="row">
                                        <!-- /.col -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Email</button>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="register-btn" href="#">Register a new membership</a>       
                                    </div> 
                                </div>                                         
                            </div>
                        </div>
                    </div>                            
                </div>

            </div>
        </div>
    </div>
</div>
<!-- reset box end -->

<!-- register box start-->
<div  id="register-pass-content" class="signup-loginbox overlay">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 ">
                <div class="col-md-8 col-md-offset-2">
                    <div class="loginbox">
                        <button style="border-radius: 50px; position: absolute" type="button" class="btn btn-flat btn-sm bg-red loginbox-close"  ><i class="fa fa-fw fa-close"></i></button>
                        <div class="panel panel-default">

                            <!-- Login panel contents -->
                            <div class="panel-heading text-center">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"> <a href="#employer-registration" aria-controls="profile" role="tab" data-toggle="tab">I'm an Employer</a></li>
                                    <li role="presentation" class="active"><a href="#candidate-registration" aria-controls="home" role="tab" data-toggle="tab">I'm a Candidate</a></li>
                                </ul>
                                <div class=" text-center row margin-top-25">
                                    <a href="<?php echo BASE_URL; ?>hauth/register_validation" class="btn btn-lg  btn-social btn-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        Sign in with LinkedIn
                                    </a>
                                </div>
                                <div class="or row">
                                    <div class="col-md-4"><hr noshade></div>
                                    <div class="col-md-4"><span>Or</span></div>
                                    <div class="col-md-4"><hr noshade></div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="candidate-registration"> 
                                        <?php
                                        $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                        echo form_open('hauth/register_validation', $attributes);
                                        ?>
                                        <div class="form-group has-feedback">
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <!-- hidden start -->
                                        <div><input type="hidden" name="type" value="2" class="form-control"></div>     
                                        <!-- hidden end -->                                        
                                        <div class="form-group has-feedback">
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <input type="password" name="cpassword" class="form-control" placeholder="Retype password" required>
                                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                        </div>               
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat"> Register </button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        </form>                                                   
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade " id="employer-registration">
                                        <?php
                                        $attributes = array('role' => 'form', 'onsubmit' => 'return validateForm()');
                                        echo form_open('hauth/register_validation', $attributes);
                                        ?>
                                        <div class="form-group has-feedback">
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        </div>
                                        <!-- hidden start -->
                                        <div><input type="hidden" name="type" value="1" class="form-control"></div>     
                                        <!-- hidden end -->                                        
                                        <div class="form-group has-feedback">
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <input type="password" name="cpassword" class="form-control" placeholder="Retype password" required>
                                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                        </div>               
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat"> Register </button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        </form>   
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="reset-pass-btn" href="#">I forgot my password</a><br>
                                        <a class="sign-pass-btn" href="#">Login if you are user</a>                                         
                                    </div> 
                                </div>                                         
                            </div>
                        </div>
                    </div>                            
                </div>
            </div>
        </div>
    </div>
</div>
<div id="xzaqt-message-alert" class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong> Validation! Message : </strong> <span class="validation_text"></span>
</div>     

<!-- register box end-->

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<script>

    $(function () {
        //Initialize Select2 Elements
        if ($(".select2").length != 0) {
            $(".select2").select2();
        }
        if ($("#country_selector").length != 0) {
            $("#country_selector").countrySelect({
                defaultCountry: "jp",
                //onlyCountries: ['gb'],
                preferredCountries: ['ca', 'gb', 'us']
            });
        }

        // citizenship 
        if ($("#cityzen_selector").length != 0) {
            $("#cityzen_selector").countrySelect({
                defaultCountry: "gb",
                //onlyCountries: ['gb'],
                preferredCountries: ['ca', 'gb', 'us']
            });
        }
        //Date picker
        if ($("#from-date").length != 0) {
            $('#from-date').datepicker({
                autoclose: true
            });
        }

        if ($('#to-date').length != 0) {
            $('#to-date').datepicker({
                autoclose: true
            });
        }
        if ($('#workStartTime').length != 0) {
            $('#workStartTime').datepicker({
                autoclose: true
            });
        }

        // send user type 

        getUserType($('.nav-tabs a[href="#candidate-login"]'));
        getUserType($('.nav-tabs a[href="#employer-login"]'));
        getUserType($('.nav-tabs a[href="#candidate-registration"]'));
        getUserType($('.nav-tabs a[href="#employer-registration"]'));

        function getUserType(select) {
            select.on('click', function () {
                var userForm = $(this).attr('href');
                //var user_type = $(this).closest('.panel').children().find('input[name="type"]').val();
                var user_type = $(userForm).find('input[name="type"]').val();

                // ajax submition 
                data = 'user_type=' + user_type;
                var url = BASE_URL + 'hauth';
                // ajax submition 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    cache: false,
                    //dataType: 'json',
                    success: function () {
                    }
                });

            });
        }

        $('button#sign-in').on('click', function () {
            data = 'user_type=' + 2;
            var url = BASE_URL + 'hauth';
            // ajax submition 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                cache: false,
                //dataType: 'json',
                success: function () {
                }
            });
        });

    });
</script>
</body>
</html>