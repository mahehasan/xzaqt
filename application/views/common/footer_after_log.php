<!-- /.content-wrapper -->
<!--<footer class="main-footer footer navbar-fixed-bottom main-footer">-->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> Alpha 1.0
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="#"> Jobsite </a>.</strong> All rights
    reserved.
    <!-- /.container -->
</footer>
<div id="xzaqt-message-alert" class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong> Validation! Message : </strong> <span class="validation_text"></span>
</div>     

<!--- Javascript -->

<?php $this->load->view('common/javascript'); ?>

<script>
    $(function () {
        if ($('input').length != 0) {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }
        //Initialize Select2 Elements
        if ($(".select2").length != 0) {
            $(".select2").select2();
        }
        if ($(".country_selector").length != 0) {
            $(".country_selector").countrySelect({
                defaultCountry: "gb",
                //onlyCountries: ['gb'],
                preferredCountries: ['ca', 'gb', 'us', 'jp']
            });
        }
        if ($(".country-search").length != 0) {
            
            console.log('Test');
//            $(".country-search").countrySelect({
//                defaultCountry: "gb",
//                //onlyCountries: ['gb'],
//                preferredCountries: ['ca', 'gb', 'us', 'jp']
//            });
        }        
        
        // citizenship 
        if ($(".country_selector").length != 0) {
            $("#citizenship").countrySelect({
                defaultCountry: "gb",
                //onlyCountries: ['gb'],
                //preferredCountries: ['ca', 'gb', 'us']
            });
        }

//    
        //Date picker
        if ($("#from-date").length != 0) {
            $('#from-date').datepicker({
                autoclose: true
            });
        }
        if ($("#to-date").length != 0) {
            $('#to-date').datepicker({
                autoclose: true
            });
        }
        if ($("#workStartTime").length != 0) {
            $('#workStartTime').datepicker({
                autoclose: true
            });
        }
        if ($("#dob").length != 0) {
            $('#dob').datepicker({
                dateFormat: 'yy-mm-dd',
                autoclose: true
            });
        }
        /********* Next **********/
    });
    $(document).ready(function () {
        var tab = $('.nav-tabs a');
        var i = 1;
        $('button.next').click(function () {
            //console.log($(tab[i++]));
            $(tab[i++]).tab('show');
        });

    });
</script>

</div>  
</body>
</html>