$(function () {
    var fail = $('div#phone-validation-fail');
    var resend = $('div#phone-validation-resend');
    fail.hide();
    resend.hide();
    $('#form-phonevalidation').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            success: function (results) {
                if (results) {
                    var progress = results + "%";
                    $('#pregressbar').css("width", progress);
                    $('.nav-tabs a[href="#skills-language"]').tab('show');
                    $('span.show-barprogerss').empty();
                    $('span.show-barprogerss').append(progress);
                } else {
                    fail.find('span.validation_text').text('Phone code is not code is not correct');
                    fail.show(1000, function () {
                        $(this).fadeIn(3000);
                        $(this).hide(10000, function () {
                            //$(this).fadeOut(8000);
                        });
                    });
                }
            }
        });
    });

    $("#code-resend").on('click', function (e) {
        e.preventDefault();
        var url = BASE_URL + "sendsms";
        // on click ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            cache: false,
            success: function (results) {
                var msg = results;
                var res = msg.substring(0, 44); 
                resend.find('span.validation_text').text(res+'.');
                resend.show(1000, function () {
                    resend.fadeIn(3000);
//                    resend.hide(10000, function () {
//                        //$(this).fadeOut(8000);
//                    });
                });

            }
        });
    });


});


