<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //  $this->load->model('show_model');
    }

    public function index() {
        $userId = Common::userId();
        $table = 'upload_img_desc';
        $data = array(
            'user_id' => $userId,
            'type_id' => 6,
            'name' => 'abcabc'
        );
        $where = array('user_id' => $userId, 'type_id' => 6);
        $query = $this->db->select('type_id')->where($where)->get($table);   
        if($query->num_rows()){
            $this->db->where($where)->update($table, $data);
        } else {
            $this->db->insert($table, $data);
        }
    }

}
