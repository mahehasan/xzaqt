<script> var BASE_URL = "<?= BASE_URL; ?>";</script>
<div class="content-wrapper">
    <div class="container">    
        <!--<div class="container">-->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <?php // echo $title;    ?> </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom profile-setup-nav">
                        <ul class="nav nav-tabs" id="profile-setup-tabs">
                            <li class="active" >
                                <a href="#basic-info" onclick="return false;" >
                                    <i  class="font-icon-style fa fa-fw fa-user"></i> Basic Info
                                </a>
                            </li>
                            <li>
                                <a href="#phone-valication" onclick="return false;">
                                    <i class="font-icon-style fa fa-fw fa-tablet"></i>
                                    Phone Validation

                                </a>
                            </li>
                            <li>
                                <a href="#skills-language" onclick="return false;" >
                                    <i class="fa fa-fw fa-bolt font-icon-style"></i>
                                    Skills & Languages 
                                </a>
                            </li>
                            <li>
                                <a href="#employment-history" onclick="return false;" >
                                    <i class="font-icon-style fa fa-fw fa-suitcase"></i>
                                    Employment history
                                </a>

                            </li>
                            <li>
                                <a href="#education" onclick="return false;" >
                                    <i class="font-icon-style fa fa-fw fa-graduation-cap"></i>
                                    Education
                                </a>
                            </li>
                            <li>
                                <a href="#role-preference" onclick="return false;">
                                    <i class="font-icon-style fa fa-fw fa-star"></i> 
                                    Role preference 
                                </a>
                            </li>  
                            <li>
                                <a href="#personal-info" onclick="return false;">
                                    <i class="font-icon-style fa fa-fw fa-lock"></i> 
                                    Personal Info 
                                </a>
                            </li>              
                        </ul>
                        <div class="row">
                            <div class="progress-div"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-1">
                                Profile <br> Completeness
                            </div>
                            <div class="col-md-6">
                                <div class="progress">
                                    <div id="pregressbar"  class="progress-bar progress-bar-green" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar">
                                        <span class="sr-only"></span>
                                        <span class="show-barprogerss">0</span>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-1">
                                Ready to go !
                            </div>  
                            <div class="col-md-2"></div>
                        </div>
                        <div class="tab-content">
                            <!-- Basic Info Start -->
                            <div class="active tab-pane" id="basic-info">
                                <?php // require_once ''; ?>
                                <?php $this->load->view('profile/basic_info'); ?>
                            </div>
                            <!-- Basic Info End -->
                            <!-- /.tab-pane -->
                            <!-- Phone Validation Start -->
                            <div class="tab-pane" id="phone-valication">              
                                <?php $this->load->view('profile/phone_validation'); ?>
                            </div>
                            <!-- Phone Validation End -->
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="skills-language">
                                <?php $this->load->view('profile/skills'); ?>          
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="employment-history">
                                <?php $this->load->view('profile/employement_history'); ?>              
                            </div>              
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="education">
                                <?php $this->load->view('profile/education_achievement'); ?>                  
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="role-preference">
                                <?php $this->load->view('profile/job_preference'); ?>     
                                <?php $this->load->view('profile/prefered_workplace'); ?>     
                                <?php //$this->load->view('profile/prefered_job_industry'); ?>     
                                <?php $this->load->view('profile/tell_us_video'); ?>     
                                <?php $this->load->view('profile/quiz'); ?>    
                                <?php $this->load->view('profile/reference'); ?>    
                            </div>
                            <div class="tab-pane" id="personal-info">
                                <div class="content">
                                    <?php $this->load->view('profile/personal_info'); ?> 
                                </div>                    
                            </div>              
                            <!-- /.tab-pane -->              
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>  
    <!--    </div> /.container -->
</div>
<!-- /.content-wrapper -->
<script language="javascript">
// progress bar 
    var progress = <?php echo Common::progress(); ?>%";

    $(function () {
      var progressId = <?php if(Common::maxProgress()){ echo Common::maxProgress(); } else { echo 0; } ?>;
        switch (progressId) {
            case 1:
                $('.nav-tabs a[href="#phone-valication"]').tab('show');
                break;
            case 2:
                $('.nav-tabs a[href="#skills-language"]').tab('show');
                break;
            case 3:
                $('.nav-tabs a[href="#skills-language"]').tab('show');
                break;
            case 4:
                $('.nav-tabs a[href="#employment-history"]').tab('show');
                
                break;
            case 5:
                $('.nav-tabs a[href="#education"]').tab('show');
                //$('.nav-tabs a[href="#role-preference"]').tab('show');
                break;
            case 6:
                $('.nav-tabs a[href="#role-preference"]').tab('show');
                break;
            case 7:
                $('.nav-tabs a[href="#role-preference"]').tab('show');               
                $('#content-jobs-preferences').show();
                $('#content-your-preferred-workplace-industries').hide();
                $('#content-your-self-video').hide();
                $('#content-your-quiz').hide();
                $('#content-your-references').hide();
                break;     
            case 8:
                $('.nav-tabs a[href="#role-preference"]').tab('show');               
                $('#content-jobs-preferences').hide();
                $('#content-your-preferred-workplace-industries').show();
                $('#content-your-self-video').hide();
                $('#content-your-quiz').hide();
                $('#content-your-references').hide();
                break;       
            case 9:
                $('.nav-tabs a[href="#role-preference"]').tab('show');               
                $('#content-jobs-preferences').hide();
                $('#content-your-preferred-workplace-industries').hide();
                $('#content-your-self-video').show();
                $('#content-your-quiz').hide();
                $('#content-your-references').hide();
                break;                    
            case 10:
                $('.nav-tabs a[href="#role-preference"]').tab('show');               
                $('#content-jobs-preferences').hide();
                $('#content-your-preferred-workplace-industries').hide();
                $('#content-your-self-video').hide();
                $('#content-your-quiz').show();
                $('#content-your-references').hide();
                break;   
            case 11:
                $('.nav-tabs a[href="#role-preference"]').tab('show');               
                $('#content-jobs-preferences').hide();
                $('#content-your-preferred-workplace-industries').hide();
                $('#content-your-self-video').hide();
                $('#content-your-quiz').hide();
                $('#content-your-references').show();
                break;                   
            case 12:
                $('.nav-tabs a[href="#personal-info"]').tab('show');               
                break;  
            case 13:
                //var url = BASE_URL + "candidate";
                //window.location.href = url;
                $('.nav-tabs a[href="#personal-info"]').tab('show');      
                break;                                
            default:
             $('.nav-tabs a[href="#basic-info"]').tab('show');
        }
        $('div#pregressbar').css("width", progress);
        $('span.show-barprogerss').empty();
        $('span.show-barprogerss').append(progress);

    });


</script>