<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="image">
                <label class="edit-profile-button">
                    <input style="display: none;" type="file" name="file" id="edit-profile-pic" data-url="<?php echo BASE_URL ?>upload/profilepic" class="btn btn-flat bg-green-active btn-sm "> 
                    <i class="fa fa-fw fa-pencil"></i></label>
                <!-- User Image --> 
                <img width="137" height="137" src="<?php
                echo BASE_URL;
                if ($profile_pic) {
                    echo $profile_pic;
                } else {
                    echo 'assets/images/avatar_img.png';
                }
                ?> " class="profile-user-img img-responsive img-circle" alt="User Image">
            </div> <!-- assets/images/avatar_img.png -->
            <div class="info">
                <h4><?php echo Common::basic('first_name', 'Set Basic Info') . ' ' . Common::basic('last_name'); ?> </h4>


         <!--  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
                <div class="row">
                    <div class="col-md-6">
                        <p>   <strong><i class="fa fa-map-marker margin-r-5"></i></strong>
                            <span class="text-muted">London, UK</span></p>
                    </div>
                    <div class="col-md-6">
                        <strong><i class="fa fa-clock-o margin-r-5"></i></strong>
                        <span class="text-muted">London, UK</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <p class="text-muted">Profile Completed</p>
                    </div>
                    <div class="col-md-4">
                        <p> <?= Common::progress() ?> </p>
                    </div>
                    <div class="col-md-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-green" style="width: <?= Common::progress() ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar">
                                <span class="sr-only"> <?= Common::progress() ?>% Complete (success)</span><span> <?= Common::progress() ?>%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p class="btn-custom"><a id="btn-custom" class="btn btn-block btn-success " href="<?php echo BASE_URL; ?>candidate/profile"> View Profile <!-- Finalize your profile --> </a></p>
                    </div>
                </div>          
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview active">
                <a href="#"> <i class="fa fa-user"></i> <span>Profile</span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>candidate/profile">Public Profile </a></li>
                    <li><a href="<?php echo base_url() ?>candidate/profilesetup"> Profile Setup</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#" >
                    <i class="fa fa-user"></i> <span>Link 1</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li>            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
