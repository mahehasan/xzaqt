<?php

class Jobposting_model extends CI_Model {

    private $tableName = 'jobposting';
    private $userId;
    private $msg;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->userId = Common::userId();
        
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $companyName = $this->input->post('companyName');
        $sectorIndustry = $this->input->post('sectorIndustry');
        $jobTitle = $this->input->post('jobTitle');
        $numberOfVacancies = $this->input->post('numberOfVacancies');
        $workType = $this->input->post('workType');
        $lengthfrom = $this->input->post('lengthfrom');
        $lengthto = $this->input->post('lengthto');
        $country = $this->input->post('country');
        $stateProvince = $this->input->post('stateProvince');
        $city = $this->input->post('city');
        $postCode = $this->input->post('postCode');
        $startdate = $this->input->post('startdate');
		$hoursPerWeek = $this->input->post('hoursPerWeek');
		$salarymin = $this->input->post('salarymin');
		$salarymax = $this->input->post('salarymax');
		$paymentType = $this->input->post('paymentType');
		$guaranteeSought = $this->input->post('guaranteeSought');
		$essentialSkills = $this->input->post('essentialSkills');
		$niceToHave = $this->input->post('niceToHave');
		$itLiteracy = $this->input->post('itLiteracy');
		$jobDescription = $this->input->post('jobDescription');
        $data = array(
            'user_id' => $this->userId,
            'companyName' => $companyName,
            'sectorIndustry' => $sectorIndustry,
            'jobTitle' => $jobTitle,
            'numberOfVacancies' => $numberOfVacancies,
            'workType' => $workType,
            'lengthfrom' => $lengthfrom,
            'lengthto' => $lengthto,
            'country' => $country,
            'stateProvince' => $stateProvince,
            'city' => $city,
            'postCode' => $postCode,
			'startdate' => $startdate,
			'hoursPerWeek' => $hoursPerWeek,
			'salarymin' => $salarymin,
			'salarymax' => $salarymax,
			'paymentType' => $paymentType,
			'guaranteeSought' => $guaranteeSought,
			'essentialSkills' => $essentialSkills,
			'niceToHave' => $niceToHave,
			'itLiteracy' => $itLiteracy,
			'jobDescription' => $jobDescription,
        );
		$this->db->insert($this->tableName, $data);	
		if($this->db->affected_rows()){
		return true;
	    } else {
		return false;	
	    }
        
    }


}
