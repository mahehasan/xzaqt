<?php

class Education_model extends CI_Model {

    private $tableName = 'education';
    
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $uid = Common::user('id');
        $qualification = $this->input->post('qualification');
        $subjectStudied = $this->input->post('subjectStudied');
        $universityName = $this->input->post('universityName');
        $grade = $this->input->post('grade');
        $passingYear = $this->input->post('passin_year');
        $voluntryWork = $this->input->post('voluntryWork');
        
        $data = array(
            'user_id' => $uid,
            'qualification_name' => $qualification,
            'subject_studied' => $subjectStudied,
            'university_name' => $universityName,
            'grade' => $grade,
            'passing_year' => $passingYear
        );

        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
