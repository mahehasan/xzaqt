<div class="content">
    <div class="col-md-12">
        <div class="status-message-emp alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Well done!</strong> Well done! You successfully read this important alert message.
        </div>
    </div>
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-3">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class="fa fa-fw fa-suitcase"></i></span>
                <span class="thm-text ">  Employment </span>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>

    <form id="form-empHistory" role="form" action="<?php echo base_url(); ?>candidate/profilesetup/addemphistory" method="post">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="roll">Roll</label> 
                        <?php echo form_error('roll'); ?>
                        <input name="roll"  type="text" value="<?php echo $this->input->post('roll'); ?>" class="form-control" id="roll" placeholder="Roll" required>
                    </div>
                    <div class="form-group">
                        <label for="companyName"> Company Name </label> 
                        <?php echo form_error('companyName'); ?>
                        <input name="companyName"  type="text" value="<?php echo $this->input->post('companyName'); ?>" class="form-control" id="roll" placeholder="Company Name" required>
                    </div>
                    <div class="form-group">
                        <label for="industrySector"> Industry Sector </label> 
                        <?php echo form_error('industrySector'); ?>
                        <input name="industrySector"  type="text" value="<?php echo $this->input->post('industrySector'); ?>" class="form-control" id="roll" placeholder="Industry Sector" required>
                    </div>  
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>From :</label>
                                <input name="from" value="<?php echo $this->input->post('from'); ?>" type="text" class="form-control" id="from-date" required>

                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>To:</span> </label>
                                <input name="to" value="<?php echo $this->input->post('to'); ?>" type="text" class="form-control" id="to-date" required>
                                <span style="color: red;" class="to-date-error"></span>
                            </div>                              
                        </div>
                        <div class="col-md-4">
                            <div style="font-size: 12px;" class="row text-center">  <label>Current <br>employment </label> </div>
                            <div class="row">      
                                <div class=" text-center form-group">  
                                    <label>
                                        <input value="0" type="checkbox" class="minimal-red" name="current">
                                    </label>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="systemUsed"> Computer System Used </label> 
                        <?php echo form_error('industrySector'); ?>
                        <input name="system" value="<?php echo $this->input->post('system'); ?>"  type="text" class="form-control" id="systemUsed" placeholder="System Used" required>
                    </div>
                </div>
                <div class="col-md-5">
                    <div style="margin-bottom: 27px;" class="form-group">
                        <label>Role description & Tasks</label>
                        <textarea name="rollDescription" value="<?php echo $this->input->post('rollDescription'); ?>" class="form-control"  rows="9"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="achievement">Achievement & Accomplishment </label>
                        <input name="achievement"  type="text" value="<?php echo $this->input->post('achievement'); ?>" class="form-control" id="achievement" required>
                    </div>
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div class="row"> 
            <div class="col-md-12 text-center">
                <input type="submit" class="btn btn-theme" value="addAnotherEmp" name="addEmp">
                <a href="employement_history.php"></a>
                <button id="save-empHistory"  type="submit" class=" btn bg-olive" value="save & Next" name="addNext">Save & Next </button>
            </div>
        </div>
    </form>
</div> <!-- History End -->  
<script src="<?php echo BASE_URL; ?>assets/js/profile/emphistory.js"></script>
<script>
    $(function () {
//        var d, date, month, year, today;
//        d = new Date();
//        date = d.getDate();
//        month = d.getMonth() + 1;
//        year = d.getFullYear();
//        if (date.toString().length == 1) {
//            date = '0' + date;
//        } else {
//            date = date;
//        }
//        if (month.toString().length == 1) {
//            month = '0' + month;
//        } else {
//            month = month;
//        }
//        today = date + '/' + month + '/' + year;
        var to = $("input[name='to']");
        $("input[name='current']").on('ifChecked ifUnchecked', function(event){
            if(event.type ==="ifChecked"){
                $(this).trigger('click');  
                $(this).val('1');  
                to.removeAttr("required");
                to.val('');
                $('.to-date-error').text('');
            } 
            if(event.type ==="ifUnchecked"){
                $(this).trigger('click');  
                $(this).val('0');  
                to.attr("required");
                $('.to-date-error').text('');
                $('.to-date-error').text('Its Required');
            }
           
        });       
    });
</script>