<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Progress extends CI_Controller {

    public function __construct() {
        parent::__construct();
        Common::canview();
        Common::goToEmp();
        $this->load->model('Progress_model');
    }
    
    public function index() {
        echo $this->progress();
    }

    public static function progress(){
        $progress = Common::progress();
        return $progress;      
    }
    public static function progressststus() {
        $uid = Common::user('id');
        $this->db->where('user_id', $uid);
        $query = $this->db->get('progress_bar');
        $progress = $query->result();    
        echo '<pre>';            
        print_r($progress);
        echo '</pre>';
    }    
    

}
