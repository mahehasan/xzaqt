<?php

class Emphistory_model extends CI_Model {
    
    private $tableName = 'employee_history';


    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function get() {
        $data =  findAll(DB_TABLE);
        return $data;
    }
    public function add() {
            $uid = Common::user('id');
            $roll = $this->input->post('roll');
            $companyName = $this->input->post('companyName');
            $industrySector = $this->input->post('industrySector');
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            $current = $this->input->post('current');
            $system = $this->input->post('system');
            $rollDescription = $this->input->post('rollDescription');
            $achievement = $this->input->post('achievement');
            
            $data = array(
                            'user_id' => $uid,
                            'roll' => $roll,
                            'company_name' =>$companyName,
                            'industry_sector' =>$industrySector,
                            'from_date' => $from,
                            'to_date' => $to,
                            'current' => $current,
                            'operating_system_used' => $system,
                            'role_description_tasks' => $rollDescription,
                            'achievement_accomplishment' => $achievement        
                    );
      
           $status = Common::save($this->tableName, $data);        
    }
    
    
    public function save() {
        
    }
    
    
}
