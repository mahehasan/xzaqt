<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Employer
 *
 * @author farhad
 */
class CandidateSearch extends CI_Controller {

    //put your code here
    private $to;

    public function __construct() {
        parent::__construct();
        Common::canview();
        Common::goToCan();
      
        //$this->load->model('profile_model');
    }

    public function index() {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        die();
        $this->create();
    }

    public function create() {
        $data['title'] = 'Search a candidate';
        $data['show2ndmenu'] = true;
        
        $header = 'common/header_home';
        $body = 'employer/profile/search_candidate';
        $footer = 'common/footer_dashboard';
        Template::set_view($header, $data);
        Template::set_view($body);
        Template::set_view($footer);
    }

    public function search_filters() {
        echo "here";
    }
}
