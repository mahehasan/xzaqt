<?php

/**
 * Description of Registration_model
 * 
 * @author farhad
 */
class Registration_model extends CI_Model {

    private $tableName = 'employer_registration';

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();

    }

    //put your code here
    public function add(){
        $userid =  Common::userId();
        $companyName = $this->input->post('companyName');
        $industry = $this->input->post('industry');
        $structure = $this->input->post('structure');
        $preferedcompanySize = $this->input->post('preferedcompanySize');
        $mainTelephone = $this->input->post('mainTelephone');
        $website = $this->input->post('website');
        $buildingHouseNo = $this->input->post('buildingHouseNo');
        $streetName = $this->input->post('streetName');
        $addressLine = $this->input->post('addressLine');
        $postalTown = $this->input->post('postalTown');
        $companyBiography = $this->input->post('companyBiography');
        $cultureAndValues = $this->input->post('cultureAndValues');
        $data = array(
             	'user_id' => $userid,
             	'company_name' => $companyName,
             	'Industry_type' => $industry,
             	'structure' => $structure,
             	'prefered_company_size' => $preferedcompanySize,
             	'main_telephone' => $mainTelephone,
             	'website' => $website,
             	'building_house_no' => $buildingHouseNo,
             	'street_name' => $streetName,
             	'address_line' => $addressLine,
             	'postal_town' => $postalTown,
             	'company_biography' => $companyBiography,
             	'culture_and_values' => $cultureAndValues,
             	'status' => 1
        );
        
        if($this->save($data)){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    public function status() {
        $user_id = Common::userId();
        $query = $this->db->where('user_id', $user_id)
                ->get($this->tableName);
        $result = $query->num_rows();
        return $result;
    }
        
    private function save($data) {
        $user_id = Common::userId();
        $table = $this->tableName;
        if($this->status()== 0){
            return $this->db->insert($table, $data);    
        } else {
            $this->db->where('user_id', $user_id);
            $this->db->update($table, $data);
            if($this->db->affected_rows()){
                return true;
            } else {
                return false;
            }
        }
        
    }

}
