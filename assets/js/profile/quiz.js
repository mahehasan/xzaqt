$(function () {
    
    $('.preferedIndustry label').on('click', function(){
       if($(this).hasClass( "preferedIndustry-checked")){
           $(this).removeClass("preferedIndustry-checked");
       } else{
           $(this).addClass('preferedIndustry-checked');
       }    
    });
    
    
    $('#your-quiz-frm').on('submit',function(e){
        e.preventDefault();
        var data = $(this).serialize();
       
        var url = $(this).attr('action');
                                          
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
            	var progress = results+"%";
            	$('#pregressbar').css("width", progress);
            	$('#content-your-quiz').hide();
   		$('#content-your-references').show();
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });
        
    });
   
   
});

