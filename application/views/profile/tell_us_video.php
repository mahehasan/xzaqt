<style>
    #dropzone {
        /*        background: palegreen;
                width: 150px;
                height: 50px;
                line-height: 50px;
                text-align: center;
                font-weight: bold;*/
    }
    #dropzone.in {
        /*        width: 600px;
                height: 200px;
                line-height: 200px;
                font-size: larger;*/
    }
    #dropzone.hover {
        background: rgba(126,182,111, .5);
        color: #fff;
    }
    #dropzone.fade {
        -webkit-transition: all 0.3s ease-out;
        -moz-transition: all 0.3s ease-out;
        -ms-transition: all 0.3s ease-out;
        -o-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
        opacity: 1;
    }  
    .file-upload {
        position: relative;
        overflow: hidden;
        margin: 10px; }

    .file-upload input.file-input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0); 
    }
</style>
<div class="content" id="content-your-self-video" style="display: none;">
    <div class="row margin-10">
        <div class="col-md-1"> </div>
        <div class="col-md-8">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class=" fa fa-fw fa-graduation-cap"></i></span>
                <span class="thm-text "> Tell us your self video  </span>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
    <form role="form" action="<?php echo BASE_URL; ?>candidate/profilesetup/self_video" method="post" enctype="m ultipart/form-data" accept-charset="utf-8" id="self-video-frm">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-10">
                    <div id="dropzone" class="uploadvideo text-center">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="video">
                                    <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/video.png">
                                    <p class="draganddrop"> Click on the button or Drag & Drop Video files here  </p>
                                    <div class="form-group margin-top-50 uploading-msg" >

                                    </div>
                                    <div id="file-upload-group" class="form-group ">
                                        <label class="file-upload">            
                                            <input name="file" type="file" id="videoupload" class="file-input" data-url="<?php echo BASE_URL ?>upload/tellusvideo" >Upload A Video
                                        </label>
                                        <!-- to demo the original button style side-by-side -->
                                    </div>
                                    <p  class="help-block margin-top-50 margin-bottom-50">We accept <strong>avi, wmv, flv, mp4, 3gp </strong> videos files | Max size: 100MB</p>                              
                                </div>
                            </div>
                        </div>
                        <div class="row"></div>
                        <div class="row"></div>
                    </div>
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>
        <div style="margin-top:30px;" class="row margin-bottom-50 "> 
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-theme" name="save" id="save-self-video"> Save & Next </button>
            </div>
        </div>
    </form>
</div> <!-- Employment History End -->

<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/vendor/jquery.ui.widget.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.iframe-transport.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.fileupload.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/js/profile/tell_us_your_self_video.js"></script>

<script>
    if ($('#videoupload').length != 0) {

        $('#videoupload').fileupload({
            dataType: 'html',
            add: function (e, data) {
                data.context = $('<p/>').append('<img src="<?php echo BASE_IMG ?>icon/loader.gif" alt="uploading....">').appendTo($('div.uploading-msg'));
                data.submit();
                //$('#file-upload-group').hide();
            },
            done: function (e, data) {
                $('div.uploading-msg p').remove();
                //$('#file-upload-group').show();
                var messageDiv = $("div#xzaqt-message-alert");
                var result = data._response.result;
                if (result == 1) {
                    result = 'Video Uploaded Successfully';
                    if(messageDiv.hasClass("alert-warning")){
                        messageDiv.removeClass("alert-warning")
                        .addClass('alert-success');
                    }   
                } else {
                    if (messageDiv.hasClass("alert-success")) {
                        messageDiv.removeClass("alert-success")
                        .addClass('alert-warning');
                    }
                }

                messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
                messageDiv.delay(2000).fadeOut("slow");

            }
        });

        $('#videoupload').fileupload({
            dropZone: $('#dropzone')
        });

        $(document).bind('dragover', function (e) {
            var dropZone = $('#dropzone'),
                    timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            var found = false,
                    node = e.target;
            do {
                if (node === dropZone[0]) {
                    found = true;
                    break;
                }
                node = node.parentNode;
            } while (node != null);
            if (found) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });
    }
</script>