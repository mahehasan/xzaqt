<?php

class Progress_settings_model extends CI_Model {

    private $tableName = 'settings_progress';

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function findProgressIdByName($progressName){
        $where = array('name'=> $progressName);
        $query = $this->db->where($where)->get($this->tableName);
        $resut = $query->result();
        $id =   $resut[0]->id;
          return $id;      
    }

}
