<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <?php //echo $title;  ?> </h1>
        </section>

        <!-- Main content -->
        <section class="content dashboard">
            <div class="row">
                <div class="col-md-6"> 
                    <h3>Month Calender</h3>
                    <!-- Calender Start -->
                    <?php $this->load->view('dashboard/calender'); ?>
                    <!-- Calender End -->
                </div>
                <div class="col-md-5"> 
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="box-title"> Month Overview </h3>
                            <?php $this->load->view('dashboard/monthoverview'); ?>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="box-title"> Profile View </h3>
                            <?php $this->load->view('dashboard/profileview'); ?>                            
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title"> Latest Activities </h3>
                    <?php $this->load->view('dashboard/latestactivities'); ?>                  
                </div>
                <div class="col-md-5">
                    <h3 class="box-title"> Potential Jobs </h3>
                    <?php $this->load->view('dashboard/potential-job'); ?>     
                
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
