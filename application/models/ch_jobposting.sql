-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- হোষ্ট: 127.0.0.1
-- তৈরী করতে ব্যবহৃত সময়: আগ 21, 2016 at 01:31 PM
-- সার্ভার সংস্করন: 10.1.9-MariaDB
-- পিএইছপির সংস্করন: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- ডাটাবেইজ: `jobsite`
--

-- --------------------------------------------------------

--
-- টেবলের জন্য টেবলের গঠন `ch_jobposting`
--

CREATE TABLE `ch_jobposting` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `companyName` text NOT NULL,
  `sectorIndustry` text NOT NULL,
  `jobTitle` text NOT NULL,
  `numberOfVacancies` varchar(255) NOT NULL,
  `workType` int(11) NOT NULL,
  `lengthfrom` int(11) NOT NULL,
  `lengthto` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `stateProvince` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `postCode` text NOT NULL,
  `startdate` date NOT NULL,
  `hoursPerWeek` text NOT NULL,
  `salarymin` text NOT NULL,
  `salarymax` text NOT NULL,
  `paymentType` int(11) NOT NULL,
  `guaranteeSought` int(11) NOT NULL,
  `essentialSkills` int(11) NOT NULL,
  `niceToHave` text NOT NULL,
  `itLiteracy` text NOT NULL,
  `jobDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- টেবলের জন্য তথ্য স্তুপ করছি `ch_jobposting`
--

INSERT INTO `ch_jobposting` (`id`, `user_id`, `companyName`, `sectorIndustry`, `jobTitle`, `numberOfVacancies`, `workType`, `lengthfrom`, `lengthto`, `country`, `stateProvince`, `city`, `postCode`, `startdate`, `hoursPerWeek`, `salarymin`, `salarymax`, `paymentType`, `guaranteeSought`, `essentialSkills`, `niceToHave`, `itLiteracy`, `jobDescription`) VALUES
(1, 3, 'company', 'sectror', 'jobtitle', '2', 2, 1, 1, 2, 1, 1, 'dsfgsdf', '0000-00-00', '232', '23', '23', 1, 1, 2, 'fdsf', 'sdf', 'sdfsdf'),
(2, 3, 'company', 'sectror', 'jobtitle', '2', 2, 1, 2, 2, 1, 2, '1250', '0000-00-00', '1233', '12', '13', 1, 2, 2, 'Nice to have', 'yes', 'Jobdescription');

--
-- স্তুপকৃত টেবলের ইনডেক্স
--

--
-- টেবিলের ইনডেক্সসমুহ `ch_jobposting`
--
ALTER TABLE `ch_jobposting`
  ADD PRIMARY KEY (`id`);

--
-- স্তুপকৃত টেবলের জন্য স্বয়ক্রীয় বর্দ্ধিত মান (AUTO_INCREMENT)
--

--
-- টেবলের জন্য স্বয়ক্রীয় বর্দ্ধিত মান (AUTO_INCREMENT) `ch_jobposting`
--
ALTER TABLE `ch_jobposting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
