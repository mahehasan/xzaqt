<!-- Full Width Column -->
<div class="content-wrapper">
    <section class="content-header">
        <h1> <?php echo $title; ?> </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php if (isset($_SESSION['update_true'])) { ?>

                <div  class="pad margin no-print">
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                        <button id="update_warning" type="button" class="close" data-dismiss="update_warning" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p>Data Updated Successfully.</p>
                    </div>
                </div>
                <?php
                unset($_SESSION['update_true']);
            }
            ?>  
            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title"> Language Skills </h3>
<!--                        <a class="btn btn-warning pull-right" href="<?php // echo base_url();   ?>profile/contactInfo"><i class="fa fa-edit"></i> Edit</a>-->
                            <button class="btn btn-primary pull-right language-add" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Add
                            </button> 
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body datalist">
                        <div class="col-md-4"> 
                            <p> Language </p>
                            <p class="text-muted" style="font-size:12px; font-weight: 400; "> <span style="font-weight: bold; " > THREE LABEL</span> :  Low, Medium, Excellent <p>                        

                        </div>
                        <div class="col-md-8"> 
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Reading</th>
                                        <th>Writing </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($languages as $language) { ?>
                                        <tr>
                                            <td value="<?= $language->language_id; ?>"><?= $language->language_name ?> </td>
                                            <td value="<?= $language->reading_label;?>"><?= $language_label[$language->reading_label]; ?></td>
                                            <td value="<?= $language->writing_label; ?>"><?= $language_label[$language->writing_label]; ?></td>
                                            <td>
                                            <button value="<?= $language->skills_dist_id ?> " class="btn btn-warning skill-dist-id pull-right" type="button" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-edit"></i> 
                                            </button> 
                                            </td>
                                            <td>
                                                <form action="<?php echo base_url();?>profile/delete" method="post">
                                                    <input value="<?= $language->skills_dist_id ?>" type="hidden" name="id">
                                                    <button class="btn btn-danger" type="submit">
                                                             <i class="fa fa-trash"></i>
                                                    </button> 
                                                </form>
                                            </td>
                                        </tr>  
                                    <?php } ?>   
                                </tbody>
                            </table>
                        </div>                            
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>            
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!--    </div>
         /.container -->
</div>
<!-- /.content-wrapper -->
<!-- modal  -->

<!-- Button trigger modal -->

<!-- Modal -->
<?php
//echo $_SESSION['email'];
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Update Skills </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action=<?= base_url(); ?>profile/skillsInsert method="post">
                    <div class="row">
                        <div class="col-md-12"> 
                            <input type="hidden" name="skills_list_id" value="0">
                           
                            <input type="hidden" name="id" value="<?= $basicinfo[0]->id; ?>">
                            <input type="hidden" name="skills" value="1">
                            <!--Profile::iif($languages, 'skills_head_id');-->
                            <div class="col-md-4"> 
                                <div style="margin: 0 5px;" class=" form-group">
                                <label> Language </label>
                                <select name="language" class="form-control">
                                    <?php foreach ($languages_list as $language) { ?>
                                        <option value="<?= $language->id; ?>"><?= $language->language_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="margin: 0 5px;" class=" form-group">
                                <label> Reading </label>
                                <select name="reading" class="form-control">
                                    <?php foreach ($language_label as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value; ?></option>                        
                                    <?php } ?>
                                </select>
                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div style="margin: 0 5px;" class=" form-group">
                                <label> Writing </label>
                                <select name="writing" class="form-control">
                                    <?php foreach ($language_label as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value; ?></option>                        
                                    <?php } ?>
                                </select>
                            </div>                            
                        </div>
                    </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <input type="submit" class="btn btn-primary" name="update" value="Save changes">
            </div>
            </form>
        </div>
    </div>
</div>