<style>
    #dropzone {
        /*        background: palegreen;
                width: 150px;
                height: 50px;
                line-height: 50px;
                text-align: center;
                font-weight: bold;*/
    }
    #dropzone.in {
        /*        width: 600px;
                height: 200px;
                line-height: 200px;
                font-size: larger;*/
    }
    #dropzone1.hover {
        background: rgba(126,182,111, .5);
        color: #fff;
    }
    #dropzone1.fade {
        -webkit-transition: all 0.3s ease-out;
        -moz-transition: all 0.3s ease-out;
        -ms-transition: all 0.3s ease-out;
        -o-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
        opacity: 1;
    }  
    .file-upload {
        position: relative;
        overflow: hidden;
        margin: 10px; }

    .file-upload input.file-input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0); 
    }
</style>

<div class="container">
    <h1 class="margin-top-50 text-center text-bold "> Employer registration </h1>
    <div class="emp-registration">
        <?php
        $attributes = array('class' => 'email', 'id' => 'form-registration');
        echo form_open('employer/registration/create', $attributes);
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="companyName">Company Name</label> 
                    <span class="text-red field-error "><?php echo form_error('companyName'); ?></span>
                    <input name="companyName"  type="text" value="<?php echo Common::iifPost($object = NULL, 'companyName'); ?>" class="form-control" id="companyName" placeholder="Company Name" required>
                </div>                                                                  
            </div>                
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="title">Industry</label> 
                    <span class="text-red field-error "> <?php echo form_error('industry'); ?> </span>
                    <input name="industry"  type="text" value="<?php echo Common::iifPost($object = NULL, 'industry'); ?>" class="form-control" id="Industry" placeholder="Industry" required>
                </div> 
            </div> 
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="structure"> Structure </label> 
                    <span class="text-red field-error "><?php echo form_error('Structure'); ?></span>
                    <input name="structure"  type="text" value="<?php echo Common::iifPost($object = NULL, 'structure'); ?>" class="form-control" id="structure" placeholder="Structure" required>
                </div> 
            </div>       
        </div>    
        <!-- Company Size start -->    
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="title"> Company Size </label> 
                    <span class="text-red field-error "><?php echo form_error('preferedcompanySize'); ?></span>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="preferedcompanySize text-center">
                                    <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/small-business-icon.png" alt="Small Business">
                                    <label> Small Business </label>
                                    <p> ( up to 500 headcounts ) </p>
                                    <input name="preferedcompanySize" value="1"  type="radio">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="preferedcompanySize text-center">
                                    <img  src="<?php echo BASE_URL; ?>assets/images/theme/icon/medium-sized.png" alt="Medium Sized">
                                    <label> Medium Sized </label>
                                    <p> ( 500-5000 people ) </p>
                                    <input name="preferedcompanySize" value="2"  type="radio">
                                </div>
                            </div>
                        </div>                   
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="preferedcompanySize text-center">
                                    <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/a-large-corporation.png" alt="A large Corporation">
                                    <label> A Large Corporation </label>
                                    <p style="color:#fff"> . </p>
                                    <input name="preferedcompanySize" value="3"  type="radio">
                                </div>
                            </div>
                        </div>                   
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="preferedcompanySize text-center">
                                    <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/global-business.png" alt="A Global Corporation ">
                                    <label> A Global Corporation </label>
                                    <p style="color:#fff"> . </p>
                                    <input name="preferedcompanySize" value="4"  type="radio">
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div> 
        </div>  
        <!-- Company Size end -->                    

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="mainTelephone">Main telephone</label> 
                    <span class="text-red field-error "><?php echo form_error('mainTelephone'); ?></span>
                    <input name="mainTelephone"  type="text" value="<?php echo Common::iifPost($object = NULL, 'mainTelephone'); ?>" class="form-control" id="mainTelephone" placeholder="Main Telephone" required>
                </div>                     
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="website"> Website </label> 
                    <span class="text-red field-error "><?php echo form_error('website'); ?></span>
                    <input name="website"  type="text" value="<?php echo Common::iifPost($object = NULL, 'website'); ?>" class="form-control" id="Website" placeholder="Website" required>
                </div>                     
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="buildingHouseNo">Building/ House No</label> 
                    <span class="text-red field-error "><?php echo form_error('buildingHouseNo'); ?></span>
                    <input name="buildingHouseNo"  type="text" value="<?php echo Common::iifPost($object = NULL, 'buildingHouseNo'); ?>" class="form-control" id="buildingHouseNo" placeholder="buildingHouseNo" required>
                </div>                     
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="streetName"> Street name </label> 
                    <span class="text-red field-error "><?php echo form_error('streetName'); ?></span>
                    <input name="streetName"  type="text" value="<?php echo Common::iifPost($object = NULL, 'streetName'); ?>" class="form-control" id="streetName" placeholder="Street name" required>
                </div>                     
            </div>
        </div>            
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="addressLine">Address line</label> 
                    <span class="text-red field-error "><?php echo form_error('addressLine'); ?></span>
                    <input name="addressLine"  type="text" value="<?php echo Common::iifPost($object = NULL, 'addressLine'); ?>" class="form-control" id="addressline" placeholder="Address line" required>
                </div>                     
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="postalTown"> Postal town </label> 
                    <span class="text-red field-error "><?php echo form_error('postalTown'); ?></span>
                    <input name="postalTown"  type="text" value="<?php echo Common::iifPost($object = NULL, 'postalTown'); ?>" class="form-control" id="postalTown" placeholder="Postal Town" required>
                </div>                     
            </div>
        </div> 

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="companyBiography">Company Biography</label> 
                    <span class="text-red field-error "><?php echo form_error('companyBiography'); ?></span>
                    <textarea rows="6" name="companyBiography"  type="text" class="form-control" id="companyBiography" placeholder="Company Biography" required><?php echo Common::iifPost($object = NULL, 'companyBiography'); ?></textarea>
                </div>                     
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cultureAndValues">Culture and values</label> 
                    <span class="text-red field-error "><?php echo form_error('cultureAndValues'); ?></span>
                    <textarea rows="6" name="cultureAndValues"  type="text"class="form-control" id="cultureAndValues" placeholder="Company Biography" required><?php echo Common::iifPost($object = NULL, 'cultureAndValues'); ?></textarea>
                </div>                     
            </div>
        </div>                  

        <div class="row">
            <!-- Button trigger modal -->      
            <div class="col-md-12">
                <button id="add-company-user" type="button" class="btn btn-theme-2 text-bold" data-toggle="modal" data-target="#add-company-user-form">Add company user</button>
            </div>   
        </div>  

        <div class="row">
            <div class="col-md-6 margin-top-25">
                <!-- Video Presentation Start -->
                <div class="form-group">
                    <label class="file-upload">Video Presentation</label>      
                    <div class="upload-doc text-center">
                        <p class="draganddrop"> Click on the button or <br>
                            Drag & Drop a document files here  </p>
                        <div class="uploading-msg"></div>
                        <div class="form-group">
                            <label class="file-upload">            
                                <input id="company-video-presentation" name="file" type="file" class="fileupload file-input" data-url = "<?php echo BASE_URL?>upload/companyvideopresentation" > Upload a Video
                            </label>
                        </div>
                    </div>
                </div>                    
                <!-- Video Presentation End -->
            </div>  
            <div class="col-md-6  margin-top-25">
                <!-- company Image start -->
                <div class="form-group">
                    <label class="file-upload"> Company Picture </label>       
                    <div class="upload-doc text-center">
                        <p class="draganddrop "> Click on the button or <br>
                            Drag & Drop a document files here  </p>
                        <div class="uploading-msg"></div>
                        <div class="form-group">
                            <label class="file-upload">            
                                <input id="company-picture" name="file" type="file" class="fileupload file-input" data-url="<?php echo BASE_URL?>upload/companyimagepicture" >Upload a Picture
                            </label>
                        </div>
                    </div>
                </div>                    
                <!-- company Image end -->
            </div>                 
        </div>   
        <div class="row">
            <div class="col-md-12">
                <div class="text-center margin-top-25 margin-bottom-50 ">
                    <button type="submit" class="btn bg-olive-active"> DONE </button>
                </div>
            </div>
        </div>                    
        </form>
    </div>   
</div>
<div id="add-company-user-form" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-center margin-top-25 margin-bottom-25 "><img src="<?php echo BASE_IMG ?>icon/add-new-company-user.png" width="100" height="100" alt="ADD COMPANY USER"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <?php
                        $attributes = array('id' => 'form-companyuser-registration');
                        echo form_open('employer/registration/createcompanyuser', $attributes);
                        ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="firstName"> First Name </label> 
                                <span class="text-red field-error "><?php echo form_error('firstName'); ?></span>
                                <input name="firstName"  type="text" value="<?php echo Common::iifPost($object = NULL, 'firstName'); ?>" class="form-control" id="firstName" required>
                            </div>                     
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="lastName"> Last Name </label> 
                                <span class="text-red field-error "><?php echo form_error('lastName'); ?></span>
                                <input name="lastName"  type="text" value="<?php echo Common::iifPost($object = NULL, 'lastName'); ?>" class="form-control" id="lastName" required>
                            </div>                     
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="jobPosition"> Job position </label> 
                                <span class="text-red field-error "><?php echo form_error('jobPosition'); ?></span>
                                <input name="jobPosition"  type="text" value="<?php echo Common::iifPost($object = NULL, 'jobPosition'); ?>" class="form-control" id="jobPosition" required>
                            </div>                     
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email"> Email </label> 
                                <span class="text-red field-error "><?php echo form_error('email'); ?></span>
                                <input name="email"  type="email" value="<?php echo Common::iifPost($object = NULL, 'email'); ?>" class="form-control" id="email" required>
                                <span class="text-red email-error field-error help-block"></span>
                            </div>                     
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password"> Password </label> 
                                <span class="text-red field-error "><?php echo form_error('password'); ?></span>
                                <input name="password"  type="password" value="<?php echo Common::iifPost($object = NULL, 'password'); ?>" class="form-control" id="password" required>
                            </div>                     
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cpassword"> Confirm Password </label> 
                                <span class="text-red field-error "><?php echo form_error('cpassword'); ?></span>
                                <input name="cpassword"  type="password" value="<?php echo Common::iifPost($object = NULL, 'cpassword'); ?>" class="form-control" data-match="#password" data-match-error="Whoops, these don't match"  id="cpassword" required>
                            </div>                     
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button id="button-create-user" type="submit" name="createNewUser" class="btn bg-olive-active"> Create user </button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script> var IMG = "<?php echo BASE_IMG ?>"</script>