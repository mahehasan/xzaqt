<!-- /.content-wrapper -->
<!--<footer class="main-footer footer navbar-fixed-bottom main-footer">-->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> Alpha 1.0
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="#"> Jobsite </a>.</strong> All rights
    reserved.
    <!-- /.container -->
</footer>
</div>
<div id="xzaqt-message-alert" class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong> Validation! Message : </strong> <span class="validation_text"></span>
</div> 
<!-- jQuery 2.2.0 -->
<script src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/jQueryUI/jquery-ui.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo BASE_URL; ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/fastclick/fastclick.js"></script>

<!-- date and calender -->
<script src="<?php echo BASE_URL; ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- bootstrap datepicker -->
<script src="<?php echo BASE_URL; ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo BASE_URL; ?>assets/plugins/chartjs/Chart.min.js"></script>

<!-- Upload Pro image start  -->
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/vendor/jquery.ui.widget.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.iframe-transport.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.fileupload.js"></script>  

<!-- Upload Pro image end -->


<script>
    $(function () {
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }

        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random default events
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1),
                    backgroundColor: "#f56954", //red
                    borderColor: "#f56954" //red
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: "#f39c12", //yellow
                    borderColor: "#f39c12" //yellow
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    backgroundColor: "#0073b7", //Blue
                    borderColor: "#0073b7" //Blue
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    backgroundColor: "#00c0ef", //Info (aqua)
                    borderColor: "#00c0ef" //Info (aqua)
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false,
                    backgroundColor: "#00a65a", //Success (green)
                    borderColor: "#00a65a" //Success (green)
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    backgroundColor: "#3c8dbc", //Primary (light-blue)
                    borderColor: "#3c8dbc" //Primary (light-blue)
                }
            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });

        /*************** pie chart ********************************/

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var PieData = [
            {
                value: 40,
                color: "#7eb66f",
                highlight: "#f56954",
                label: "Invited to apply or interview"
            },
            {
                value: 40,
                color: "#dd5f32",
                highlight: "#00a65a",
                label: "Jobs completed "
            },
            {
                value: 30,
                color: "#69bac8",
                highlight: "#00a65a",
                label: "Running Job "
            }
        ];
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: false,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 0,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        if ($("#pieChart").length != 0) {
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            pieChart.Doughnut(PieData, pieOptions);
        }
        //-------------
        //- Emp1  CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var empChartOneData = [
            {
                value: 45,
                color: "rgba(126,182,111, 0.9)",
                highlight: "rgba(126,182,111, 1)",
                label: "Invited"
            },
            {
                value: 40,
                color: "rgba(221,95,50, 0.9)",
                highlight: "rgba(221,95,50, 1)",
                label: "Applied "
            },
            {
                value: 20,
                color: "rgba(105,186,200, 0.9)",
                highlight: "rgba(105,186,200, 1)",
                label: "Shortlisted "
            }
        ];
        var empChartOneOption = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: false,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 0,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 105,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        if ($("#empChartOne").length != 0) {
            var empChartOne = $("#empChartOne").get(0).getContext("2d");
            var empChartOneObj = new Chart(empChartOne);
            empChartOneObj.Doughnut(empChartOneData, empChartOneOption);
        }

        //-------------
        //- Emp2  CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var empChartTwoData = [
            {
                value: 20,
                color: "rgba(126,182,111, .8)",
                highlight: "rgba(126,182,111, 1)",
                label: "Interview"
            },
            {
                value: 5,
                color: "rgba(221,95,50,.9)",
                highlight: "rgba(221,95,50,1)",
                label: "Offers"
            },
            {
                value: 6,
                color: "rgba(105,186,200,.9)",
                highlight: "rgba(105,186,200,.9)",
                label: "Hired"
            }
        ];
        var empChartTwoOption = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: false,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#000",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 0,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 31,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        if ($("#empChartTwo").length != 0) {
            var empChartTwo = $("#empChartTwo").get(0).getContext("2d");
            var empChartTwoObj = new Chart(empChartTwo);
            empChartTwoObj.Doughnut(empChartTwoData, empChartTwoOption);
        }
        //LINE CHART
        var areaChartData = {
            labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "12"],
            datasets: [
                {
                    label: "ProfileView",
                    fillColor: "#45a72b",
                    strokeColor: "#45a72b",
                    pointColor: "#45a72b",
                    pointStrokeColor: "#7eb66f",
                    pointHighlightFill: "#196c03",
                    pointHighlightStroke: "#45a72b",
                    data: [0, 10, 30, 20, 40, 10, 60, 10, 80, 90, 100]
                }
            ]
        };

        var areaChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: "#90ccd6",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: false,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        if ($("#lineChart").length != 0) {
            var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
            var lineChart = new Chart(lineChartCanvas);
            var lineChartOptions = areaChartOptions;
            lineChartOptions.datasetFill = false;
            lineChart.Line(areaChartData, lineChartOptions);
        }

        /*******************
         * Upload Doc Start
         *******************/
        $('#edit-profile-pic').fileupload({
            dataType: 'html',
            add: function (e, data) {
                data.context = $(this).closest('div.image').children('img').attr('src', "<?php echo BASE_IMG ?>icon/loader.gif");
                //$(this).closest('div.image').children('img').attr('src', "<?php //echo BASE_IMG ?>icon/loader.gif");
                //console.log($(this).closest('div.image').children('img').attr('src'));
                data.submit();
                $('#personal-info-group').hide();
            },
            done: function (e, data) {
                $('div.uploading-msg p').remove();
                $('#personal-info-group').show();
                var messageDiv = $("div#xzaqt-message-alert");
                var result = data._response.result;  
                var resultObj = jQuery.parseJSON(result);
                if (resultObj.success == 1) {
                    result = 'Profile Picture Uploaded Successfully ';
                    $(this).closest('div.image').children('img').attr('src', "<?php echo BASE_URL ?>uploads/"+resultObj.name);
                    console.log(messageDiv);
                    if (messageDiv.hasClass("alert-warning")) {
                        messageDiv.removeClass("alert-warning")
                                .addClass('alert-success');
                    }
                } else {
                    result = resultObj.error;
                    $(this).closest('div.image').children('img').attr('src', "<?php echo base_url()?>assets/images/avatar_img.png");
                    if (messageDiv.hasClass("alert-success")) {
                        messageDiv.removeClass("alert-success")
                                .addClass('alert-warning');
                    }
                  
                }
                messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
                messageDiv.delay(3000).fadeOut("slow");
            }
        });

        $('#edit-profile-pic').fileupload({
            dropZone: $('#dropzone')
        });

        $(document).bind('dragover', function (e) {
            var dropZone = $('#dropzone'),
                    timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            var found = false,
                    node = e.target;
            do {
                if (node === dropZone[0]) {
                    found = true;
                    break;
                }
                node = node.parentNode;
            } while (node != null);
            if (found) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });
        /*******************
         * Upload Doc End
         *******************/


    });
</script>

</div>  
</body>
</html>