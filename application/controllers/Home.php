<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //  $this->load->model('show_model');
    }

    public function index() {
        if ($this->session->userdata('isUserConnected')) { 
            Common::redirectTo($this->session->userdata('user_type'));
        }
        $data['title'] = 'Home';
        Template::set_view('common/header_home', $data);
        Template::set_view('home/home');
        Template::set_view('common/footer_home');
    }

}
