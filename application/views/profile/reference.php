<style>
    #dropzone {
        /*        background: palegreen;
                width: 150px;
                height: 50px;
                line-height: 50px;
                text-align: center;
                font-weight: bold;*/
    }
    #dropzone.in {
        /*        width: 600px;
                height: 200px;
                line-height: 200px;
                font-size: larger;*/
    }
    #dropzone.hover {
        background: rgba(126,182,111, .5);
        color: #fff;
    }
    #dropzone.fade {
        -webkit-transition: all 0.3s ease-out;
        -moz-transition: all 0.3s ease-out;
        -ms-transition: all 0.3s ease-out;
        -o-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
        opacity: 1;
    }  
    .file-upload {
        position: relative;
        overflow: hidden;
        margin: 10px; }

    .file-upload input.file-input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0); 
    }
</style>
<div class="content" id="content-your-references" style="display: none;">
    <div class="row margin-bottom-50">
        <div class="col-md-1"> </div>
        <div class="col-md-8">
            <div class="skill-title-bar">
                <span class="skill-circle"><i class=" fa fa-fw fa-users"></i></span>
                <span class="thm-text "> References   </span>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>

    <form role="form" action="<?php echo BASE_URL; ?>candidate/profilesetup/references" method="post" id="your-references-frm">
        <div class="row"> 
            <div class="col-md-12">
                <div class="col-md-1"> </div>
                <div class="col-md-5 border-left">
                    <div class="form-group">
                        <label for="name"> Name </label> 
                        <?php echo form_error('name'); ?>
                        <input name="name"  type="text" value="" class="form-control" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email"> email </label> 
                        <?php echo form_error('email'); ?>
                        <input name="email"  type="text" value="" class="form-control" id="email" required>
                    </div>  
                    <div class="form-group">
                        <label for="jobTitle"> Job Title </label> 
                        <?php echo form_error('jobTitle'); ?>
                        <input name="jobTitle"  type="text" value="" class="form-control" id="jobTitle" required>
                    </div>                     
                    <div class="form-group">
                        <label for="relation"> Relationship </label> 
                        <?php echo form_error('relation'); ?>
                        <input name="relation"  type="text" value="" class="form-control" id="relation" required>
                    </div>  
                    <div class="form-group">
                        <div class="upload-doc text-center">
                            <p class="draganddrop "> Click on the button or <br>
                                Drag & Drop a document files here 
                                <br> Allow File <span class="text-sm text-black text-bold "> pdf, doc, docx</span>
                            </p>
                            <div class="uploading-msg">
                            </div>
                            <div id="reference-info-group" class="form-group">
                                <label class="file-upload">            
                                    <input name="file" type="file" id="reference-doc" class="file-input" data-url="<?php echo BASE_URL ?>upload/referencedoc" >Upload a Doc
                                </label>
                            </div>
                        </div>
                    </div>                     
                </div>

                <div class="col-md-5">
                    <div class="row">
                        <div class="form-group text-center reference-2nd">
                            <div>
                                <img src="<?php echo BASE_URL; ?>assets/images/theme/icon/reference-icon.png" width="80" height="95">
                            </div>
                            <div class="margin-10  ">
                                <p class="theme-text-color"> Dont' have a reference !</p>
                                <p> Request a recommendation now  </p>

                            </div>
                        </div>   
                    </div>
                    <div class="form-group">
                        <label for="refereeFullName"> Referee full name </label> 
                        <?php echo form_error('refereeFullName'); ?>
                        <input name="refereeFullName"  type="text" value="" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="refereeEmail"> Referee Email </label> 
                        <?php echo form_error('refereeEmail'); ?>
                        <input name="refereeEmail"  type="text" value="" class="form-control" >
                    </div>
                    <div class="form-group text-center">
                        <btton class='btn btn-theme-2'> Request </button>
                    </div>
                </div>
                <div class="col-md-1"> </div>
            </div> 
        </div>

        <div class="row"> 
            <div class="col-md-12 text-center margin-bottom-50 margin-top-50">
                <button class="btn btn-theme" name="save"> Add other reference </button>
                <button type="submit" class="btn btn-theme save-role-preference" name="save" id="save-your-references"> Save & Next </button>
            </div>
        </div>
    </form>
</div> <!-- Employment History End -->  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/vendor/jquery.ui.widget.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.iframe-transport.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/plugins/jquery-blue-imp/jquery.fileupload.js"></script>  
<script src="<?php echo BASE_URL; ?>assets/js/profile/reference.js"></script>

<script>

    $('#reference-doc').fileupload({
        dataType: 'html',
        add: function (e, data) {
            data.context = $('<p/>').append('<img src="<?php echo BASE_IMG ?>icon/loader.gif" alt="uploading..">').appendTo($('div.uploading-msg'));
            data.submit();
            //$('div#reference-info-group').show();
        },
        done: function (e, data) {
            $('div.uploading-msg p').remove();
            //$('div#reference-info-group').hide();
            var messageDiv = $("div#xzaqt-message-alert");
            var result = data._response.result;
            if (result == 1) {
                result = 'Document Successfully';
                if (messageDiv.hasClass("alert-warning")) {
                    messageDiv.removeClass("alert-warning")
                            .addClass('alert-success');
                }
            } else {
                if (messageDiv.hasClass("alert-success")) {
                    messageDiv.removeClass("alert-success")
                            .addClass('alert-warning');
                }
            }
            messageDiv.fadeIn("slow").find(".validation_text").empty().append(result);
            messageDiv.delay(2000).fadeOut("slow");
        }
    });

    $('#reference-doc').fileupload({
        dropZone: $('#dropzone')
    });

    $(document).bind('dragover', function (e) {
        var dropZone = $('#dropzone'),
                timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
                node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        if (found) {
            dropZone.addClass('hover');
        } else {
            dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

</script>