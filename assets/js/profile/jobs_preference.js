$(function () {
    $('.status-message-emp').hide();
    var submitType;
    var messageDiv = $("div#xzaqt-message-alert");
    $("button[name='addJobPref']").on('click', function () {
        submitType = $(this).val();
        // remove space 
        submitType = submitType.replace(/\s/g, '');
        // make string lower
        submitType = submitType.toLowerCase();
    });
    $("button[name='addJobPrefNext']").on('click', function () {
        submitType = $(this).val();
        // remove space 
        submitType = submitType.replace(/\s/g, '');
        // make string lower
        submitType = submitType.toLowerCase();
    });
    $('#jobs-preferences-frm').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        // var url = BASE_URL + "profilesetup/jobs_preferences";
         var url = $(this).attr('action');
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                if (submitType == 'addjobprefnext') {
                    $('#content-jobs-preferences').hide();
                    $('#content-your-preferred-workplace-industries').show();
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Jobs Preferences added Successfully.');
                    messageDiv.delay(1700).fadeOut("slow");
                } else {
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Jobs Preferences Successfully and ready to add another');
                    messageDiv.delay(1700).fadeOut("slow");
                }
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });
    });
});

