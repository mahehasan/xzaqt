<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="login-box">
    <div class="login-logo">
       
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <script> var BASE_URL = "<?php echo BASE_URL; ?>"; </script>
        <?php
        $attributes = array('role' => 'form', 'id'=>'set-user-type');
        echo form_open('hauth/setusertype_validation', $attributes);
        ?>
        <div class="form-group has-feedback">
            <button id="emp-dash-candidate" type="submit" value="2" class="btn btn-info btn-lg btn-block"> Login as Candidate </button>
            <button id="emp-dash-employer" type="submit" value="1" class="btn btn-theme-3 btn-lg btn-block"> Login as Employer </button>
        </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>