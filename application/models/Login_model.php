<?php //

class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function can_log_in() {
        $this->db->where('email', $this->input->post('email'));
        $this->db->where('password', md5($this->input->post('password')));
        $this->db->where('type', $this->input->post('type'));
        $query = $this->db->get('users');
        if($query->num_rows() == 1){
            $result = $query->result();
            $user_data = $result[0];
            $this->session->set_userdata('id', $user_data->id);            
            return true;
        } else { 
            return false;
        }
    }
    
    public function insert_entry($data) {
        $this->db->insert('source', $data);
    }
    
    public function totalrow() {
        $query = $this->db->get('source');
        return count($query->result());
    }
    
    public function get_data($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by("id","desc")->get("source");
        return $query->result();
    }
    
    public function add_temp_users($key) {
        
        $data = array(
            'email' => $this->input->post('email'),
            'type' => $this->input->post('type'),
            'password' => md5($this->input->post('password')),
            'key' => $key 
        );
       
        $query = $this->db->insert('temp_users', $data);
        if($query){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function is_valid_key($key) {
        $this->db->where('key', $key); 
        $query = $this->db->get('temp_users');
        if($query->num_rows() == 1){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function add_user($key) {
        $this->db->where('key', $key);
        $temp_users = $this->db->get('temp_users');
        
        if($temp_users){
            $row = $temp_users->row();
            $data = array(
                'email' => $row->email,
                'type' => $row->type,
                'password' => $row->password
            );
            $did_add_user = $this->db->insert('users', $data);
        }
        if($did_add_user){
            $this->db->where('key', $key);
            $this->db->delete('temp_users');
            return $data; 
        } else {
            return FALSE;
        }
    }
    
    public function add_company_user($key) {
        $this->db->where('key', $key);
        $temp_users = $this->db->get('temp_users');
        
        if($temp_users){
            $row = $temp_users->row();
            $data = array(
                'email' => $row->email,
                'type' => $row->type,
                'password' => $row->password
            );
            $did_add_user = $this->db->insert('users', $data);
        }
        if($did_add_user){
            $this->db->where('key', $key);
            $this->db->delete('temp_users');
            return $data['email']; 
        } else {
            return FALSE;
        }
    }
    
    public function check_email($email) {
        $query = $this->db->where('email', $email)->get('users'); 
        if($query->num_rows()>0){
             return $query->result();
        } else{
            return false;
        }
    }
    
    public function get_email($id){
        $query = $this->db->where('id', $id)
                 ->get('users'); 
         return $query->result();
    }
    
    public function check_temp_data($key) {
        $query = $this->db->where('key', $key)
                 ->get('temp_users'); 
        if($query->num_rows()>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}