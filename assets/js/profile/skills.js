$(function () {
    if (langjson) {
        var lang1 = langjson[0]['skills_id'];
        var lang2 = langjson[1]['skills_id'];
        var lang1_level = langjson[0]['fluency_level'];
        var lang2_level = langjson[1]['fluency_level'];

        // select 2 selecttor 
        //$("[name='language1']").val(2).trigger("change");
        function selectSkill(selector, lang) {
            selector.val(lang).trigger("change")
        }
        selectSkill($("[name='language1']"), lang1);
        selectSkill($("[name='language2']"), lang2);
    }
    if (comjson) {
        var com1 = comjson[0]['skills_id'];
        var com2 = comjson[1]['skills_id'];
        var com3 = comjson[2]['skills_id'];
        var com1_level = comjson[0]['fluency_level'];
        var com2_level = comjson[1]['fluency_level'];
        var com3_level = comjson[2]['fluency_level'];

        selectSkill($("[name='comSkillOne']"), com1);
        selectSkill($("[name='comSkillTwo']"), com2);
        selectSkill($("[name='comSkillThree']"), com3);
    }

    $('#save-next').on('click', function () {
        
        if(addlanguage($('#form-language')) && addComSkills($('#computerSkills'))){
            $('.nav-tabs a[href="#employment-history"]').tab('show');      
        } else {
            $('div#skills-error-notice').show(1000, function () {
                $(this).fadeIn(3000);
                $(this).hide(10000, function () {
                    //$(this).fadeOut(8000);
                });
            });            
        }
    });

    $('#status-message-language').hide();
    $('#status-message-com').hide();
    $('#form-language').on('submit', function (e) {
        e.preventDefault();
        addlanguage($(this));
    });

    $('#computerSkills').on('submit', function (e) {
        e.preventDefault();
        addComSkills($(this));
    });

});

// add comskills
function addComSkills(selectedform) {
    var data = selectedform.serialize();

    var com_level_1 = $('#skills1').find('span.active-pro').attr('value');
    var com_level_2 = $('#skills2').find('span.active-pro').attr('value');
    var com_level_3 = $('#skills3').find('span.active-pro').attr('value');

    if (com_level_1 && com_level_2 && com_level_3) {

        data = data + '&com_level_1=' + com_level_1 + '&com_level_2=' + com_level_2 + '&com_level_3=' + com_level_3;

        var url = BASE_URL + "candidate/profilesetup/addComSkills";

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
                $('#status-message-language').show(1000, function () {
                    $(this).delay(3000).hide(100);
                });
            
                if (results.length) {
                    var data = results;
                    //$('#status-message').css("color", "#337ab7").text('Data inserted successfully');

                    var com1 = data[0]['skills_id'];
                    var com2 = data[1]['skills_id'];
                    var com3 = data[2]['skills_id'];
                    var com1_level = data[0]['fluency_level'];
                    var com2_level = data[1]['fluency_level'];
                    var com3_level = data[2]['fluency_level'];
                }
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });
        return true;
    } else {
        return false;
    }

}

// add language 

function addlanguage(selectedform) {

    var data = selectedform.serialize();
    var lang_level_1 = $('#language1').find('span.active-pro').attr('value');
    var lang_level_2 = $('#language2').find('span.active-pro').attr('value');
    data = data + '&lang1_level=' + lang_level_1 + '&lang2_level=' + lang_level_2;
    if (lang_level_1 && lang_level_2) {

        var url = BASE_URL + "candidate/profilesetup/addskills";
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (results) {
                $('#status-message-language').show(1000, function () {
                    $(this).delay(3000).hide(100);
                });
                if (results.length) {
                    var data = results;


                    var lang1 = data[0]["skills_id"];
                    var lang2 = data[1]["skills_id"];

                    var lang1_level = data[0]["fluency_level"];
                    var lang2_level = data[1]["fluency_level"];
                }
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });
        return true;
    } else {
        return false;
    }
}
