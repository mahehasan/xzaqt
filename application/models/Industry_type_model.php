<?php

class Industry_type_model extends CI_Model {

    private $tableName = 'settings_industry_type';

    public function __construct() {
        parent::__construct();
    }

    public function get() {
        $data = Common::findAll($this->tableName);
        return $data;
    }

    public function add() {
        $data = array(
            'name' => $this->input->post('name'),
        );

        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
