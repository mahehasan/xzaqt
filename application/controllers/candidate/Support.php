<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('profile_model');
    }

    public function index() {
        if ($this->session->userdata('isUserConnected')) {
            $this->indexdata();
        } else {
            redirect('hauth');
        }
    }

    public function indexdata() {
        $data['title'] = 'Profile Public';
        $data['view_name'] = 'profile/profile_public';    
        Template::set_view('common/header_home', $data); 
        Template::set_view('profile/profile_public');
        Template::set_view('common/footer_home');    
    }   
}
