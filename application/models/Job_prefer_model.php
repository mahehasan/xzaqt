<?php

class Job_prefer_model extends CI_Model {

    private $tableName = 'job_preference';
    
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $uid = Common::user('id');       
        $data = array(
            'user_id' => $uid,
            'roll_interested' => $this->input->post('rollInterested'),
            'prefered_wlocation' => $this->input->post('preferedWlocation'),
            'minimum_rate' => $this->input->post('minimumRate'),
            'prefered_rate' => $this->input->post('preferedRate'),
            'role_type' => $this->input->post('roleType'),
            'role_duration' => $this->input->post('roleDuration'),
            'work_start_time' => $this->input->post('workStartTime'),
            'work_status' => $this->input->post('workStatus'),
            'benefits' => $this->input->post('benefits'),
            'pensions_plan' => $this->input->post('pensionsPlan'),
            'health_insurance' => $this->input->post('healthInsurance'),
            'life_insurance' => $this->input->post('lifeInsurance'),
            'bonus_payment' => $this->input->post('bonusPayment'),
            'contract_future' => $this->input->post('contractFuture'),
        );
        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
