$(function () {
    var submitType;
    var messageDiv = $("div#xzaqt-message-alert");
    $("input[name='addEmp']").on('click', function () {
        submitType = $(this).val();
        // remove space 
        submitType = submitType.replace(/\s/g, '');

        //make string lower
        submitType = submitType.toLowerCase();
    });

    $("button[name='addNext']").on('click', function () {
        submitType = $(this).val();

        // remove space 
        submitType = submitType.replace(/\s/g, '');

        //make string lower
        submitType = submitType.toLowerCase();
    });
    $('.status-message-emp').hide();
    $('#form-empHistory').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        //var url = BASE_URL + "profilesetup/addemphistory";
        var url = $(this).attr('action');

        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
//            dataType: 'json',
            success: function (results) {
                var progress = results + "%";
                $('#pregressbar').css("width", progress);
                if(submitType == 'save&next'){
                    $('.nav-tabs a[href="#education"]').tab('show');
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Employment History added Successfully.');
                    messageDiv.delay(1700).fadeOut("slow");
                } else {
                    messageDiv.fadeIn("slow");
                    messageDiv.find(".validation_text").text('Employment History Successfully and ready to add another');
                    messageDiv.delay(1700).fadeOut("slow");
                }
                
                $('span.show-barprogerss').empty();
                $('span.show-barprogerss').append(progress);
            }
        });
    });

});

