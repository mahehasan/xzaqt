<?php

class Reference_model extends CI_Model {

    private $tableName = 'references';
    
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get() {
        $data = findAll($this->tableName);
        return $data;
    }

    public function add() {
        $uid = Common::user('id');
        
        $data = array(
            'user_id' => $uid,
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'job_title' => $this->input->post('jobTitle'),
            'relationship' => $this->input->post('relation'),
            'file_path' => '',
            'create_date' => time()
        );

        Common::save($this->tableName, $data);
    }

    public function save() {
        
    }

}
