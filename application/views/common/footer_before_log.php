<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/set_user_type.js"></script>
<script src="<?php echo BASE_URL; ?>assets/plugins/bootstrap-validatior/bootstrapvalidator.min.js"></script>
<script>
    $(function () {
        if ($('.remember').length != 0) {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }
        if ($('#country_selector').length != 0) {
            $("#country_selector").countrySelect({
                //defaultCountry: "jp",
                //onlyCountries: ['gb'],
                //preferredCountries: ['ca', 'gb', 'us']
            });
        }
    });


    $(function () {
        $('#form-registration').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: ' Please fill the field'
                        },
                        emailAddress: {
                            message: 'Please insert an email address '
                        },
                        
                    }
                }, 
                type: {
                    validators: {
                        notEmpty: {
                            message: ' Please select an option'
                        },
                        feedback: {
                            success: ' ',
                            error: ' '
                        }
                        
                    }
                },                
                
                password: {
                    validators: {
                        stringLength: {
                            min: 6,
                            message: 'Please put at least 6 character'
                        },
                        notEmpty: {
                            message: 'Please fill the field'
                        }
                    }
                }, cpassword: {
                    validators: {
                        stringLength: {
                            min: 6,
                            message: ''
                        },
                        notEmpty: {
                            message: 'Please fill the field'
                        }, 
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm must be the same'
                        }
                    }
                },
            }
        })
                .on('success.form.bv', function (e) {
                    $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                    $('#form-companyuser-registration').data('bootstrapValidator').resetForm();

                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function (result) {
                        console.log(result);
                    }, 'json');
                });
                
       
    });
</script>
</body>
</html>