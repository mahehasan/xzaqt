<!-- Full Width Column -->
<div class="content-wrapper">
    <?php
    //echo '<pre>';
    //print_r($basicinfo);
    // echo '</pre>';
    ?>
    <!--<div class="container">-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $title; ?> </h1>
        <!--        <ol class="breadcrumb">
            <li><a href="<?php // echo base_url();  ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php // echo base_url();  ?>">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>-->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
        <?php if(isset($_SESSION['update_true'])){ ?>
            
        <div  class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <button id="update_warning" type="button" class="close" data-dismiss="update_warning" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p>Data Updated Successfully.</p>
            </div>
        </div>
         <?php
         unset($_SESSION['update_true']);
        } ?>  
            <div class="col-md-12">
                <div class="box box-primary">
                    
                    <div class="box-header with-border">
                    
                        <h3 class="box-title"> <!-- Contact Info --> </h3>
<!--                        <a class="btn btn-warning pull-right" href="<?php // echo base_url(); ?>profile/contactInfo"><i class="fa fa-edit"></i> Edit</a>-->
                        <button class="btn btn-warning pull-right" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-edit"></i> Edit
                        </button>            
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body datalist">
                        <div class="row">
                            <div class="col-md-4"> <p> Name </p></div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'first_name'); Profile::iif($basicinfo,'last_name');?>  </div>                            
                        </div> 
                        <div class="row">
                            <div class="col-md-4"> <p>Email</p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'email'); ?> </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"> <p>Job Title</p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'job_title'); ?> </div>
                        </div>                        
                        <div class="row">                                                   
                            <div class="col-md-4"> <p>Mobile</p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'mobile'); ?>  </div>
                        </div>
                        <div class="row">                                                     
                            <div class="col-md-4"> <p>Time Zone </p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'time_zone'); ?>  </div>
                        </div>
                        <div class="row">                            
                            <div class="col-md-4"> <p>Street Address</p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'street_address'); ?>  </div>
                        </div>
                        <div class="row">                       
                            <div class="col-md-4"> <p>Address 2 </p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'address_2'); ?>   </div>
                        </div>
                        <div class="row">                  
                            
                            <div class="col-md-4"> <p>Postal Code</p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'postal_code'); ?>   </div>
                        </div>
                        <div class="row">                  
                            <div class="col-md-4"> <p>City </p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'city'); ?>   </div>
                        </div>
                        <div class="row">                                               
                            <div class="col-md-4"> <p>Country </p> </div>
                            <div class="col-md-8"> <?php Profile::iif($basicinfo,'county'); ?>   </div>
                        </div>                       
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>            
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!--    </div>
         /.container -->
</div>
<!-- /.content-wrapper -->
<!-- modal  -->

<!-- Button trigger modal -->

<!-- Modal -->
<?php 
    echo $_SESSION['email'];

?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Update Contact </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action=<?= base_url(); ?>profile/contactInfo method="post">
                    <input type="hidden" name="id" value="<?php Profile::iif($basicinfo,'id'); ?>" class="form-control" id="firstName" placeholder="First Name">
                    <div class="form-group">
                        <label for="firstName" class="col-sm-5 control-label">First Name</label>
                        <div class="col-sm-7">
                            <input type="text" name="firstName" value="<?php Profile::iif($basicinfo,'id'); ?>" class="form-control" id="firstName" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastName" class="col-sm-5 control-label">Last Name</label>
                        <div class="col-sm-7">
                            <input type="text" name="lastName" value="<?php Profile::iif($basicinfo,'last_name'); ?>"  class="form-control" id="lastName" placeholder="Last Name">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="jobTitle" class="col-sm-5 control-label">Job Title</label>
                        <div class="col-sm-7">
                            <input type="text" name="jobTitle" value="<?php Profile::iif($basicinfo,'job_title'); ?>" class="form-control" id="jobTitle" placeholder="Job Title">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="email" class="col-sm-5 control-label">Email</label>
                        <div class="col-sm-7">
                            <input type="email" name="email" value="<?php Profile::iif($basicinfo,'email'); ?>" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-5 control-label">Mobile</label>
                        <div class="col-sm-7">
                            <input type="text" name="mobile" value="<?php Profile::iif($basicinfo,'mobile'); ?>" class="form-control" id="moble" placeholder="Mobile">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="timeZone" class="col-sm-5 control-label">Time Zone</label>
                        <div class="col-sm-7">
                            <input type="text" name="timeZone"  value="<?= $conatct_info[0]->time_zone; ?>" class="form-control" id="timeZone" placeholder="Time Zone">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="streetAddress" class="col-sm-5 control-label">Street Address</label>
                        <div class="col-sm-7">
                            <input type="text" name="streetAddress" value="<?php Profile::iif($basicinfo,'street_address'); ?>" class="form-control" id="streetAddress" placeholder="Street Address">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="addressTwo" class="col-sm-5 control-label">Address 2</label>
                        <div class="col-sm-7">
                            <input type="text" name="addressTwo" value="<?php Profile::iif($basicinfo,'address_2'); ?>"  class="form-control" id="addressTwo" placeholder="Address 2">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="postalCode" class="col-sm-5 control-label">Postal Code</label>
                        <div class="col-sm-7">
                            <input type="text" name="postalCode" value="<?php Profile::iif($basicinfo,'postal_code'); ?> ?>" class="form-control" id="postalCode" placeholder="Postal Code">
                        </div>
                    </div>                     
                    <div class="form-group">
                        <label for="city" class="col-sm-5 control-label">City</label>
                        <div class="col-sm-7">
                            <input type="text" name="city" value="<?php Profile::iif($basicinfo,'city'); ?> ?>" class="form-control" id="city" placeholder="City">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="country" class="col-sm-5 control-label">Country</label>
                        <div class="col-sm-7">
                            <input type="text" name="country" value="<?php Profile::iif($basicinfo,'county'); ?> ?>" class="form-control" id="country" placeholder="country">
                        </div>
                    </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <input type="submit" class="btn btn-primary" name="update" value="Save changes">
            </div>
             </form>
        </div>
    </div>
</div>