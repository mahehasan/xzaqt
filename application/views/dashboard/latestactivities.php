<div class="latest-activities">
    <div class="row">
        <div class="col-md-12">
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                    <span class=".bg-gray-light">
                       <i class="fa fa-calendar"></i> Today
                    </span>
                </li><!-- /.timeline-label -->
                <li><!-- timeline item -->
                    <!-- timeline icon -->
                    <i class="fa fa-suitcase bg-aqua-active"></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-aqua-active text-bold "> Show your interest </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>

                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-comment-o bg-olive"></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-olive text-bold "> Set interview  </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>                
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-star bg-red "></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-red text-bold "> Manage Offer </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>  
                <!-- timeline time label -->
                <li class="time-label">
                    <span class=".bg-gray-light">
                       <i class="fa fa-calendar"></i>   10 Feb. 2014
                    </span>
                </li><!-- /.timeline-label -->
                <li><!-- timeline item -->
                    <!-- timeline icon -->
                    <i class="fa fa-suitcase bg-aqua-active"></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-aqua-active text-bold "> Show your interest </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>

                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-comment-o bg-olive"></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-olive text-bold "> Set interview  </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>                
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-star bg-red "></i>
                    <div class="timeline-item">
                        <span class=" btn-sm pull-right btn bg-red text-bold "> Manage Offer </span>
                        <h3 class="timeline-header"><span class="text-bold">Smith John</span> would like you to apply for the role of <span class="text-bold">Product Manager</span>.</h3>
                        <span class="status-button"><i class="fa fa-clock-o"></i> 2 minuted ago</span>       
                    </div>
                </li>                  
            </ul>
        </div>  
    </div>
</div>