<div class="container emp-dashboard-main-content">
    <div class="col-md-12">
        <div class="emp-dashboard">
            <div class="row">
                <div class="col-md-12 emp-dash-top">
                    <div class="col-md-3">
                        <div class="col-md-12 emp-dash-job-position">
                            <h4>Job Positions</h4>
                            <p><span>5</span></p>
                        </div>
                        <div class="col-md-12 emp-dash-average-time-to-fill">
                            <h4>Average time to fill</h4>
                            <p><span>2</span> days</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-6">
                            <div class="emp-dash-chart">
                                <div class="row">
                                    <div class="col-md-6">
                                        <canvas style="margin-top: 30px;" id="empChartOne"></canvas>                    
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-unstyled">
                                            <li> <i class="emp-dashboard-chart-icon-1 fa fa-fw fa-circle"></i> Invited <br> <span>45</span>  </li>
                                            <li> <i class="emp-dashboard-chart-icon-2 fa fa-fw fa-circle"></i> Applied  <br> <span>40</span> </li>
                                            <li> <i class="emp-dashboard-chart-icon-3 fa fa-fw fa-circle"></i> Shortlisted <br> <span>20</span></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>                            
                        </div>               
                        <div class="col-md-6">
                            <div class="emp-dash-chart">
                                <div class="row">
                                    <div class="col-md-6">
                                        <canvas style="margin-top: 30px;" id="empChartTwo"></canvas>                    
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-unstyled">
                                            <li> <i class="emp-dashboard-chart-icon-1 fa fa-fw fa-circle"></i> Interview <br> <span>20</span>  </li>
                                            <li> <i class="emp-dashboard-chart-icon-2 fa fa-fw fa-circle"></i> Offers  <br> <span>5</span> </li>
                                            <li> <i class="emp-dashboard-chart-icon-3 fa fa-fw fa-circle"></i> Hired <br> <span>6</span></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>                        
                        </div>               
                    </div>   
                </div>
            </div>
        </div>
    </div>   
    <div class="col-md-12">
        <h4> Upcoming Interview  </h4>
        <div class="emp-dash-upcomming-interview">
            <div class="row emp-dash-upcomming-interview-head ">
                <div class="col-md-3"> Candidate  </div>
                <div class="col-md-3"> time left  </div>
                <div class="col-md-4"> Related Job  </div>
                <div class="col-md-2"> Actions  </div>
            </div>
            <div class="row emp-dash-interview-list black-and-gray">
                <div class="col-md-12" >
                    <div class="col-md-3">
                        <img class="img-circle" width="100" height="100" src="<?php echo BASE_URL ?>assets/images/elina-strastoviski.png" alt="IMG">
                    </div>
                    <div class="col-md-3">
                        <p>Today <br><span>2h 36min left</span></p>

                    </div>
                    <div class="col-md-4">
                        <p>Product Manager needed ASAP!<br> <span>Los Angles, CA</span></p>
                    </div>
                    <div class="col-md-2">
                        <i class="emp-dash-comment-icon fa fa-fw fa-2x fa-commenting-o" ></i>
                    </div>
                </div>
            </div>
            <div class="row emp-dash-interview-list black-and-gray ">
                <div class="col-md-12" >
                    <div class="col-md-3">
                        <img class="img-circle" width="100" height="100" src="<?php echo BASE_URL ?>assets/images/jon-doe.png" alt="IMG">
                    </div>
                    <div class="col-md-3 ">
                        <p>Friday 25, July <br><span>2h 36min left</span></p>

                    </div>
                    <div class="col-md-4">
                        <p>Product Manager needed ASAP!<br> <span>Los Angles, CA</span></p>
                    </div>
                    <div class="col-md-2">
                        <i class="emp-dash-comment-icon icon fa fa-fw fa-2x fa-commenting-o" ></i>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h4> Last Jobs </h4>
        <div class="emp-dash-upcomming-interview">
            <div class="row emp-dash-upcomming-interview-head ">
                <div class="col-md-3"> Job Title </div>
                <div class="col-md-1"> Actions </div>
                <div class="col-md-2 text-center"> Best matches  </div>
                <div class="col-md-2 text-center"> Invite to apply  </div>
                <div class="col-md-2"> Interviews planned  </div>
                <div class="col-md-1"> Offered  </div>
                <div class="col-md-1"> Hired  </div>
            </div>
            <div class="row emp-dash-upcomming-interview-head black-and-gray ">
                <div class="col-md-3 text-sm "> 
                    <p class="job-title" > <button class="btn btn-success btn-sm ">Open</button> <br>
                        Product Manager needed ASAP! <br>
                        <span>Los Angels, CA <br>
                    </p>
                </div>
                <div class="col-md-1 margin-top-25"> 
                    <i class="emp-dash-action emp-dash-last-job fa fa-fw fa-edit "></i>
                </div>
                <div class="col-md-2 margin-top-25 "> <p class="text-center emp-dash-last-job"> 25 </p>  </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job "> 10 </p>   </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 5 </p>   </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 2 </p>  </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 0 </p>   </div>                
            </div> 
            <div class="row emp-dash-upcomming-interview-head black-and-gray ">
                <div class="col-md-3 text-sm "> 
                    <p class="job-title"> <button class="btn btn-theme-2 btn-sm "> Finished </button> <br>
                        UX Strategist & consultant <br>
                        <span>Los Angels, CA <br>
                    </p>
                </div>
                <div class="col-md-1 margin-top-25"> 
                    <i class="emp-dash-action emp-dash-last-job fa fa-fw fa-edit "></i>
                </div>
                <div class="col-md-2 margin-top-25 "> <p class="text-center emp-dash-last-job"> 25 </p>  </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job "> 10 </p>   </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 5 </p>   </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 2 </p>  </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 0 </p>   </div>                
            </div>
            <div class="row emp-dash-upcomming-interview-head black-and-gray ">
                <div class="col-md-3 text-sm "> 
                    <p class="job-title"> <button class="btn btn-danger btn-sm "> Closed </button> <br>
                        Copyright expert <br>
                        <span>Los Angels, CA <br>
                    </p>
                </div>
                <div class="col-md-1 margin-top-25"> 
                    <i class="emp-dash-action emp-dash-last-job fa fa-fw fa-edit "></i>
                </div>
                <div class="col-md-2 margin-top-25 "> <p class="text-center emp-dash-last-job"> 25 </p>  </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job "> 10 </p>   </div>
                <div class="col-md-2 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 5 </p>   </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 2 </p>  </div>
                <div class="col-md-1 borderleft margin-top-25"> <p class="text-center emp-dash-last-job"> 0 </p>   </div>                
            </div>            
            </div>
        </div>
    </div>