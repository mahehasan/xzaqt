<?php

class Upload extends CI_Controller {

    private $tableName = 'upload_img_desc';

    public function __construct() {
        parent::__construct();
        Common::canview();

        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        // $this->load->view('php', array('error' => ' '));
    }

    public function tellusvideo() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'avi|wmv|flv|mp4|3gp';
        $config['max_size'] = 100000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;
        $userId = Common::userId();
        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            echo $error['error'];
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
//                'user_id' => $user_id,
//                'type_id' => $type,                
                'user_id' => $userId,
                'type_id' => 1,
                'name' => $uploaddata['file_name']
            );
            $this->db->insert($this->tableName, $data);
            echo '1';
        }
    }

    public function referencedoc() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'doc|docx|pdf';
        $config['max_size'] = 10000;
//        $config['max_width'] = 1024;
//        $config['max_height'] = 768;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;
        $userId = Common::userId();
        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            echo $error['error'];
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $userId; //$this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
//                'user_id' => $user_id,
//                'type_id' => $type,                
                'user_id' => 1,
                'type_id' => 2,
                'name' => $uploaddata['file_name']
            );
            $this->db->insert($this->tableName, $data);
            echo '1';
        }
    }

    public function personaldoc() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'doc|docx|pdf';
        $config['max_size'] = 10000;
//        $config['max_width'] = 1024;
//        $config['max_height'] = 768;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;

        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            echo $error['error'];
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
//                'user_id' => $user_id,
//                'type_id' => $type,                
                'user_id' => 1,
                'type_id' => 3,
                'name' => $uploaddata['file_name']
            );
            $this->db->insert($this->tableName, $data);
            echo '1';
        }
    }

    public function companyVideoPresentation() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'avi|wmv|flv|mp4|3gp';
        $config['max_size'] = 100000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;
        $userId = Common::userId();
        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            echo $error['error'];
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
//                'user_id' => $user_id,
//                'type_id' => $type,                
                'user_id' => $userId,
                'type_id' => 4,
                'name' => $uploaddata['file_name']
            );
            $this->db->insert($this->tableName, $data);

            echo '1';
        }
    }

    public function companyImagePicture() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'TIF|JPG|PNG|GIF|tif|jpg|png|gif';
        $config['max_size'] = 100000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;
        $userId = Common::userId();
        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            echo $error['error'];
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
//                'user_id' => $user_id,
//                'type_id' => $type,                
                'user_id' => $userId,
                'type_id' => 5,
                'name' => $uploaddata['file_name']
            );
            $this->db->insert($this->tableName, $data);
            echo '1';
        }
    }

    public function profilepic() {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'tif|jpg|png|gif';
        $config['max_size'] = 1000;
//        $config['max_width'] = 120;
//        $config['max_height'] = 120;
//        $config['min_height'] = 120;
//        $config['min_width'] = 120;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['detect_mime'] = TRUE;
        $config['mod_mime_fix'] = TRUE;
        $userId = Common::userId();
        $table = $this->tableName;
        $this->load->library('upload', $config);
        // Alternately
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('php', $error);
            $data = array();
            $data['status'] = 0;
            $data['error'] = $error['error'];
            echo json_encode($data);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $uploaddata = $data['upload_data'];
            $name = $this->input->post('name');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $data = array(
                'user_id' => $userId,
                'type_id' => 6,
                'name' => $uploaddata['file_name']
            );
            $where = array('user_id' => $userId, 'type_id' => 6);
            
            $query = $this->db->select('type_id')->where($where)->get($table);
            if ($query->num_rows()) {
                $this->db->where($where)->update($table, $data);
            } else {
                $this->db->insert($table, $data);
            }
            $data['success'] = 1;
            echo json_encode($data);
        }
    }

}

?>