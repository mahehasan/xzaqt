<?php
// language list 
//$languages = Common::findAll('settings_languages');
// user 
$user_id = Common::user('id');
// selected languages 
$where = array( 'user_id'=> $user_id, 'skills_head_id'=> 1);
$languageDetails = Common::findByColumn('skills_dist', $where);
$languageDetails_json = json_encode($languageDetails);
// computer skills list 
$computerSkillList = Common::findAll('settings_computer_skills');

// computer details 
$where = array( 'user_id'=> $user_id, 'skills_head_id'=> 2);
$comSkillsDetails = Common::findByColumn('skills_dist', $where);
$comSkillsDetails_json = json_encode($comSkillsDetails);

?>
<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>
<div class="content">
    <div id="skills-error-notice">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
               <div class="alert alert-danger alert-dismissible" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   <strong>Warning!</strong> Please Add Language and Computer Skills First 
               </div>          
           </div>
        </div>     
    </div>
    <div class="row">
        <form id="form-language" action="<?php echo BASE_URL; ?>profile" method="post"> 
            <div class="col-md-12">
                <div class="row">
                    <div id="status-message-language" class="success">
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                            <strong>Success!</strong> Language Setup Successfully
                        </div>                        
                    </div>
                </div>
                <div class="result-result"></div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="skill-title-bar">
                            <span class="skill-circle"><i class="fa fa-fw fa-comment-o"></i></span>
                            <span class="thm-text ext-margin"> Languages (2)</span>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <button id="save-language" class="btn btn-theme pull-right " type="submit" name="language"> Add language </button>
                    </div>
                </div>
                <div id="language1" class="row">
                    <div class="col-md-3">                       
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> Language 1 </label>
                            <select name="language1" class="form-control select2" style="width: 100%;" >
                                <?php foreach ($languages as $lang) { ?>
                                    <option  value="<?php echo Common::iifMsg($lang, 'id'); ?>"> <?php echo Common::iifMsg($lang, 'name'); ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                        <!-- /.form-group -->                       
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12"><label class="fluency-label"> Fluency Label</label> </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid fluency-progress">
                                <div class="grid-1">
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8"><p class="left-bar"> <span value="1" class="circle <?php echo Common::skillSet($languageDetails, 0, 1); ?>"></span></p></div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="grid-2">
                                    <p> <span value="2" class="circle <?php echo Common::skillSet($languageDetails, 0, 2); ?>"></span></p>
                                </div>
                                <div class="grid-3">
                                    <p> <span value="3"  class="circle <?php echo Common::skillSet($languageDetails, 0, 3); ?>"></span></p>
                                </div>
                                <div class="grid-4">
                                    <p> <span value="4" class="circle <?php echo Common::skillSet($languageDetails, 0, 4); ?>"></span></p>
                                </div>
                                <div class="grid-5">
                                    <div class="row">
                                        <div class="col-md-8"><p class="right-bar"> <span value="5" class="circle <?php echo Common::skillSet($languageDetails, 0, 5); ?>"></span></p></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>   
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid">
                                <div class="grid-1">
                                    Elementary <br>Proficiency 
                                </div>
                                <div class="grid-2">
                                    Limited Working <br> Proficiency
                                </div>
                                <div class="grid-3">                              
                                    Minimum Professional <br> Proficiency
                                </div>
                                <div class="grid-4">
                                    Full Professional <br> Proficiency 
                                </div>
                                <div class="grid-5">
                                    Native or Bilingual<br> Proficiency                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
                <!-- Language 1 end -->
                <hr>
                <!-- Language 2 start -->
                <div id="language2" class="row">
                    <div class="col-md-3">                       
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> Language 2 </label>
                            <select name="language2" class="form-control select2" style="width: 100%;" >
                                <?php foreach ($languages as $lang) { ?>
                                    <option value="<?php echo Common::iifMsg($lang, 'id'); ?>"> <?php echo Common::iifMsg($lang, 'name'); ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                        <!-- /.form-group -->                       
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12"><label class="fluency-label"> Fluency Label</label> </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid fluency-progress">
                                <div class="grid-1">
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8"><p class="left-bar">  <span value="1" class="circle <?php echo Common::skillSet($languageDetails, 0, 5); ?>"></span></p></div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="grid-2">
                                    <p> <span value="2" class="circle <?php echo Common::skillSet($languageDetails, 1, 2); ?>"></span></p>
                                </div>
                                <div class="grid-3">
                                    <p> <span value="3"  class="circle <?php echo Common::skillSet($languageDetails, 1, 3); ?>"></span></p>
                                </div>
                                <div class="grid-4">
                                    <p> <span value="4"  class="circle <?php echo Common::skillSet($languageDetails, 1, 4); ?>"></span></p>
                                </div>
                                <div class="grid-5">
                                    <div class="row">
                                        <div class="col-md-8"><p class="right-bar"> <span value="5" class="circle <?php echo Common::skillSet($languageDetails, 1, 5); ?>"></span></p></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>   
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid">
                                <div class="grid-1">
                                    Elementary <br>Proficiency 
                                </div>
                                <div class="grid-2">
                                    Limited Working <br> Proficiency
                                </div>
                                <div class="grid-3">                              
                                    Minimum Professional <br> Proficiency
                                </div>
                                <div class="grid-4">
                                    Full Professional <br> Proficiency 
                                </div>
                                <div class="grid-5">
                                    Native or Bilingual<br> Proficiency                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                  
                <!-- Language 2 end -->

            </div>
        </form>
    </div>
    <!-- computer Skills start -->
    <div class="row">
        <br>
        <hr>
        <br>
        <form id="computerSkills" action="<?php echo BASE_URL; ?>profile/" method="post"> 
            <div class="col-md-12">
                <div class="row">
                    <div id="status-message-com" class="success">
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
                            <strong>Success!</strong> Computer Skills Setup Successfully 
                        </div>                        
                    </div>
                </div>
                <div class="result-result"></div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="skill-title-bar">
                            <span class="skill-circle"><i class="fa fa-fw  fa-laptop"></i></span>
                            <span class="thm-text ext-margin"> Computer Skills (3)</span>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <button class="btn btn-theme pull-right " type="submit" name="language"> Add Skills </button>
                    </div>
                </div>
                <div id="skills1" class="row">
                    <div class="col-md-3">                       
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> Computer Skills 1 </label>
                            <select name="comSkillOne" class="form-control select2" style="width: 100%;" >
                                <?php require 'computer_skills_list.php'; ?>
                            </select>
                        </div>
                        <!-- /.form-group -->                       
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12"><label class="fluency-label"> Mastering Level </label> </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid fluency-progress">
                                <div class="grid-1">
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8"><p class="left-bar"> <span value="1" class="circle <?php echo Common::skillSet($comSkillsDetails, 0, 1); ?>"></span></p></div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="grid-2">
                                    <p> <span value="2" class="circle <?php echo Common::skillSet($comSkillsDetails, 0, 2); ?>"></span></p>
                                </div>
                                <div class="grid-3">
                                    <p> <span value="3"  class="circle <?php echo Common::skillSet($comSkillsDetails, 0, 3); ?>"></span></p>
                                </div>
                                <div class="grid-4">
                                    <div class="row">
                                        <div class="col-md-8"><p class="right-bar"> <span value="4" class="circle <?php echo Common::skillSet($comSkillsDetails, 0, 4); ?>"></span></p></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div> 
                                <div class="grid-5">
                                     <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>
                                                    <input value="1" type="checkbox" class="minimal-red" name="last_used_1" <?php echo Common::skillCheck($comSkillsDetails, 0)?>> current ?
                                                </label>
                                            </div>                                           
                                        </div>
                                         <div class="col-md-4"> <div class="row">  <span data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="cskill-circle" >?</span> </div> </div>
                                    </div>
                                </div>
                                
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <?php require 'computer_skills_mastering_level.php'; ?>

                        </div>
                    </div>
                </div>              
                <!-- Sklls 1 end -->
                <hr>
                <!-- Skills 2 start -->
                <div id="skills2" class="row">
                    <div class="col-md-3">                       
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> Computer Skills 2 </label>
                            <select name="comSkillTwo" class="form-control select2" style="width: 100%;" >
                                <?php require 'computer_skills_list.php'; ?>
                            </select>
                        </div>
                        <!-- /.form-group -->                       
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12"><label class="fluency-label"> Mastering Level </label> </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid fluency-progress">
                                <div class="grid-1">
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8"><p class="left-bar"> <span value="1" class="circle <?php echo Common::skillSet($comSkillsDetails, 1, 1); ?>"></span></p></div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="grid-2">
                                    <p> <span value="2" class="circle <?php echo Common::skillSet($comSkillsDetails, 1, 2); ?>"></span></p>
                                </div>
                                <div class="grid-3">
                                    <p> <span value="3"  class="circle <?php echo Common::skillSet($comSkillsDetails, 1, 3); ?>"></span></p>
                                </div>
                                <div class="grid-4">
                                    <div class="row">
                                        <div class="col-md-8"><p class="right-bar"> <span value="4" class="circle <?php echo Common::skillSet($comSkillsDetails, 1, 4); ?>"></span></p></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div> 
                                <div class="grid-5">
                                     <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>
                                                    <input value="1" type="checkbox" class="minimal-red" name="last_used_2" <?php echo Common::skillCheck($comSkillsDetails, 1)?> > current ?
                                                </label>
                                            </div>
                                        </div>
                                         <div class="col-md-4">  <span class="cskill-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on top" >?</span> </div>
                                    </div>
                                </div>                                
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <?php require 'computer_skills_mastering_level.php'; ?>
                        </div>
                    </div>
                </div>                  
                <!-- Skills 2 End -->
                <hr>
                <!-- Skills 3 Start -->
                <div id="skills3" class="row">
                    <div class="col-md-3">                       
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label> Computer Skills 3 </label>
                            <select name="comSkillThree" class="form-control select2" style="width: 100%;" >
                                <?php require 'computer_skills_list.php'; ?>
                            </select>
                        </div>
                        <!-- /.form-group -->                       
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12"><label class="fluency-label"> Mastering Level </label> </div>
                        </div>
                        <div class="row">
                            <div class="custom-grid fluency-progress">
                                <div class="grid-1">
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8"><p class="left-bar"> <span value="1" class="circle <?php echo Common::skillSet($comSkillsDetails, 2, 1); ?>"></span></p></div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="grid-2">
                                    <p> <span value="2" class="circle <?php echo Common::skillSet($comSkillsDetails, 2, 2); ?>"></span></p>
                                </div>
                                <div class="grid-3">
                                    <p> <span value="3"  class="circle <?php echo Common::skillSet($comSkillsDetails, 2, 3); ?>"></span></p>
                                </div>
                                <div class="grid-4">
                                    <div class="row">
                                        <div class="col-md-8"><p class="right-bar"> <span value="4" class="circle <?php echo Common::skillSet($comSkillsDetails, 2, 4); ?>"></span></p></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div> 
                                <div class="grid-5">
                                     <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>
                                                    <input value="1" type="checkbox" class="minimal-red" name="last_used_3" <?php echo Common::skillCheck($comSkillsDetails, 2)?> > current ? 
                                                </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-4">  
                                            <span class="cskill-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on top" >?</span> 
                           
                                        </div>
                                    </div>
                                </div>                                
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <?php require 'computer_skills_mastering_level.php'; ?>
                        </div>
                    </div>
                </div>                  
                <!-- Skills 3 end -->
            </div>
        </form>  
        <div class="row text-center">
            <div style="margin-top:20px;" class="col-md-12">
                <button id="save-next" class=" btn btn-primary" type="submit">Save & Next </button>
            </div>    
        </div>
    </div>
    <!-- computer Skills end -->
</div>
<script>
    var langjson = <?php echo $languageDetails_json; ?>;
    var comjson = <?php echo $comSkillsDetails_json; ?>;
</script>
<script src="<?php echo BASE_URL; ?>assets/js/profile/skills.js"></script>
